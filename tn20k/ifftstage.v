module ifftstage (
	i_clk,
	i_reset,
	i_ce,
	i_sync,
	i_data,
	o_data,
	o_sync
);
	parameter IWIDTH = 12;
	parameter CWIDTH = 20;
	parameter OWIDTH = 13;
	parameter LGSPAN = 5;
	parameter BFLYSHIFT = 0;
	parameter [0:0] OPT_HWMPY = 1;
	parameter CKPCE = 1;
	parameter COEFFILE = "cmem_64.hex";
	localparam [0:0] ZERO_ON_IDLE = 1'b0;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire i_sync;
	input wire [(2 * IWIDTH) - 1:0] i_data;
	output reg [(2 * OWIDTH) - 1:0] o_data;
	output reg o_sync;
	reg wait_for_sync;
	reg [(2 * IWIDTH) - 1:0] ib_a;
	reg [(2 * IWIDTH) - 1:0] ib_b;
	reg [(2 * CWIDTH) - 1:0] ib_c;
	reg ib_sync;
	reg b_started;
	wire ob_sync;
	wire [(2 * OWIDTH) - 1:0] ob_a;
	wire [(2 * OWIDTH) - 1:0] ob_b;
	reg [(2 * CWIDTH) - 1:0] cmem [0:(1 << LGSPAN) - 1];
	initial $readmemh(COEFFILE, cmem);
	reg [LGSPAN:0] iaddr;
	reg [(2 * IWIDTH) - 1:0] imem [0:(1 << LGSPAN) - 1];
	reg [LGSPAN:0] oaddr;
	reg [(2 * OWIDTH) - 1:0] omem [0:(1 << LGSPAN) - 1];
	wire idle;
	reg [LGSPAN - 1:0] nxt_oaddr;
	reg [(2 * OWIDTH) - 1:0] pre_ovalue;
	initial wait_for_sync = 1'b1;
	initial iaddr = 0;
	always @(posedge i_clk)
		if (i_reset) begin
			wait_for_sync <= 1'b1;
			iaddr <= 0;
		end
		else if (i_ce && (!wait_for_sync || i_sync)) begin
			iaddr <= iaddr + {{LGSPAN {1'b0}}, 1'b1};
			wait_for_sync <= 1'b0;
		end
	always @(posedge i_clk)
		if (i_ce && !iaddr[LGSPAN])
			imem[iaddr[LGSPAN - 1:0]] <= i_data;
	initial ib_sync = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			ib_sync <= 1'b0;
		else if (i_ce)
			ib_sync <= iaddr == (1 << LGSPAN);
	always @(posedge i_clk)
		if (i_ce) begin
			ib_a <= imem[iaddr[LGSPAN - 1:0]];
			ib_b <= i_data;
			ib_c <= cmem[iaddr[LGSPAN - 1:0]];
		end
	generate
		if (ZERO_ON_IDLE) begin : GEN_ZERO_ON_IDLE
			reg r_idle;
			initial r_idle = 1;
			always @(posedge i_clk)
				if (i_reset)
					r_idle <= 1'b1;
				else if (i_ce)
					r_idle <= !iaddr[LGSPAN] && !wait_for_sync;
			assign idle = r_idle;
		end
		else begin : NO_IDLE_GENERATION
			assign idle = 0;
		end
		if (OPT_HWMPY) begin : HWBFLY
			hwbfly #(
				.IWIDTH(IWIDTH),
				.CWIDTH(CWIDTH),
				.OWIDTH(OWIDTH),
				.CKPCE(CKPCE),
				.SHIFT(BFLYSHIFT)
			) bfly(
				.i_clk(i_clk),
				.i_reset(i_reset),
				.i_ce(i_ce),
				.i_coef((idle && !i_ce ? {2 * CWIDTH {1'b0}} : ib_c)),
				.i_left((idle && !i_ce ? {2 * IWIDTH {1'b0}} : ib_a)),
				.i_right((idle && !i_ce ? {2 * IWIDTH {1'b0}} : ib_b)),
				.i_aux(ib_sync && i_ce),
				.o_left(ob_a),
				.o_right(ob_b),
				.o_aux(ob_sync)
			);
		end
		else begin : FWBFLY
			butterfly #(
				.IWIDTH(IWIDTH),
				.CWIDTH(CWIDTH),
				.OWIDTH(OWIDTH),
				.CKPCE(CKPCE),
				.SHIFT(BFLYSHIFT)
			) bfly(
				.i_clk(i_clk),
				.i_reset(i_reset),
				.i_ce(i_ce),
				.i_coef((idle && !i_ce ? {2 * CWIDTH {1'b0}} : ib_c)),
				.i_left((idle && !i_ce ? {2 * IWIDTH {1'b0}} : ib_a)),
				.i_right((idle && !i_ce ? {2 * IWIDTH {1'b0}} : ib_b)),
				.i_aux(ib_sync && i_ce),
				.o_left(ob_a),
				.o_right(ob_b),
				.o_aux(ob_sync)
			);
		end
	endgenerate
	initial oaddr = 0;
	initial o_sync = 0;
	initial b_started = 0;
	always @(posedge i_clk)
		if (i_reset) begin
			oaddr <= 0;
			o_sync <= 0;
			b_started <= 0;
		end
		else if (i_ce) begin
			o_sync <= (!oaddr[LGSPAN] ? ob_sync : 1'b0);
			if (ob_sync || b_started)
				oaddr <= oaddr + 1'b1;
			if (ob_sync && !oaddr[LGSPAN])
				b_started <= 1'b1;
		end
	always @(posedge i_clk)
		if (i_ce)
			nxt_oaddr[0] <= oaddr[0];
	generate
		if (LGSPAN > 1) begin : WIDE_LGSPAN
			always @(posedge i_clk)
				if (i_ce)
					nxt_oaddr[LGSPAN - 1:1] <= oaddr[LGSPAN - 1:1] + 1'b1;
		end
	endgenerate
	always @(posedge i_clk)
		if (i_ce && !oaddr[LGSPAN])
			omem[oaddr[LGSPAN - 1:0]] <= ob_b;
	always @(posedge i_clk)
		if (i_ce)
			pre_ovalue <= omem[nxt_oaddr[LGSPAN - 1:0]];
	always @(posedge i_clk)
		if (i_ce)
			o_data <= (!oaddr[LGSPAN] ? ob_a : pre_ovalue);
endmodule
