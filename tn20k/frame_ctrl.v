module frame_ctrl (
	start_bl,
	start_tx,
	bitv_tdata,
	clk,
	reset,
	bit_vector,
	bit_vector_tvalid,
	tx_end
);
	parameter DWIDTH = 16;
	parameter DDEPTH = 48;
	output reg start_bl;
	output reg start_tx;
	output reg [(DDEPTH * 3) - 1:0] bitv_tdata;
	input wire clk;
	input wire reset;
	input wire [(64 * DWIDTH) - 1:0] bit_vector;
	input wire bit_vector_tvalid;
	input wire tx_end;
	integer k;
	reg [DWIDTH - 1:0] bitv_i [0:DDEPTH];
	always @(posedge clk)
		if (bit_vector_tvalid) begin
			for (k = 5; k < 10; k = k + 1)
				bitv_i[k - 5] <= bit_vector[(63 - k) * DWIDTH+:DWIDTH];
			for (k = 11; k < 24; k = k + 1)
				bitv_i[k - 6] <= bit_vector[(63 - k) * DWIDTH+:DWIDTH];
			for (k = 25; k < 31; k = k + 1)
				bitv_i[k - 7] <= bit_vector[(63 - k) * DWIDTH+:DWIDTH];
			for (k = 32; k < 38; k = k + 1)
				bitv_i[k - 8] <= bit_vector[(63 - k) * DWIDTH+:DWIDTH];
			for (k = 39; k < 52; k = k + 1)
				bitv_i[k - 9] <= bit_vector[(63 - k) * DWIDTH+:DWIDTH];
			for (k = 53; k < 58; k = k + 1)
				bitv_i[k - 10] <= bit_vector[(63 - k) * DWIDTH+:DWIDTH];
		end
	reg bitv_tvalid_i;
	always @(posedge clk) bitv_tvalid_i <= bit_vector_tvalid;
	localparam BL = 1'b0;
	localparam TX = 1'b1;
	reg current_state;
	reg next_state;
	always @(*) begin : NEXT_STATE_LOGIC
		case (current_state)
			BL:
				if (bitv_tvalid_i)
					next_state = TX;
				else
					next_state = current_state;
			TX:
				if (tx_end)
					next_state = BL;
				else
					next_state = current_state;
			default: next_state = current_state;
		endcase
	end
	always @(posedge clk) begin : STATE_MEMORY
		if (reset)
			current_state <= BL;
		else
			current_state <= next_state;
	end
	reg [7:0] cnt = 8'b00000000;
	always @(posedge clk)
		if (next_state != current_state)
			cnt <= 0;
		else if (cnt < 15)
			cnt <= cnt + 1;
	always @(*) begin : OUTPUT_LOGIC
		case (current_state)
			BL:
				if (cnt < 10) begin
					start_bl = 1'b1;
					start_tx = 1'b0;
				end
				else
					start_bl = 1'b0;
			TX:
				if (cnt < 10) begin
					start_tx = 1'b1;
					start_bl = 1'b0;
				end
				else
					start_tx = 1'b0;
			default: begin
				start_bl = 1'b0;
				start_tx = 1'b0;
			end
		endcase
	end
	reg [1:0] bit_vector_i [0:DDEPTH - 1];
	integer j;
	always @(posedge clk)
		if (bitv_tvalid_i)
			for (j = 0; j < DDEPTH; j = j + 1)
				bitv_tdata[((DDEPTH - 1) - j) * 3+:3] <= 3'b011;
endmodule
