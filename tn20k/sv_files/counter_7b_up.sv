module counter_7b_up (
    output reg [6:0] Q,
    input wire clk, rst
    );

    always @(posedge clk or negedge rst)
    begin : COUNTER
        if (!rst)
            Q <= 0;
        else
        begin
            if (Q == 79)
                Q <= 0;
            else
                Q <= Q + 1;
        end
    end
endmodule