module sec_gen(
    //Master port
    output reg          M_SEC_TDATA,
    output reg          M_SEC_TVALID,
    input wire          M_SEC_TREADY,
    //Global
    input wire          ACLK,
    input wire          ARESETN
);
    localparam WIDTH = 9;
    localparam MAX = 448;

    reg [WIDTH-1:0]     CNT;
    reg [MAX-1:0] data_sec = 448'b0010_1110_0111_0100_0110_1001_0110_1100_0110_0101_0010_0000_0110_0111_0110_1110_0110_1001_0110_0011_0111_0011_0110_1001_0111_0000_0110_1001_0110_0100_0110_0001_0010_0000_0111_0010_0111_0101_0111_0100_0110_0101_0111_0100_0110_0011_0110_0101_0111_0011_0110_1110_0110_1111_0110_0011_0010_0000_0010_1100_0111_0100_0110_0101_0110_1101_0110_0001_0010_0000_0111_0100_0110_1001_0111_0011_0010_0000_0111_0010_0110_1111_0110_1100_0110_1111_0110_0100_0010_0000_0110_1101_0111_0101_0111_0011_0111_0000_0110_1001_0010_0000_0110_1101_0110_0101_0111_0010_0110_1111_0100_1100;
    reg next_data;
    always @(posedge ACLK or negedge ARESETN)
    begin
        if (!ARESETN) begin
            CNT <= 0;
        end
        else if (M_SEC_TREADY) begin
                next_data  = data_sec[CNT];
            if(CNT==MAX-1)
                CNT <= 0;
            else
                CNT = CNT+1;
        end
    end

	always @(posedge ACLK)
	if (!ARESETN)
		M_SEC_TVALID <= 1'b0;
	else if (CNT != 0)
		M_SEC_TVALID <= 1'b1;

	always @(posedge ACLK)
    if (M_SEC_TREADY)
		M_SEC_TDATA <= next_data;

endmodule