module top (
    output wire pin15,
    output wire pin16,
    input wire clk,
    input wire reset
);
    wire clk1, clk2;
    wire [11:0] data_tre, data_tim;
    wire [12:0] cenv_tre, cenv_tim;
    wire        cenv_tsync, data_tsync;
    wire        dsec_tdata, dsec_tvalid;
    assign pin15 = dsec_tdata;
    assign pin16 = dsec_tvalid;

    localparam DIV1 = 28'd5;
    localparam DIV2 = 28'd20;
    //reg [1:0] dmap_tsel = {2'b11};
    clock_divider #(DIV1) UC1 ( .clk_out(clk1),
                              .clk_in(clk));
    clock_divider #(DIV2) UC2 ( .clk_out(clk2),
                              .clk_in(clk));
    wire start_bl, start_tx, bit_vector_tvalid, tx_end;
    //Simula la ejecución de la rutina de bit loading
    reg [7:0] cnt_bitl = {8'd0};
    reg init_bitl = {1'b0};
    always @(posedge clk2) begin 
        cnt_bitl <= cnt_bitl + 1;
        if (cnt_bitl == 50) init_bitl <= 1;
    end

    assign bit_vector_tvalid = (init_bitl)? 1'b1 : 1'b0;

    wire [15:0] bit_vector [0:63];
    wire [2:0]  bitv_tdata [0:47];
    frame_ctrl CTRL (start_bl, start_tx, bitv_tdata, clk, reset, bit_vector, bit_vector_tvalid, tx_end);
   //bit_load_proc LC (clk, !resetn, start_bl, bit_vector, bit_vector_tvalid);

    transmitter U1 (  .TX_ICH(cenv_tre),
                      .TX_QCH(cenv_tim),
                      .TX_SYNC(cenv_tsync),
                      .BITV_TDATA(bitv_tdata),
                      .BITV_TVALID(start_tx),
                      .ACLK1(clk1),
                      .ACLK2(clk2),
                      .ARESETN(!reset));

    receiver U2 (       .dsec_tdata(dsec_tdata),
                        .dsec_tvalid(dsec_tvalid),
                        .RX_ICH(cenv_tre),
                        .RX_QCH(cenv_tim),
                        .RX_SYNC(cenv_tsync),
                        .BITV_TDATA(bitv_tdata),
                        .BITV_TVALID(start_tx),
                        .ACLK1(clk1),
                        .ACLK2(clk2),
                        .ARESETN(!reset));

endmodule