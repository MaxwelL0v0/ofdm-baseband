module pilot_extractor #(
    parameter DWIDTH = 13
)(
    //Master port
    output wire [DWIDTH-1:0]    M_MAP_TICH,
    output wire [DWIDTH-1:0]    M_MAP_TQCH,
    output wire                 M_MAP_TVALID,
    //Slave port
    input wire [DWIDTH-1:0]     SAMP_ICH,
    input wire [DWIDTH-1:0]     SAMP_QCH,
    input wire                  SAMP_SYNC,
    input wire                  ACLK,
    input wire                  ARESETN
);

    reg [DWIDTH-1:0] samp_re_i, samp_im_i;
    reg frame_valid;
    reg map_valid = 1'b0;

    always @(posedge ACLK) begin
       if (!ARESETN) begin
           frame_valid <= 1'b0;
          // map_valid <= 1'b0;
       end
       else if (SAMP_SYNC == 1'b1) begin
            frame_valid <= 1'b1;
        end
       samp_re_i <= SAMP_ICH;
       samp_im_i <= SAMP_QCH;
    end

    reg [6:0] dataCount;
    always @(posedge ACLK) begin
        if (SAMP_SYNC)
            dataCount <= 0;
        else
            dataCount <= dataCount + 1;
    end

    localparam NUMLGSC = 5;
    always @(posedge ACLK) begin
       if (frame_valid) begin
            if (dataCount >= NUMLGSC-1 && dataCount < 9)
                map_valid = 1'b1;
            else if (dataCount > 9 && dataCount < 17)
                map_valid = 1'b1;
            else if (dataCount > 32 && dataCount < 39)
                map_valid = 1'b1;
            else if (dataCount > 39 && dataCount < 46)
                map_valid = 1'b1;
            else if (dataCount > 46 && dataCount < 53)
                map_valid = 1'b1;
            else if (dataCount > 53 && dataCount < 67)
                map_valid = 1'b1;
            else if (dataCount > 67 && dataCount < 73)
                map_valid = 1'b1;
            else
                map_valid = 1'b0;
       end
    end
    localparam SF = 8;
    assign M_MAP_TVALID = map_valid;
    assign M_MAP_TICH = samp_re_i*SF;
    assign M_MAP_TQCH = samp_im_i*SF;

endmodule