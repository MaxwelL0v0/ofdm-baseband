module transmitter (
	TX_ICH,
	TX_QCH,
	TX_SYNC,
	BITV_TDATA,
	BITV_TVALID,
	ACLK1,
	ACLK2,
	ARESETN
);
	output wire [12:0] TX_ICH;
	output wire [12:0] TX_QCH;
	output wire TX_SYNC;
	input wire [143:0] BITV_TDATA;
	input wire BITV_TVALID;
	input wire ACLK1;
	input wire ACLK2;
	input wire ARESETN;
	localparam DDEPTH = 48;
	localparam IWFFT = 12;
	localparam OWFFT = 13;
	wire samp_tvalid;
	wire samp_tlast;
	wire samp_tready;
	wire [11:0] samp_i_ch;
	wire [11:0] samp_q_ch;
	wire sec_tdata;
	wire sec_tvalid;
	wire sec_tready;
	wire [11:0] map_q_ch;
	wire [11:0] map_i_ch;
	wire map_tvalid;
	wire map_tready;
	wire map_tlast;
	wire [12:0] tx_ich_i;
	wire [12:0] tx_qch_i;
	wire tx_sync_i;
	sec_gen U1(
		.M_SEC_TDATA(sec_tdata),
		.M_SEC_TVALID(sec_tvalid),
		.M_SEC_TREADY(sec_tready),
		.ACLK(ACLK1),
		.ARESETN(ARESETN)
	);
	wire bitv_ready;
	const_mapper U2(
		.M_MAP_TICH(map_i_ch),
		.M_MAP_TQCH(map_q_ch),
		.M_MAP_TVALID(map_tvalid),
		.M_MAP_TREADY(map_tready),
		.M_MAP_TLAST(map_tlast),
		.S_BITV_TDATA(BITV_TDATA),
		.S_BITV_TVALID(BITV_TVALID),
		.S_BITV_TREADY(bitv_ready),
		.S_SEC_TDATA(sec_tdata),
		.S_SEC_TVALID(sec_tvalid),
		.S_SEC_TREADY(sec_tready),
		.ACLK(ACLK1),
		.ARESETN(ARESETN)
	);
	symbol_formation U3(
		.S_MAP_TICH(map_i_ch),
		.S_MAP_TQCH(map_q_ch),
		.S_MAP_TVALID(map_tvalid),
		.S_MAP_TLAST(map_tlast),
		.S_MAP_TREADY(map_tready),
		.M_SAMP_TICH(samp_i_ch),
		.M_SAMP_TQCH(samp_q_ch),
		.M_SAMP_TVALID(samp_tvalid),
		.M_SAMP_TLAST(samp_tlast),
		.M_SAMP_TREADY(samp_tready),
		.ACLK1(ACLK1),
		.ACLK2(ACLK2),
		.ARESETN(ARESETN)
	);
	ifft_gen U4(
		.CENV_ICH(tx_ich_i),
		.CENV_QCH(tx_qch_i),
		.CENV_SYNC(tx_sync_i),
		.S_SAMP_TICH(samp_i_ch),
		.S_SAMP_TQCH(samp_q_ch),
		.S_SAMP_TVALID(samp_tvalid),
		.S_SAMP_TREADY(samp_tready),
		.S_SAMP_TLAST(samp_tlast),
		.ACLK(ACLK2),
		.ARESETN(ARESETN)
	);
	assign TX_ICH = tx_ich_i;
	assign TX_QCH = tx_qch_i;
	assign TX_SYNC = tx_sync_i;
endmodule
