module symbol_formation (
	M_SAMP_TICH,
	M_SAMP_TQCH,
	M_SAMP_TVALID,
	M_SAMP_TLAST,
	M_SAMP_TREADY,
	S_MAP_TICH,
	S_MAP_TQCH,
	S_MAP_TVALID,
	S_MAP_TLAST,
	S_MAP_TREADY,
	ACLK1,
	ACLK2,
	ARESETN
);
	parameter DWIDTH = 12;
	output wire [DWIDTH - 1:0] M_SAMP_TICH;
	output wire [DWIDTH - 1:0] M_SAMP_TQCH;
	output wire M_SAMP_TVALID;
	output wire M_SAMP_TLAST;
	input wire M_SAMP_TREADY;
	input wire [DWIDTH - 1:0] S_MAP_TICH;
	input wire [DWIDTH - 1:0] S_MAP_TQCH;
	input wire S_MAP_TVALID;
	input wire S_MAP_TLAST;
	output wire S_MAP_TREADY;
	input wire ACLK1;
	input wire ACLK2;
	input wire ARESETN;
	localparam DLENGTH = 48;
	localparam FFTLENGTH = 64;
	localparam NUMLGSC = 5;
	localparam NUMRGSC = 6;
	localparam CPLENGTH = 16;
	reg [DWIDTH - 1:0] MEM_RE_1 [0:47];
	reg [DWIDTH - 1:0] MEM_IM_1 [0:47];
	reg [DWIDTH - 1:0] MEM_RE_2 [0:47];
	reg [DWIDTH - 1:0] MEM_IM_2 [0:47];
	wire [DWIDTH - 1:0] MEM_RE_1_D0;
	wire [DWIDTH - 1:0] MEM_RE_2_D0;
	wire [DWIDTH - 1:0] MEM_IM_1_D0;
	wire [DWIDTH - 1:0] MEM_IM_2_D0;
	assign MEM_RE_1_D0 = MEM_RE_1[0];
	assign MEM_IM_1_D0 = MEM_IM_1[0];
	assign MEM_RE_2_D0 = MEM_RE_2[0];
	assign MEM_IM_2_D0 = MEM_IM_2[0];
	reg readytoForm = 1'b0;
	reg [DWIDTH - 1:0] tx_re_i;
	reg [DWIDTH - 1:0] tx_im_i;
	reg [5:0] countData = 6'd0;
	reg [5:0] countData_delay = 6'd0;
	always @(posedge ACLK1) countData_delay <= countData;
	always @(posedge ACLK1)
		if (S_MAP_TVALID) begin
			if (countData == DLENGTH)
				countData <= 0;
			else
				countData <= countData + 1;
		end
		else if (countData == DLENGTH)
			countData <= 0;
	always @(posedge ACLK1)
		if (S_MAP_TVALID) begin
			MEM_RE_1[countData] <= S_MAP_TICH;
			MEM_IM_1[countData] <= S_MAP_TQCH;
		end
	wire store_ofdm_symb;
	integer i;
	integer j;
	reg [3:0] store_cnt = 4'd0;
	always @(posedge ACLK2)
		if (store_ofdm_symb)
			store_cnt <= store_cnt + 1;
		else
			store_cnt <= 0;
	localparam LGSC = 4'b0001;
	reg [3:0] next_state;
	assign store_ofdm_symb = ((next_state == LGSC) && ((countData == 48) || (countData == 0)) ? 1'b1 : 1'b0);
	always @(posedge ACLK2)
		if (store_ofdm_symb && (store_cnt == 0))
			for (i = 0; i < DLENGTH; i = i + 1)
				for (j = 0; j < DWIDTH; j = j + 1)
					begin
						MEM_RE_2[i][j] <= MEM_RE_1[i][j];
						MEM_IM_2[i][j] <= MEM_IM_1[i][j];
					end
	reg [5:0] symb_cnt = 6'd0;
	always @(negedge S_MAP_TLAST) symb_cnt <= symb_cnt + 1;
	reg [3:0] current_state;
	localparam IDLE = 4'b0000;
	localparam D1 = 4'b0010;
	localparam P1 = 4'b0011;
	localparam D2 = 4'b0100;
	localparam P2 = 4'b0101;
	localparam D3 = 4'b0110;
	localparam DC = 4'b0111;
	localparam D4 = 4'b1000;
	localparam P3 = 4'b1001;
	localparam D5 = 4'b1010;
	localparam P4 = 4'b1011;
	localparam D6 = 4'b1100;
	localparam HGSC = 4'b1101;
	wire [6:0] id_symb;
	assign S_MAP_TREADY = (((current_state == IDLE) || (id_symb == 7'd10)) && M_SAMP_TREADY ? 1'b1 : 1'b0);
	localparam TX_IDLE = 12'h000;
	localparam TX_GSC = 12'd0;
	localparam TX_DC = 12'd0;
	wire LG_done;
	wire D1_done;
	wire P1_done;
	wire D2_done;
	wire P2_done;
	wire D3_done;
	wire DC_done;
	wire D4_done;
	wire P3_done;
	wire D5_done;
	wire P4_done;
	wire D6_done;
	wire HG_done;
	always @(posedge ACLK1)
		if ((current_state != IDLE) && (countData == 48))
			readytoForm <= 1'b1;
	counter_7b_up SYM_COUNTER(
		.clk(ACLK2),
		.rst(readytoForm),
		.Q(id_symb)
	);
	assign LG_done = (id_symb == 4 ? 1'b1 : 1'b0);
	assign D1_done = (id_symb == 9 ? 1'b1 : 1'b0);
	assign P1_done = (id_symb == 10 ? 1'b1 : 1'b0);
	assign D2_done = (id_symb == 23 ? 1'b1 : 1'b0);
	assign P2_done = (id_symb == 24 ? 1'b1 : 1'b0);
	assign D3_done = (id_symb == 30 ? 1'b1 : 1'b0);
	assign DC_done = (id_symb == 31 ? 1'b1 : 1'b0);
	assign D4_done = (id_symb == 37 ? 1'b1 : 1'b0);
	assign P3_done = (id_symb == 38 ? 1'b1 : 1'b0);
	assign D5_done = (id_symb == 51 ? 1'b1 : 1'b0);
	assign P4_done = (id_symb == 52 ? 1'b1 : 1'b0);
	assign D6_done = (id_symb == 57 ? 1'b1 : 1'b0);
	assign HG_done = (id_symb == 79 ? 1'b1 : 1'b0);
	reg idle_done = 1'b0;
	always @(posedge ACLK2)
		if (countData == 47)
			idle_done <= 1'b1;
	always @(*) begin : NEXT_STATE_LOGIC
		case (current_state)
			IDLE:
				if (idle_done)
					next_state = LGSC;
				else
					next_state = current_state;
			LGSC:
				if (LG_done)
					next_state = D1;
				else
					next_state = current_state;
			D1:
				if (D1_done)
					next_state = P1;
				else
					next_state = current_state;
			P1:
				if (P1_done)
					next_state = D2;
				else
					next_state = current_state;
			D2:
				if (D2_done)
					next_state = P2;
				else
					next_state = current_state;
			P2:
				if (P2_done)
					next_state = D3;
				else
					next_state = current_state;
			D3:
				if (D3_done)
					next_state = DC;
				else
					next_state = current_state;
			DC:
				if (DC_done)
					next_state = D4;
				else
					next_state = current_state;
			D4:
				if (D4_done)
					next_state = P3;
				else
					next_state = current_state;
			P3:
				if (P3_done)
					next_state = D5;
				else
					next_state = current_state;
			D5:
				if (D5_done)
					next_state = P4;
				else
					next_state = current_state;
			P4:
				if (P4_done)
					next_state = D6;
				else
					next_state = current_state;
			D6:
				if (D6_done)
					next_state = HGSC;
				else
					next_state = current_state;
			HGSC:
				if (HG_done)
					next_state = LGSC;
				else
					next_state = current_state;
			default: next_state = current_state;
		endcase
	end
	always @(posedge ACLK2) begin : STATE_MEMORY
		if (!ARESETN)
			current_state <= IDLE;
		else
			current_state <= next_state;
	end
	always @(*) begin : OUTPUT_LOGIC
		case (current_state)
			IDLE: begin
				tx_re_i = TX_IDLE;
				tx_im_i = TX_IDLE;
			end
			LGSC: begin
				tx_re_i = TX_GSC;
				tx_im_i = TX_GSC;
			end
			D1: begin
				tx_re_i = MEM_RE_2[id_symb - NUMLGSC];
				tx_im_i = MEM_IM_2[id_symb - NUMLGSC];
			end
			P1: begin
				tx_re_i = 1;
				tx_im_i = 1;
			end
			D2: begin
				tx_re_i = MEM_RE_2[id_symb - 6];
				tx_im_i = MEM_IM_2[id_symb - 6];
			end
			P2: begin
				tx_re_i = 1;
				tx_im_i = 1;
			end
			D3: begin
				tx_re_i = MEM_RE_2[id_symb - 7];
				tx_im_i = MEM_IM_2[id_symb - 7];
			end
			DC: begin
				tx_re_i = 0;
				tx_im_i = 0;
			end
			D4: begin
				tx_re_i = MEM_RE_2[id_symb - 8];
				tx_im_i = MEM_IM_2[id_symb - 8];
			end
			P3: begin
				tx_re_i = 1;
				tx_im_i = 1;
			end
			D5: begin
				tx_re_i = MEM_RE_2[id_symb - 9];
				tx_im_i = MEM_IM_2[id_symb - 9];
			end
			P4: begin
				tx_re_i = 1;
				tx_im_i = 1;
			end
			D6: begin
				tx_re_i = MEM_RE_2[id_symb - 10];
				tx_im_i = MEM_IM_2[id_symb - 10];
			end
			HGSC: begin
				tx_re_i = TX_GSC;
				tx_im_i = TX_GSC;
			end
		endcase
	end
	assign M_SAMP_TLAST = ((current_state == HGSC) && (id_symb == 63) ? 1'b1 : 1'b0);
	localparam SF = 1;
	assign M_SAMP_TICH = SF * tx_re_i;
	assign M_SAMP_TQCH = SF * tx_im_i;
	assign M_SAMP_TVALID = ((!readytoForm && store_ofdm_symb) || ((((current_state != IDLE) && M_SAMP_TREADY) && readytoForm) && (id_symb < FFTLENGTH)) ? 1'b1 : 1'b0);
endmodule
