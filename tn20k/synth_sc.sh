#!/bin/bash

sudo rm -r slpp_all
sudo rm top.json pnrtop.json pack.fs
#Síntesis
plug_sv="plugin -i systemverilog"
top_mod="read_systemverilog count_led.sv"
synth="synth_gowin -json top.json"
sudo yosys -p "$plug_sv"  -p "$top_mod"  \
           -p "$synth"
#Route
DEVICE='GW2AR-LV18QN88C8/I7'
BOARD='tangnano20k'
nextpnr-himbaechel --json top.json --write pnrtop.json --device $DEVICE --vopt cst=$BOARD.cst --vopt family=GW2A-18C
gowin_pack -d GW2A-18C -o pack.fs pnrtop.json

sudo openFPGALoader -b $BOARD pack.fs

#gowin_pack -d $DEVICE -o pack.fs pnrtop.json
