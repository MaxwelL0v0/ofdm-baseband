module qtrstage (
	i_clk,
	i_reset,
	i_ce,
	i_sync,
	i_data,
	o_data,
	o_sync
);
	parameter IWIDTH = 16;
	parameter OWIDTH = IWIDTH + 1;
	parameter LGWIDTH = 8;
	parameter INVERSE = 0;
	parameter SHIFT = 0;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire i_sync;
	input wire [(2 * IWIDTH) - 1:0] i_data;
	output reg [(2 * OWIDTH) - 1:0] o_data;
	output reg o_sync;
	reg wait_for_sync;
	reg [2:0] pipeline;
	reg signed [IWIDTH:0] sum_r;
	reg signed [IWIDTH:0] sum_i;
	reg signed [IWIDTH:0] diff_r;
	reg signed [IWIDTH:0] diff_i;
	reg [(2 * OWIDTH) - 1:0] ob_a;
	wire [(2 * OWIDTH) - 1:0] ob_b;
	reg [OWIDTH - 1:0] ob_b_r;
	reg [OWIDTH - 1:0] ob_b_i;
	assign ob_b = {ob_b_r, ob_b_i};
	reg [LGWIDTH - 1:0] iaddr;
	reg [(2 * IWIDTH) - 1:0] imem [0:1];
	wire signed [IWIDTH - 1:0] imem_r;
	wire signed [IWIDTH - 1:0] imem_i;
	assign imem_r = imem[1][(2 * IWIDTH) - 1:IWIDTH];
	assign imem_i = imem[1][IWIDTH - 1:0];
	wire signed [IWIDTH - 1:0] i_data_r;
	wire signed [IWIDTH - 1:0] i_data_i;
	assign i_data_r = i_data[(2 * IWIDTH) - 1:IWIDTH];
	assign i_data_i = i_data[IWIDTH - 1:0];
	reg [(2 * OWIDTH) - 1:0] omem [0:1];
	wire signed [OWIDTH - 1:0] rnd_sum_r;
	wire signed [OWIDTH - 1:0] rnd_sum_i;
	wire signed [OWIDTH - 1:0] rnd_diff_r;
	wire signed [OWIDTH - 1:0] rnd_diff_i;
	wire signed [OWIDTH - 1:0] n_rnd_diff_r;
	wire signed [OWIDTH - 1:0] n_rnd_diff_i;
	convround #(
		.IWID(IWIDTH + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT)
	) do_rnd_sum_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(sum_r),
		.o_val(rnd_sum_r)
	);
	convround #(
		.IWID(IWIDTH + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT)
	) do_rnd_sum_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(sum_i),
		.o_val(rnd_sum_i)
	);
	convround #(
		.IWID(IWIDTH + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT)
	) do_rnd_diff_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(diff_r),
		.o_val(rnd_diff_r)
	);
	convround #(
		.IWID(IWIDTH + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT)
	) do_rnd_diff_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(diff_i),
		.o_val(rnd_diff_i)
	);
	assign n_rnd_diff_r = -rnd_diff_r;
	assign n_rnd_diff_i = -rnd_diff_i;
	initial wait_for_sync = 1'b1;
	initial iaddr = 0;
	always @(posedge i_clk)
		if (i_reset) begin
			wait_for_sync <= 1'b1;
			iaddr <= 0;
		end
		else if (i_ce && (!wait_for_sync || i_sync)) begin
			iaddr <= iaddr + 1'b1;
			wait_for_sync <= 1'b0;
		end
	always @(posedge i_clk)
		if (i_ce) begin
			imem[0] <= i_data;
			imem[1] <= imem[0];
		end
	initial pipeline = 3'h0;
	always @(posedge i_clk)
		if (i_reset)
			pipeline <= 3'h0;
		else if (i_ce)
			pipeline <= {pipeline[1:0], iaddr[1]};
	always @(posedge i_clk)
		if (i_ce && iaddr[1]) begin
			sum_r <= imem_r + i_data_r;
			sum_i <= imem_i + i_data_i;
			diff_r <= imem_r - i_data_r;
			diff_i <= imem_i - i_data_i;
		end
	always @(posedge i_clk)
		if (i_ce) begin
			ob_a <= {rnd_sum_r, rnd_sum_i};
			if (!iaddr[0]) begin
				ob_b_r <= rnd_diff_r;
				ob_b_i <= rnd_diff_i;
			end
			else if (INVERSE == 0) begin
				ob_b_r <= rnd_diff_i;
				ob_b_i <= n_rnd_diff_r;
			end
			else begin
				ob_b_r <= n_rnd_diff_i;
				ob_b_i <= rnd_diff_r;
			end
		end
	always @(posedge i_clk)
		if (i_ce) begin
			omem[0] <= ob_b;
			omem[1] <= omem[0];
			if (pipeline[2])
				o_data <= ob_a;
			else
				o_data <= omem[1];
		end
	initial o_sync = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			o_sync <= 1'b0;
		else if (i_ce)
			o_sync <= iaddr[2:0] == 3'b101;
endmodule
