module longbimpy (
	i_clk,
	i_ce,
	i_a_unsorted,
	i_b_unsorted,
	o_r
);
	parameter IAW = 8;
	parameter IBW = 12;
	localparam AW = (IAW < IBW ? IAW : IBW);
	localparam BW = (IAW < IBW ? IBW : IAW);
	localparam IW = (AW + 1) & -2;
	localparam LUTB = 2;
	localparam TLEN = (AW + 1) / LUTB;
	input wire i_clk;
	input wire i_ce;
	input wire [IAW - 1:0] i_a_unsorted;
	input wire [IBW - 1:0] i_b_unsorted;
	output reg [(AW + BW) - 1:0] o_r;
	wire [AW - 1:0] i_a;
	wire [BW - 1:0] i_b;
	generate
		if (1) begin : PARAM_CHECK
			if (IAW <= IBW) begin : NO_PARAM_CHANGE_I
				assign i_a = i_a_unsorted;
				assign i_b = i_b_unsorted;
			end
			else begin : SWAP_PARAMETERS_I
				assign i_a = i_b_unsorted;
				assign i_b = i_a_unsorted;
			end
		end
	endgenerate
	reg [IW - 1:0] u_a;
	reg [BW - 1:0] u_b;
	reg sgn;
	reg [IW - 5:0] r_a [0:TLEN - 3];
	reg [BW - 1:0] r_b [0:TLEN - 3];
	reg [TLEN - 1:0] r_s;
	reg [(IW + BW) - 1:0] acc [0:TLEN - 2];
	genvar _gv_k_2;
	wire [(BW + LUTB) - 1:0] pr_a;
	wire [(BW + LUTB) - 1:0] pr_b;
	wire [(IW + BW) - 1:0] w_r;
	initial u_a = 0;
	generate
		if (1) begin : ABS
			if (IW > AW) begin : ABS_AND_ADD_BIT_TO_A
				always @(posedge i_clk)
					if (i_ce)
						u_a <= {1'b0, (i_a[AW - 1] ? -i_a : i_a)};
			end
			else begin : ABS_A
				always @(posedge i_clk)
					if (i_ce)
						u_a <= (i_a[AW - 1] ? -i_a : i_a);
			end
		end
	endgenerate
	initial sgn = 0;
	initial u_b = 0;
	always @(posedge i_clk)
		if (i_ce) begin : ABS_B
			u_b <= (i_b[BW - 1] ? -i_b : i_b);
			sgn <= i_a[AW - 1] ^ i_b[BW - 1];
		end
	bimpy #(.BW(BW)) lmpy_0(
		.i_clk(i_clk),
		.i_reset(1'b0),
		.i_ce(i_ce),
		.i_a(u_a[1:0]),
		.i_b(u_b),
		.o_r(pr_a)
	);
	bimpy #(.BW(BW)) lmpy_1(
		.i_clk(i_clk),
		.i_reset(1'b0),
		.i_ce(i_ce),
		.i_a(u_a[3:LUTB]),
		.i_b(u_b),
		.o_r(pr_b)
	);
	initial r_s = 0;
	initial r_a[0] = 0;
	initial r_b[0] = 0;
	always @(posedge i_clk)
		if (i_ce) begin
			r_a[0] <= u_a[IW - 1:4];
			r_b[0] <= u_b;
			r_s <= {r_s[TLEN - 2:0], sgn};
		end
	initial acc[0] = 0;
	always @(posedge i_clk)
		if (i_ce)
			acc[0] <= {{IW - LUTB {1'b0}}, pr_a} + {{IW - 4 {1'b0}}, pr_b, {LUTB {1'b0}}};
	generate
		if (1) begin : COPY
			if (TLEN > 3) begin : FOR
				for (_gv_k_2 = 0; _gv_k_2 < (TLEN - 3); _gv_k_2 = _gv_k_2 + 1) begin : GENCOPIES
					localparam k = _gv_k_2;
					initial r_a[k + 1] = 0;
					initial r_b[k + 1] = 0;
					always @(posedge i_clk)
						if (i_ce) begin
							r_a[k + 1] <= {{LUTB {1'b0}}, r_a[k][IW - 5:LUTB]};
							r_b[k + 1] <= r_b[k];
						end
				end
			end
		end
		if (1) begin : STAGES
			if (TLEN > 2) begin : FOR
				for (_gv_k_2 = 0; _gv_k_2 < (TLEN - 2); _gv_k_2 = _gv_k_2 + 1) begin : GENSTAGES
					localparam k = _gv_k_2;
					wire [(BW + LUTB) - 1:0] genp;
					bimpy #(.BW(BW)) genmpy(
						.i_clk(i_clk),
						.i_reset(1'b0),
						.i_ce(i_ce),
						.i_a(r_a[k][1:0]),
						.i_b(r_b[k]),
						.o_r(genp)
					);
					initial acc[k + 1] = 0;
					always @(posedge i_clk)
						if (i_ce)
							acc[k + 1] <= acc[k] + {{IW - (LUTB * (k + 3)) {1'b0}}, genp, {LUTB * (k + 2) {1'b0}}};
				end
			end
		end
	endgenerate
	assign w_r = (r_s[TLEN - 1] ? -acc[TLEN - 2] : acc[TLEN - 2]);
	initial o_r = 0;
	always @(posedge i_clk)
		if (i_ce)
			o_r <= w_r[(AW + BW) - 1:0];
	generate
		if (1) begin : GUNUSED
			if (IW > AW) begin : VUNUSED
				wire unused;
				assign unused = &{1'b0, w_r[(IW + BW) - 1:AW + BW]};
			end
		end
	endgenerate
endmodule
