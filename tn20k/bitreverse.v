module bitreverse (
	i_clk,
	i_reset,
	i_ce,
	i_in,
	o_out,
	o_sync
);
	parameter LGSIZE = 5;
	parameter WIDTH = 24;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire [(2 * WIDTH) - 1:0] i_in;
	output reg [(2 * WIDTH) - 1:0] o_out;
	output reg o_sync;
	reg [LGSIZE:0] wraddr;
	wire [LGSIZE:0] rdaddr;
	reg [(2 * WIDTH) - 1:0] brmem [0:(1 << (LGSIZE + 1)) - 1];
	reg in_reset;
	genvar _gv_k_1;
	generate
		for (_gv_k_1 = 0; _gv_k_1 < LGSIZE; _gv_k_1 = _gv_k_1 + 1) begin : DBL
			localparam k = _gv_k_1;
			assign rdaddr[k] = wraddr[(LGSIZE - 1) - k];
		end
	endgenerate
	assign rdaddr[LGSIZE] = !wraddr[LGSIZE];
	initial in_reset = 1'b1;
	always @(posedge i_clk)
		if (i_reset)
			in_reset <= 1'b1;
		else if (i_ce && &wraddr[LGSIZE - 1:0])
			in_reset <= 1'b0;
	initial wraddr = 0;
	always @(posedge i_clk)
		if (i_reset)
			wraddr <= 0;
		else if (i_ce) begin
			brmem[wraddr] <= i_in;
			wraddr <= wraddr + 1;
		end
	always @(posedge i_clk)
		if (i_ce)
			o_out <= brmem[rdaddr];
	initial o_sync = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			o_sync <= 1'b0;
		else if (i_ce && !in_reset)
			o_sync <= wraddr[LGSIZE - 1:0] == 0;
endmodule
