module counter_7b_up (
	Q,
	clk,
	rst
);
	output reg [6:0] Q;
	input wire clk;
	input wire rst;
	always @(posedge clk or negedge rst) begin : COUNTER
		if (!rst)
			Q <= 0;
		else if (Q == 79)
			Q <= 0;
		else
			Q <= Q + 1;
	end
endmodule
