module hwbfly (
	i_clk,
	i_reset,
	i_ce,
	i_coef,
	i_left,
	i_right,
	i_aux,
	o_left,
	o_right,
	o_aux
);
	parameter IWIDTH = 16;
	parameter CWIDTH = IWIDTH + 4;
	parameter OWIDTH = IWIDTH + 1;
	parameter SHIFT = 0;
	parameter [1:0] CKPCE = 1;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire [(2 * CWIDTH) - 1:0] i_coef;
	input wire [(2 * IWIDTH) - 1:0] i_left;
	input wire [(2 * IWIDTH) - 1:0] i_right;
	input wire i_aux;
	output wire [(2 * OWIDTH) - 1:0] o_left;
	output wire [(2 * OWIDTH) - 1:0] o_right;
	output reg o_aux;
	reg [(2 * IWIDTH) - 1:0] r_left;
	reg [(2 * IWIDTH) - 1:0] r_right;
	reg r_aux;
	reg r_aux_2;
	reg [(2 * CWIDTH) - 1:0] r_coef;
	wire signed [IWIDTH - 1:0] r_left_r;
	wire signed [IWIDTH - 1:0] r_left_i;
	wire signed [IWIDTH - 1:0] r_right_r;
	wire signed [IWIDTH - 1:0] r_right_i;
	reg signed [CWIDTH - 1:0] ir_coef_r;
	reg signed [CWIDTH - 1:0] ir_coef_i;
	reg signed [IWIDTH:0] r_sum_r;
	reg signed [IWIDTH:0] r_sum_i;
	reg signed [IWIDTH:0] r_dif_r;
	reg signed [IWIDTH:0] r_dif_i;
	reg [(2 * IWIDTH) + 2:0] leftv;
	reg [(2 * IWIDTH) + 2:0] leftvv;
	wire signed [((IWIDTH + 1) + CWIDTH) - 1:0] p_one;
	wire signed [((IWIDTH + 1) + CWIDTH) - 1:0] p_two;
	wire signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] p_three;
	wire signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] w_one;
	wire signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] w_two;
	wire aux_s;
	wire signed [IWIDTH + CWIDTH:0] left_si;
	wire signed [IWIDTH + CWIDTH:0] left_sr;
	reg [(2 * IWIDTH) + 2:0] left_saved;
	(* use_dsp48 = "no" *) reg signed [(CWIDTH + IWIDTH) + 2:0] mpy_r;
	(* use_dsp48 = "no" *) reg signed [(CWIDTH + IWIDTH) + 2:0] mpy_i;
	wire signed [OWIDTH - 1:0] rnd_left_r;
	wire signed [OWIDTH - 1:0] rnd_left_i;
	wire signed [OWIDTH - 1:0] rnd_right_r;
	wire signed [OWIDTH - 1:0] rnd_right_i;
	assign r_left_r = r_left[(2 * IWIDTH) - 1:IWIDTH];
	assign r_left_i = r_left[IWIDTH - 1:0];
	assign r_right_r = r_right[(2 * IWIDTH) - 1:IWIDTH];
	assign r_right_i = r_right[IWIDTH - 1:0];
	initial r_aux = 1'b0;
	initial r_aux_2 = 1'b0;
	always @(posedge i_clk)
		if (i_reset) begin
			r_aux <= 1'b0;
			r_aux_2 <= 1'b0;
		end
		else if (i_ce) begin
			r_aux <= i_aux;
			r_aux_2 <= r_aux;
		end
	always @(posedge i_clk)
		if (i_ce) begin
			r_left <= i_left;
			r_right <= i_right;
			r_coef <= i_coef;
			r_sum_r <= r_left_r + r_right_r;
			r_sum_i <= r_left_i + r_right_i;
			r_dif_r <= r_left_r - r_right_r;
			r_dif_i <= r_left_i - r_right_i;
			ir_coef_r <= r_coef[(2 * CWIDTH) - 1:CWIDTH];
			ir_coef_i <= r_coef[CWIDTH - 1:0];
		end
	initial leftv = 0;
	initial leftvv = 0;
	always @(posedge i_clk)
		if (i_reset) begin
			leftv <= 0;
			leftvv <= 0;
		end
		else if (i_ce) begin
			leftv <= {r_aux_2, r_sum_r, r_sum_i};
			leftvv <= leftv;
		end
	generate
		if (CKPCE <= 1) begin : CKPCE_ONE
			reg signed [CWIDTH - 1:0] p1c_in;
			reg signed [CWIDTH - 1:0] p2c_in;
			reg signed [IWIDTH:0] p1d_in;
			reg signed [IWIDTH:0] p2d_in;
			reg signed [CWIDTH:0] p3c_in;
			reg signed [IWIDTH + 1:0] p3d_in;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp_one;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_three;
			always @(posedge i_clk)
				if (i_ce) begin
					p1c_in <= ir_coef_r;
					p2c_in <= ir_coef_i;
					p1d_in <= r_dif_r;
					p2d_in <= r_dif_i;
					p3c_in <= ir_coef_i + ir_coef_r;
					p3d_in <= r_dif_r + r_dif_i;
				end
			always @(posedge i_clk)
				if (i_ce) begin
					rp_one <= p1c_in * p1d_in;
					rp_two <= p2c_in * p2d_in;
					rp_three <= p3c_in * p3d_in;
				end
			assign p_one = rp_one;
			assign p_two = rp_two;
			assign p_three = rp_three;
		end
		else if (CKPCE <= 2) begin : CKPCE_TWO
			reg [(2 * CWIDTH) - 1:0] mpy_pipe_c;
			reg [(2 * (IWIDTH + 1)) - 1:0] mpy_pipe_d;
			wire signed [CWIDTH - 1:0] mpy_pipe_vc;
			wire signed [IWIDTH:0] mpy_pipe_vd;
			reg signed [CWIDTH + 0:0] mpy_cof_sum;
			reg signed [IWIDTH + 1:0] mpy_dif_sum;
			reg mpy_pipe_v;
			reg ce_phase;
			reg signed [(CWIDTH + IWIDTH) + 0:0] mpy_pipe_out;
			reg signed [(IWIDTH + CWIDTH) + 2:0] longmpy;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp_one;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp2_one;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_three;
			assign mpy_pipe_vc = mpy_pipe_c[(2 * CWIDTH) - 1:CWIDTH];
			assign mpy_pipe_vd = mpy_pipe_d[(2 * (IWIDTH + 1)) - 1:IWIDTH + 1];
			initial ce_phase = 1'b1;
			always @(posedge i_clk)
				if (i_reset)
					ce_phase <= 1'b1;
				else if (i_ce)
					ce_phase <= 1'b0;
				else
					ce_phase <= 1'b1;
			always @(*) mpy_pipe_v = i_ce || !ce_phase;
			always @(posedge i_clk)
				if (!ce_phase) begin
					mpy_pipe_c[(2 * CWIDTH) - 1:0] <= {ir_coef_r, ir_coef_i};
					mpy_pipe_d[(2 * (IWIDTH + 1)) - 1:0] <= {r_dif_r, r_dif_i};
					mpy_cof_sum <= ir_coef_i + ir_coef_r;
					mpy_dif_sum <= r_dif_r + r_dif_i;
				end
				else if (i_ce) begin
					mpy_pipe_c[(2 * CWIDTH) - 1:0] <= {mpy_pipe_c[CWIDTH - 1:0], {CWIDTH {1'b0}}};
					mpy_pipe_d[(2 * (IWIDTH + 1)) - 1:0] <= {mpy_pipe_d[IWIDTH + 0:0], {IWIDTH + 1 {1'b0}}};
				end
			always @(posedge i_clk)
				if (i_ce)
					longmpy <= mpy_cof_sum * mpy_dif_sum;
			always @(posedge i_clk)
				if (mpy_pipe_v)
					mpy_pipe_out <= mpy_pipe_vc * mpy_pipe_vd;
			always @(posedge i_clk)
				if (!ce_phase)
					rp_one <= mpy_pipe_out;
			always @(posedge i_clk)
				if (i_ce)
					rp_two <= mpy_pipe_out;
			always @(posedge i_clk)
				if (i_ce)
					rp_three <= longmpy;
			always @(posedge i_clk)
				if (i_ce)
					rp2_one <= rp_one;
			assign p_one = rp2_one;
			assign p_two = rp_two;
			assign p_three = rp_three;
		end
		else if (CKPCE <= 2'b11) begin : CKPCE_THREE
			reg [(3 * (CWIDTH + 1)) - 1:0] mpy_pipe_c;
			reg [(3 * (IWIDTH + 2)) - 1:0] mpy_pipe_d;
			wire signed [CWIDTH:0] mpy_pipe_vc;
			wire signed [IWIDTH + 1:0] mpy_pipe_vd;
			reg mpy_pipe_v;
			reg [2:0] ce_phase;
			reg signed [(CWIDTH + IWIDTH) + 2:0] mpy_pipe_out;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp_one;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp_two;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp2_one;
			reg signed [((IWIDTH + 1) + CWIDTH) - 1:0] rp2_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_three;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_three;
			assign mpy_pipe_vc = mpy_pipe_c[(3 * (CWIDTH + 1)) - 1:2 * (CWIDTH + 1)];
			assign mpy_pipe_vd = mpy_pipe_d[(3 * (IWIDTH + 2)) - 1:2 * (IWIDTH + 2)];
			initial ce_phase = 3'b011;
			always @(posedge i_clk)
				if (i_reset)
					ce_phase <= 3'b011;
				else if (i_ce)
					ce_phase <= 3'b000;
				else if (ce_phase != 3'b011)
					ce_phase <= ce_phase + 1'b1;
			always @(*) mpy_pipe_v = i_ce || (ce_phase < 3'b010);
			always @(posedge i_clk)
				if (ce_phase == 3'b000) begin
					mpy_pipe_c[(3 * (CWIDTH + 1)) - 1:CWIDTH + 1] <= {ir_coef_r[CWIDTH - 1], ir_coef_r, ir_coef_i[CWIDTH - 1], ir_coef_i};
					mpy_pipe_c[CWIDTH:0] <= ir_coef_i + ir_coef_r;
					mpy_pipe_d[(3 * (IWIDTH + 2)) - 1:IWIDTH + 2] <= {r_dif_r[IWIDTH], r_dif_r, r_dif_i[IWIDTH], r_dif_i};
					mpy_pipe_d[IWIDTH + 1:0] <= r_dif_r + r_dif_i;
				end
				else if (mpy_pipe_v) begin
					mpy_pipe_c[(3 * (CWIDTH + 1)) - 1:0] <= {mpy_pipe_c[(2 * (CWIDTH + 1)) - 1:0], {CWIDTH + 1 {1'b0}}};
					mpy_pipe_d[(3 * (IWIDTH + 2)) - 1:0] <= {mpy_pipe_d[(2 * (IWIDTH + 2)) - 1:0], {IWIDTH + 2 {1'b0}}};
				end
			always @(posedge i_clk)
				if (mpy_pipe_v)
					mpy_pipe_out <= mpy_pipe_vc * mpy_pipe_vd;
			always @(posedge i_clk)
				if (i_ce)
					rp_one <= mpy_pipe_out[CWIDTH + IWIDTH:0];
			always @(posedge i_clk)
				if (ce_phase == 3'b000)
					rp_two <= mpy_pipe_out[CWIDTH + IWIDTH:0];
			always @(posedge i_clk)
				if (ce_phase == 3'b001)
					rp_three <= mpy_pipe_out;
			always @(posedge i_clk)
				if (i_ce) begin
					rp2_one <= rp_one;
					rp2_two <= rp_two;
					rp2_three <= rp_three;
				end
			assign p_one = rp2_one;
			assign p_two = rp2_two;
			assign p_three = rp2_three;
		end
	endgenerate
	assign w_one = {{2 {p_one[((IWIDTH + 1) + CWIDTH) - 1]}}, p_one};
	assign w_two = {{2 {p_two[((IWIDTH + 1) + CWIDTH) - 1]}}, p_two};
	assign left_sr = {{2 {left_saved[(2 * (IWIDTH + 1)) - 1]}}, left_saved[(2 * (IWIDTH + 1)) - 1:IWIDTH + 1], {CWIDTH - 2 {1'b0}}};
	assign left_si = {{2 {left_saved[IWIDTH + 0]}}, left_saved[IWIDTH + 0:0], {CWIDTH - 2 {1'b0}}};
	assign aux_s = left_saved[(2 * IWIDTH) + 2];
	initial left_saved = 0;
	initial o_aux = 1'b0;
	always @(posedge i_clk)
		if (i_reset) begin
			left_saved <= 0;
			o_aux <= 1'b0;
		end
		else if (i_ce) begin
			left_saved <= leftvv;
			o_aux <= aux_s;
		end
	always @(posedge i_clk)
		if (i_ce) begin
			mpy_r <= w_one - w_two;
			mpy_i <= (p_three - w_one) - w_two;
		end
	convround #(
		.IWID((CWIDTH + IWIDTH) + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 2)
	) do_rnd_left_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(left_sr),
		.o_val(rnd_left_r)
	);
	convround #(
		.IWID((CWIDTH + IWIDTH) + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 2)
	) do_rnd_left_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(left_si),
		.o_val(rnd_left_i)
	);
	convround #(
		.IWID((CWIDTH + IWIDTH) + 3),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 4)
	) do_rnd_right_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(mpy_r),
		.o_val(rnd_right_r)
	);
	convround #(
		.IWID((CWIDTH + IWIDTH) + 3),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 4)
	) do_rnd_right_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(mpy_i),
		.o_val(rnd_right_i)
	);
	assign o_left = {rnd_left_r, rnd_left_i};
	assign o_right = {rnd_right_r, rnd_right_i};
endmodule
