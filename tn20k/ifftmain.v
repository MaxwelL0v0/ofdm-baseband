module ifftmain (
	i_clk,
	i_reset,
	i_ce,
	i_sample,
	o_result,
	o_sync
);
	localparam IWIDTH = 12;
	localparam OWIDTH = 13;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire [23:0] i_sample;
	output reg [25:0] o_result;
	output reg o_sync;
	wire br_sync;
	wire [25:0] br_result;
	wire w_s64;
	wire [29:0] w_d64;
	ifftstage #(
		.IWIDTH(IWIDTH),
		.CWIDTH(16),
		.OWIDTH(15),
		.LGSPAN(5),
		.BFLYSHIFT(0),
		.OPT_HWMPY(1),
		.CKPCE(1),
		.COEFFILE("icmem_64.hex")
	) stage_64(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce),
		.i_sync(!i_reset),
		.i_data(i_sample),
		.o_data(w_d64),
		.o_sync(w_s64)
	);
	wire w_s32;
	wire [29:0] w_d32;
	ifftstage #(
		.IWIDTH(15),
		.CWIDTH(19),
		.OWIDTH(15),
		.LGSPAN(4),
		.BFLYSHIFT(0),
		.OPT_HWMPY(1),
		.CKPCE(1),
		.COEFFILE("icmem_32.hex")
	) stage_32(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce),
		.i_sync(w_s64),
		.i_data(w_d64),
		.o_data(w_d32),
		.o_sync(w_s32)
	);
	wire w_s16;
	wire [29:0] w_d16;
	ifftstage #(
		.IWIDTH(15),
		.CWIDTH(19),
		.OWIDTH(15),
		.LGSPAN(3),
		.BFLYSHIFT(0),
		.OPT_HWMPY(1),
		.CKPCE(1),
		.COEFFILE("icmem_16.hex")
	) stage_16(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce),
		.i_sync(w_s32),
		.i_data(w_d32),
		.o_data(w_d16),
		.o_sync(w_s16)
	);
	wire w_s8;
	wire [29:0] w_d8;
	ifftstage #(
		.IWIDTH(15),
		.CWIDTH(19),
		.OWIDTH(15),
		.LGSPAN(2),
		.BFLYSHIFT(0),
		.OPT_HWMPY(1),
		.CKPCE(1),
		.COEFFILE("icmem_8.hex")
	) stage_8(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce),
		.i_sync(w_s16),
		.i_data(w_d16),
		.o_data(w_d8),
		.o_sync(w_s8)
	);
	wire w_s4;
	wire [29:0] w_d4;
	qtrstage #(
		.IWIDTH(15),
		.OWIDTH(15),
		.LGWIDTH(6),
		.INVERSE(1),
		.SHIFT(0)
	) stage_4(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce),
		.i_sync(w_s8),
		.i_data(w_d8),
		.o_data(w_d4),
		.o_sync(w_s4)
	);
	wire w_s2;
	wire [25:0] w_d2;
	laststage #(
		.IWIDTH(15),
		.OWIDTH(13),
		.SHIFT(1)
	) stage_2(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce),
		.i_sync(w_s4),
		.i_val(w_d4),
		.o_val(w_d2),
		.o_sync(w_s2)
	);
	wire br_start;
	reg r_br_started;
	initial r_br_started = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			r_br_started <= 1'b0;
		else if (i_ce)
			r_br_started <= r_br_started || w_s2;
	assign br_start = r_br_started || w_s2;
	bitreverse #(
		.LGSIZE(6),
		.WIDTH(13)
	) revstage(
		.i_clk(i_clk),
		.i_reset(i_reset),
		.i_ce(i_ce & br_start),
		.i_in(w_d2),
		.o_out(br_result),
		.o_sync(br_sync)
	);
	initial o_sync = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			o_sync <= 1'b0;
		else if (i_ce)
			o_sync <= br_sync;
	always @(posedge i_clk)
		if (i_ce)
			o_result <= br_result;
endmodule
