module const_demapper (
	M_SEC_TDATA,
	M_SEC_TVALID,
	S_BITV_TDATA,
	S_BITV_TVALID,
	S_BITV_TREADY,
	S_MAP_TICH,
	S_MAP_TQCH,
	S_MAP_TVALID,
	ACLK1,
	ACLK2,
	ARESETN
);
	parameter DWIDTH = 13;
	parameter DDEPTH = 48;
	parameter WORD_LENGTH = 2;
	output wire M_SEC_TDATA;
	output wire M_SEC_TVALID;
	input wire [(DDEPTH * 3) - 1:0] S_BITV_TDATA;
	input wire S_BITV_TVALID;
	output wire S_BITV_TREADY;
	input wire [DWIDTH - 1:0] S_MAP_TICH;
	input wire [DWIDTH - 1:0] S_MAP_TQCH;
	input wire S_MAP_TVALID;
	input wire ACLK1;
	input wire ACLK2;
	input wire ARESETN;
	localparam SF_16QAM = 316;
	reg signed [DWIDTH - 1:0] data_i;
	reg signed [DWIDTH - 1:0] data_q;
	reg [2:0] data_count = 3'b000;
	always @(posedge ACLK1) begin : DATA_COUNT_PROC
		if (!ARESETN)
			data_count <= 0;
		else if (S_MAP_TVALID && (data_count < 4))
			data_count <= data_count + 1;
		else
			data_count <= 0;
	end
	reg map_tvalid = 1'b0;
	always @(posedge ACLK1) map_tvalid <= S_MAP_TVALID;
	reg data_out;
	reg reg_bpsk = 1'b0;
	reg [3:0] reg_qpsk = 2'b00;
	reg [3:0] reg_16qam = 4'b0000;
	reg [1:0] bitv_tdata_i [0:DDEPTH - 1];
	always @(posedge ACLK1)
		if (S_BITV_TVALID) begin : sv2v_autoblock_1
			integer n;
			for (n = 0; n <= (DDEPTH - 1); n = n + 1)
				bitv_tdata_i[n] <= S_BITV_TDATA[((DDEPTH - 1) - n) * 3+:3];
		end
	reg [2:0] map_mode;
	reg [15:0] samp_cnt = 16'b0000000000000000;
	reg s_map_tvalid_delay = 1'b0;
	always @(posedge ACLK1) s_map_tvalid_delay <= S_MAP_TVALID;
	always @(posedge ACLK2)
		if (samp_cnt == 48)
			samp_cnt <= 0;
		else if (s_map_tvalid_delay == 1'b1)
			samp_cnt <= samp_cnt + 1;
	always @(posedge ACLK1) map_mode <= bitv_tdata_i[samp_cnt];
	always @(posedge ACLK1)
		if (S_MAP_TVALID && (data_count == 0)) begin
			data_i <= S_MAP_TICH;
			data_q <= S_MAP_TQCH;
		end
	localparam INIT = 2'b00;
	localparam IDLE = 2'b01;
	localparam COMPARE = 2'b10;
	localparam SHIFTING = 2'b11;
	localparam TX_IDLE = 13'd0;
	localparam TX_SHIFTING = 13'd5;
	wire bpsk_sig;
	wire qpsk_sig;
	wire qam16_sig;
	assign bpsk_sig = (map_mode == 3'b001 ? 1'b1 : 1'b0);
	assign qpsk_sig = (map_mode == 3'b010 ? 1'b1 : 1'b0);
	assign qam16_sig = (map_mode == 3'b011 ? 1'b1 : 1'b0);
	reg [1:0] current_state;
	reg [1:0] next_state;
	always @(*) begin : NEXT_STATE_LOGIC
		case (current_state)
			INIT:
				if (S_BITV_TVALID)
					next_state = IDLE;
				else
					next_state = current_state;
			IDLE:
				if (S_MAP_TVALID)
					next_state = COMPARE;
				else
					next_state = current_state;
			COMPARE: next_state = SHIFTING;
			SHIFTING:
				if (data_count == 4)
					next_state = IDLE;
				else
					next_state = current_state;
			default: next_state = current_state;
		endcase
	end
	always @(posedge ACLK1) begin : STATE_MEMORY
		if (!ARESETN)
			current_state <= INIT;
		else
			current_state <= next_state;
	end
	reg error_sig;
	always @(posedge ACLK1) begin : DEMAPPER_PROC
		if (current_state == COMPARE)
			case (map_mode)
				3'b001:
					if (data_i > 0)
						reg_bpsk <= 1'b1;
					else
						reg_bpsk <= 1'b0;
				3'b010:
					if ((data_i > 0) && (data_q >= 0))
						reg_qpsk <= 2'b11;
					else if ((data_i > 0) && (data_q <= 0))
						reg_qpsk <= 2'b10;
					else if ((data_i <= 0) && (data_q > 0))
						reg_qpsk <= 2'b01;
					else
						reg_qpsk <= 2'b00;
				3'b011:
					if (data_i > 632) begin
						if (data_q > 632)
							reg_16qam <= 4'b1010;
						else if ((data_q > 0) && (data_q <= 632))
							reg_16qam <= 4'b1011;
						else if (data_q < (-2 * SF_16QAM))
							reg_16qam <= 4'b1000;
						else
							reg_16qam <= 4'b1001;
					end
					else if ((data_i > 0) && (data_i <= 632)) begin
						if (data_q > 632)
							reg_16qam <= 4'b1110;
						else if ((data_q > 0) && (data_q <= 632))
							reg_16qam <= 4'b1111;
						else if (data_q < (-2 * SF_16QAM))
							reg_16qam <= 4'b1100;
						else
							reg_16qam <= 4'b1101;
					end
					else if (data_i < (-2 * SF_16QAM)) begin
						if (data_q > 632)
							reg_16qam <= 4'b0010;
						else if ((data_q > 0) && (data_q <= 632))
							reg_16qam <= 4'b0011;
						else if (data_q < (-2 * SF_16QAM))
							reg_16qam <= 4'b0000;
						else
							reg_16qam <= 4'b0001;
					end
					else if (data_q > 632)
						reg_16qam <= 4'b0110;
					else if ((data_q > 0) && (data_q <= 632))
						reg_16qam <= 4'b0111;
					else if (data_q < (-2 * SF_16QAM))
						reg_16qam <= 4'b0100;
					else
						reg_16qam <= 4'b0101;
				3'b000: error_sig = 1'b1;
				3'b100: error_sig = 1'b1;
				3'b101: error_sig = 1'b1;
				3'b110: error_sig = 1'b1;
				3'b111: error_sig = 1'b1;
			endcase
	end
	reg [1:0] shift_count = 2'b11;
	always @(posedge ACLK1) begin : SHIFTING_PROC
		if (S_MAP_TVALID) begin
			if (data_count == 1)
				shift_count <= 3;
			else if (shift_count != data_count)
				shift_count <= shift_count - 1;
			else
				shift_count <= 3;
		end
		else
			shift_count <= 3;
	end
	reg valid_out = 1'b0;
	always @(posedge ACLK1)
		case (current_state)
			INIT: begin
				data_out <= 1'b0;
				valid_out <= 1'b0;
			end
			IDLE:
				if (map_tvalid) begin
					valid_out <= 1'b1;
					if (qam16_sig)
						data_out <= reg_16qam[shift_count];
					else if (qpsk_sig)
						data_out <= reg_qpsk[shift_count];
					else if (bpsk_sig)
						data_out <= reg_bpsk;
				end
				else begin
					valid_out <= 1'b0;
					data_out <= 1'b0;
				end
			COMPARE: begin
				data_out <= 1'b0;
				valid_out <= 1'b0;
			end
			SHIFTING:
				if (qam16_sig) begin
					data_out <= reg_16qam[shift_count];
					valid_out <= 1'b1;
				end
				else if (qpsk_sig) begin
					if (shift_count < 2) begin
						valid_out <= 1'b1;
						data_out <= reg_qpsk[shift_count];
					end
					else
						valid_out <= 1'b0;
				end
			default: begin
				data_out <= 1'b0;
				valid_out <= 1'b0;
			end
		endcase
	assign M_SEC_TDATA = data_out;
	assign M_SEC_TVALID = valid_out;
endmodule
