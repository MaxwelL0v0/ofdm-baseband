module const_mapper (
	M_MAP_TICH,
	M_MAP_TQCH,
	M_MAP_TVALID,
	M_MAP_TLAST,
	M_MAP_TREADY,
	S_BITV_TDATA,
	S_BITV_TVALID,
	S_BITV_TREADY,
	S_SEC_TDATA,
	S_SEC_TVALID,
	S_SEC_TREADY,
	ACLK,
	ARESETN
);
	parameter DWIDTH = 12;
	parameter DDEPTH = 48;
	parameter WORD_LENGTH = 2;
	output wire [DWIDTH - 1:0] M_MAP_TICH;
	output wire [DWIDTH - 1:0] M_MAP_TQCH;
	output wire M_MAP_TVALID;
	output wire M_MAP_TLAST;
	input wire M_MAP_TREADY;
	input wire [(DDEPTH * 3) - 1:0] S_BITV_TDATA;
	input wire S_BITV_TVALID;
	output wire S_BITV_TREADY;
	input wire S_SEC_TDATA;
	input wire S_SEC_TVALID;
	output reg S_SEC_TREADY;
	input wire ACLK;
	input wire ARESETN;
	localparam INIT = 2'b00;
	localparam IDLE = 2'b01;
	localparam SHIFTING = 2'b10;
	localparam DONE = 2'b11;
	localparam CPLEN = 16;
	localparam TX_IDLE = 16'h0000;
	localparam TX_SHIFTING = 16'h0001;
	reg data_in;
	reg [1:0] sel;
	reg [2:0] data_count;
	reg [1:0] shift_buffer1;
	reg [3:0] shift_buffer2;
	reg [5:0] shift_buffer3;
	reg [DWIDTH - 1:0] I_ch;
	reg [DWIDTH - 1:0] Q_ch;
	reg [2:0] map_mode;
	reg [2:0] map_mode_delay;
	reg [2:0] map_mode_delay2;
	reg [1:0] bitv_tdata_i [0:DDEPTH - 1];
	reg initial_frame = 1'b0;
	reg [6:0] sampCount = 6'd0;
	reg [6:0] sampCount_delay = 6'd0;
	always @(posedge ACLK)
		if (!initial_frame) begin
			if (sampCount < DDEPTH) begin
				map_mode <= bitv_tdata_i[sampCount];
				map_mode_delay <= map_mode;
				map_mode_delay2 <= map_mode_delay;
			end
			else begin
				map_mode <= 3'b000;
				map_mode_delay <= map_mode;
				map_mode_delay2 <= map_mode_delay;
			end
		end
		else if (sampCount_delay < DDEPTH) begin
			map_mode <= bitv_tdata_i[sampCount_delay];
			map_mode_delay <= map_mode;
			map_mode_delay2 <= map_mode_delay;
		end
		else begin
			map_mode <= 3'b000;
			map_mode_delay <= map_mode;
			map_mode_delay2 <= map_mode_delay;
		end
	wire bpsk_sel;
	wire qpsk_sel;
	wire qam16_sel;
	wire qam64_sel;
	assign bpsk_sel = (map_mode == 3'b001 ? 1'b1 : 1'b0);
	assign qpsk_sel = (map_mode == 3'b010 ? 1'b1 : 1'b0);
	assign qam16_sel = ((map_mode == 3'b011) || ((!initial_frame && (map_mode == 3'b000)) && (map_mode_delay == 3'b011)) ? 1'b1 : 1'b0);
	assign qam64_sel = (map_mode == 3'b100 ? 1'b1 : 1'b0);
	reg [1:0] current_state;
	reg [1:0] next_state;
	reg balloc_done = 1'b0;
	assign S_BITV_TREADY = (current_state == INIT ? 1'b1 : 1'b0);
	integer k;
	always @(ACLK)
		if (S_BITV_TVALID && S_BITV_TREADY)
			for (k = 0; k < DDEPTH; k = k + 1)
				bitv_tdata_i[k] <= S_BITV_TDATA[((DDEPTH - 1) - k) * 3+:3];
		else
			balloc_done <= 0;
	wire [6:0] sampCount_reg;
	assign sampCount_reg = (!initial_frame ? sampCount : sampCount_delay);
	reg [2:0] pos_cnt = 3'b000;
	always @(posedge ACLK)
		if (!ARESETN)
			S_SEC_TREADY <= 1'b0;
		else if (((next_state != DONE) && (sampCount_reg <= (DDEPTH - 1))) && (current_state != INIT)) begin
			if ((map_mode_delay == 3'b001) && (pos_cnt > 1))
				S_SEC_TREADY <= 1'b0;
			else if ((map_mode_delay == 3'b010) && (pos_cnt > 2))
				S_SEC_TREADY <= 1'b0;
			else
				S_SEC_TREADY <= 1'b1;
		end
		else if ((((sampCount_reg == DDEPTH) && (map_mode_delay == 3'b011)) && !initial_frame) && (pos_cnt == 0))
			S_SEC_TREADY <= 1'b1;
		else
			S_SEC_TREADY <= 1'b0;
	always @(posedge ACLK)
		if (S_SEC_TVALID && S_SEC_TREADY) begin
			data_in <= S_SEC_TDATA;
			shift_buffer1[0] <= S_SEC_TDATA;
			shift_buffer2[0] <= S_SEC_TDATA;
		end
	integer l;
	always @(posedge ACLK)
		if (S_SEC_TVALID && S_SEC_TREADY) begin
			shift_buffer1[1] = shift_buffer1[0];
			for (l = 3; l > 0; l = l - 1)
				shift_buffer2[l] = shift_buffer2[l - 1];
		end
	always @(posedge ACLK)
		if (!ARESETN)
			data_count <= 0;
		else if (S_SEC_TREADY && S_SEC_TVALID)
			data_count <= data_count + 1;
		else
			data_count <= 0;
	always @(posedge ACLK)
		if (sampCount == 50)
			initial_frame <= 1'b1;
	wire shift_done;
	wire bpsk_sig;
	wire qpsk_sig;
	wire qam16_sig;
	assign bpsk_sig = ((bpsk_sel && S_SEC_TVALID) && S_SEC_TREADY ? 1'b1 : 1'b0);
	assign qpsk_sig = ((qpsk_sel && S_SEC_TVALID) && S_SEC_TREADY ? 1'b1 : 1'b0);
	assign qam16_sig = ((qam16_sel && S_SEC_TVALID) && S_SEC_TREADY ? 1'b1 : 1'b0);
	assign shift_done = (((qpsk_sel && (data_count == 1)) || (qam16_sel && (data_count == 3))) || ((map_mode != map_mode_delay) && (data_count == 3)) ? 1'b1 : 1'b0);
	always @(*) begin : NEXT_STATE_LOGIC
		case (current_state)
			INIT:
				if (S_BITV_TVALID)
					next_state = IDLE;
				else
					next_state = current_state;
			IDLE:
				if (bpsk_sig)
					next_state = DONE;
				else if (qpsk_sel && (pos_cnt < 3))
					next_state = SHIFTING;
				else if (qam16_sig)
					next_state = SHIFTING;
				else
					next_state = current_state;
			SHIFTING:
				if (shift_done)
					next_state = DONE;
				else
					next_state = current_state;
			DONE: next_state = IDLE;
			default: next_state = current_state;
		endcase
	end
	reg [2:0] pos_cnt_3delay = 3'b000;
	always @(posedge ACLK)
		if ((current_state == INIT) && (next_state == IDLE))
			pos_cnt <= 0;
		else if (pos_cnt == 4)
			pos_cnt <= 0;
		else
			pos_cnt <= pos_cnt + 1;
	always @(posedge ACLK) begin : STATE_MEMORY
		if (!ARESETN)
			current_state <= INIT;
		else
			current_state <= next_state;
	end
	always @(*) begin : OUTPUT_LOGIC
		case (current_state)
			INIT: begin
				I_ch = TX_IDLE;
				Q_ch = TX_IDLE;
			end
			IDLE: begin
				I_ch = TX_IDLE;
				Q_ch = TX_IDLE;
			end
			SHIFTING: begin
				I_ch = TX_SHIFTING;
				Q_ch = TX_SHIFTING;
			end
			DONE:
				case (map_mode_delay2)
					3'b000: begin
						I_ch = TX_IDLE;
						Q_ch = TX_IDLE;
					end
					3'b001:
						case (data_in)
							1'b0: begin
								I_ch = -1;
								Q_ch = 0;
							end
							1'b1: begin
								I_ch = 1;
								Q_ch = 0;
							end
						endcase
					3'b010:
						case (shift_buffer1)
							2'b00: begin
								I_ch = -707;
								Q_ch = -707;
							end
							2'b01: begin
								I_ch = -707;
								Q_ch = 707;
							end
							2'b10: begin
								I_ch = 707;
								Q_ch = -707;
							end
							2'b11: begin
								I_ch = 707;
								Q_ch = 707;
							end
							default: begin
								I_ch = 4;
								Q_ch = 4;
							end
						endcase
					3'b011:
						case (shift_buffer2)
							4'b0000: begin
								I_ch = -948;
								Q_ch = -948;
							end
							4'b0001: begin
								I_ch = -948;
								Q_ch = -316;
							end
							4'b0010: begin
								I_ch = -948;
								Q_ch = 948;
							end
							4'b0011: begin
								I_ch = -948;
								Q_ch = 316;
							end
							4'b0100: begin
								I_ch = -316;
								Q_ch = -948;
							end
							4'b0101: begin
								I_ch = -316;
								Q_ch = -316;
							end
							4'b0110: begin
								I_ch = -316;
								Q_ch = 948;
							end
							4'b0111: begin
								I_ch = -316;
								Q_ch = 316;
							end
							4'b1000: begin
								I_ch = 948;
								Q_ch = -948;
							end
							4'b1001: begin
								I_ch = 948;
								Q_ch = -316;
							end
							4'b1010: begin
								I_ch = 948;
								Q_ch = 948;
							end
							4'b1011: begin
								I_ch = 948;
								Q_ch = 316;
							end
							4'b1100: begin
								I_ch = 316;
								Q_ch = -948;
							end
							4'b1101: begin
								I_ch = 316;
								Q_ch = -316;
							end
							4'b1110: begin
								I_ch = 316;
								Q_ch = 948;
							end
							4'b1111: begin
								I_ch = 316;
								Q_ch = 316;
							end
							default: begin
								I_ch = 4;
								Q_ch = 4;
							end
						endcase
					3'b100: begin
						I_ch = TX_IDLE;
						Q_ch = TX_IDLE;
					end
					3'b101: begin
						I_ch = TX_IDLE;
						Q_ch = TX_IDLE;
					end
					3'b110: begin
						I_ch = TX_IDLE;
						Q_ch = TX_IDLE;
					end
					3'b111: begin
						I_ch = TX_IDLE;
						Q_ch = TX_IDLE;
					end
				endcase
		endcase
	end
	wire done_sig;
	reg map_tlast;
	assign done_sig = (current_state == DONE ? 1'b1 : 1'b0);
	always @(posedge ACLK)
		if (!ARESETN)
			sampCount <= 0;
		else if ((current_state != INIT) && (pos_cnt == 4))
			sampCount <= sampCount + 1;
		else if ((sampCount > (DDEPTH + 1)) && M_MAP_TREADY)
			sampCount <= 0;
	always @(posedge ACLK)
		if (!ARESETN)
			sampCount_delay <= 0;
		else if ((current_state != INIT) && (pos_cnt == 3))
			sampCount_delay <= sampCount_delay + 1;
		else if ((sampCount_delay > (DDEPTH + 1)) && M_MAP_TREADY)
			sampCount_delay <= 0;
	assign M_MAP_TLAST = (done_sig && (sampCount_reg == DDEPTH) ? 1'b1 : 1'b0);
	assign M_MAP_TVALID = (done_sig && (sampCount_reg <= DDEPTH) ? 1'b1 : 1'b0);
	assign M_MAP_TICH = I_ch;
	assign M_MAP_TQCH = Q_ch;
endmodule
