module laststage (
	i_clk,
	i_reset,
	i_ce,
	i_sync,
	i_val,
	o_val,
	o_sync
);
	parameter IWIDTH = 16;
	parameter OWIDTH = IWIDTH + 1;
	parameter SHIFT = 0;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire i_sync;
	input wire [(2 * IWIDTH) - 1:0] i_val;
	output wire [(2 * OWIDTH) - 1:0] o_val;
	output reg o_sync;
	reg signed [IWIDTH - 1:0] m_r;
	reg signed [IWIDTH - 1:0] m_i;
	wire signed [IWIDTH - 1:0] i_r;
	wire signed [IWIDTH - 1:0] i_i;
	reg signed [IWIDTH:0] rnd_r;
	reg signed [IWIDTH:0] rnd_i;
	reg signed [IWIDTH:0] sto_r;
	reg signed [IWIDTH:0] sto_i;
	reg wait_for_sync;
	reg stage;
	reg [1:0] sync_pipe;
	wire signed [OWIDTH - 1:0] o_r;
	wire signed [OWIDTH - 1:0] o_i;
	assign i_r = i_val[(2 * IWIDTH) - 1:IWIDTH];
	assign i_i = i_val[IWIDTH - 1:0];
	initial wait_for_sync = 1'b1;
	initial stage = 1'b0;
	always @(posedge i_clk)
		if (i_reset) begin
			wait_for_sync <= 1'b1;
			stage <= 1'b0;
		end
		else if ((i_ce && (!wait_for_sync || i_sync)) && !stage) begin
			wait_for_sync <= 1'b0;
			stage <= 1'b1;
		end
		else if (i_ce)
			stage <= 1'b0;
	initial sync_pipe = 0;
	always @(posedge i_clk)
		if (i_reset)
			sync_pipe <= 0;
		else if (i_ce)
			sync_pipe <= {sync_pipe[0], i_sync};
	initial o_sync = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			o_sync <= 1'b0;
		else if (i_ce)
			o_sync <= sync_pipe[1];
	always @(posedge i_clk)
		if (i_ce) begin
			if (!stage) begin
				m_r <= i_r;
				m_i <= i_i;
				rnd_r <= sto_r;
				rnd_i <= sto_i;
			end
			else begin
				rnd_r <= m_r + i_r;
				rnd_i <= m_i + i_i;
				sto_r <= m_r - i_r;
				sto_i <= m_i - i_i;
			end
		end
	convround #(
		.IWID(IWIDTH + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT)
	) do_rnd_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(rnd_r),
		.o_val(o_r)
	);
	convround #(
		.IWID(IWIDTH + 1),
		.OWID(OWIDTH),
		.SHIFT(SHIFT)
	) do_rnd_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(rnd_i),
		.o_val(o_i)
	);
	assign o_val = {o_r, o_i};
endmodule
