module butterfly (
	i_clk,
	i_reset,
	i_ce,
	i_coef,
	i_left,
	i_right,
	i_aux,
	o_left,
	o_right,
	o_aux
);
	parameter IWIDTH = 16;
	parameter CWIDTH = 20;
	parameter OWIDTH = 17;
	parameter SHIFT = 0;
	parameter CKPCE = 1;
	localparam MXMPYBITS = ((IWIDTH + 2) > (CWIDTH + 1) ? CWIDTH + 1 : IWIDTH + 2);
	localparam MPYDELAY = ((MXMPYBITS + 1) / 2) + 2;
	localparam LCLDELAY = (CKPCE == 1 ? MPYDELAY : (CKPCE == 2 ? (MPYDELAY / 2) + 2 : (MPYDELAY / 3) + 2));
	localparam LGDELAY = (MPYDELAY > 64 ? 7 : (MPYDELAY > 32 ? 6 : (MPYDELAY > 16 ? 5 : (MPYDELAY > 8 ? 4 : (MPYDELAY > 4 ? 3 : 2)))));
	localparam AUXLEN = LCLDELAY + 3;
	localparam MPYREMAINDER = MPYDELAY - (CKPCE * (MPYDELAY / CKPCE));
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire [(2 * CWIDTH) - 1:0] i_coef;
	input wire [(2 * IWIDTH) - 1:0] i_left;
	input wire [(2 * IWIDTH) - 1:0] i_right;
	input wire i_aux;
	output wire [(2 * OWIDTH) - 1:0] o_left;
	output wire [(2 * OWIDTH) - 1:0] o_right;
	output reg o_aux;
	reg [(2 * IWIDTH) - 1:0] r_left;
	reg [(2 * IWIDTH) - 1:0] r_right;
	reg [(2 * CWIDTH) - 1:0] r_coef;
	reg [(2 * CWIDTH) - 1:0] r_coef_2;
	wire signed [IWIDTH - 1:0] r_left_r;
	wire signed [IWIDTH - 1:0] r_left_i;
	wire signed [IWIDTH - 1:0] r_right_r;
	wire signed [IWIDTH - 1:0] r_right_i;
	reg signed [IWIDTH:0] r_sum_r;
	reg signed [IWIDTH:0] r_sum_i;
	reg signed [IWIDTH:0] r_dif_r;
	reg signed [IWIDTH:0] r_dif_i;
	reg [LGDELAY - 1:0] fifo_addr;
	wire [LGDELAY - 1:0] fifo_read_addr;
	reg [(2 * IWIDTH) + 1:0] fifo_left [0:(1 << LGDELAY) - 1];
	wire signed [CWIDTH - 1:0] ir_coef_r;
	wire signed [CWIDTH - 1:0] ir_coef_i;
	wire signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] p_one;
	wire signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] p_two;
	wire signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] p_three;
	wire signed [IWIDTH + CWIDTH:0] fifo_i;
	wire signed [IWIDTH + CWIDTH:0] fifo_r;
	reg [(2 * IWIDTH) + 1:0] fifo_read;
	reg signed [(CWIDTH + IWIDTH) + 2:0] mpy_r;
	reg signed [(CWIDTH + IWIDTH) + 2:0] mpy_i;
	wire signed [OWIDTH - 1:0] rnd_left_r;
	wire signed [OWIDTH - 1:0] rnd_left_i;
	wire signed [OWIDTH - 1:0] rnd_right_r;
	wire signed [OWIDTH - 1:0] rnd_right_i;
	wire signed [(CWIDTH + IWIDTH) + 2:0] left_sr;
	wire signed [(CWIDTH + IWIDTH) + 2:0] left_si;
	reg [AUXLEN - 1:0] aux_pipeline;
	assign r_left_r = r_left[(2 * IWIDTH) - 1:IWIDTH];
	assign r_left_i = r_left[IWIDTH - 1:0];
	assign r_right_r = r_right[(2 * IWIDTH) - 1:IWIDTH];
	assign r_right_i = r_right[IWIDTH - 1:0];
	assign ir_coef_r = r_coef_2[(2 * CWIDTH) - 1:CWIDTH];
	assign ir_coef_i = r_coef_2[CWIDTH - 1:0];
	assign fifo_read_addr = fifo_addr - LCLDELAY[LGDELAY - 1:0];
	always @(posedge i_clk)
		if (i_ce) begin
			r_left <= i_left;
			r_right <= i_right;
			r_coef <= i_coef;
			r_sum_r <= r_left_r + r_right_r;
			r_sum_i <= r_left_i + r_right_i;
			r_dif_r <= r_left_r - r_right_r;
			r_dif_i <= r_left_i - r_right_i;
			r_coef_2 <= r_coef;
		end
	initial fifo_addr = 0;
	always @(posedge i_clk)
		if (i_reset)
			fifo_addr <= 0;
		else if (i_ce)
			fifo_addr <= fifo_addr + 1;
	always @(posedge i_clk)
		if (i_ce)
			fifo_left[fifo_addr] <= {r_sum_r, r_sum_i};
	generate
		if (CKPCE <= 1) begin : CKPCE_ONE
			wire [CWIDTH:0] p3c_in;
			wire [IWIDTH + 1:0] p3d_in;
			assign p3c_in = ir_coef_i + ir_coef_r;
			assign p3d_in = r_dif_r + r_dif_i;
			longbimpy #(
				.IAW(CWIDTH + 1),
				.IBW(IWIDTH + 2)
			) p1(
				.i_clk(i_clk),
				.i_ce(i_ce),
				.i_a_unsorted({ir_coef_r[CWIDTH - 1], ir_coef_r}),
				.i_b_unsorted({r_dif_r[IWIDTH], r_dif_r}),
				.o_r(p_one)
			);
			longbimpy #(
				.IAW(CWIDTH + 1),
				.IBW(IWIDTH + 2)
			) p2(
				.i_clk(i_clk),
				.i_ce(i_ce),
				.i_a_unsorted({ir_coef_i[CWIDTH - 1], ir_coef_i}),
				.i_b_unsorted({r_dif_i[IWIDTH], r_dif_i}),
				.o_r(p_two)
			);
			longbimpy #(
				.IAW(CWIDTH + 1),
				.IBW(IWIDTH + 2)
			) p3(
				.i_clk(i_clk),
				.i_ce(i_ce),
				.i_a_unsorted(p3c_in),
				.i_b_unsorted(p3d_in),
				.o_r(p_three)
			);
		end
		else if (CKPCE == 2) begin : CKPCE_TWO
			reg [(2 * CWIDTH) - 1:0] mpy_pipe_c;
			reg [(2 * (IWIDTH + 1)) - 1:0] mpy_pipe_d;
			wire signed [CWIDTH - 1:0] mpy_pipe_vc;
			wire signed [IWIDTH:0] mpy_pipe_vd;
			reg signed [CWIDTH + 0:0] mpy_cof_sum;
			reg signed [IWIDTH + 1:0] mpy_dif_sum;
			reg mpy_pipe_v;
			reg ce_phase;
			wire signed [(CWIDTH + IWIDTH) + 2:0] mpy_pipe_out;
			wire signed [(IWIDTH + CWIDTH) + 2:0] longmpy;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_one;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_three;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_one;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_three;
			assign mpy_pipe_vc = mpy_pipe_c[(2 * CWIDTH) - 1:CWIDTH];
			assign mpy_pipe_vd = mpy_pipe_d[(2 * (IWIDTH + 1)) - 1:IWIDTH + 1];
			initial ce_phase = 1'b0;
			always @(posedge i_clk)
				if (i_reset)
					ce_phase <= 1'b0;
				else if (i_ce)
					ce_phase <= 1'b1;
				else
					ce_phase <= 1'b0;
			always @(*) mpy_pipe_v = i_ce || ce_phase;
			always @(posedge i_clk)
				if (ce_phase) begin
					mpy_pipe_c[(2 * CWIDTH) - 1:0] <= {ir_coef_r, ir_coef_i};
					mpy_pipe_d[(2 * (IWIDTH + 1)) - 1:0] <= {r_dif_r, r_dif_i};
					mpy_cof_sum <= ir_coef_i + ir_coef_r;
					mpy_dif_sum <= r_dif_r + r_dif_i;
				end
				else if (i_ce) begin
					mpy_pipe_c[(2 * CWIDTH) - 1:0] <= {mpy_pipe_c[CWIDTH - 1:0], {CWIDTH {1'b0}}};
					mpy_pipe_d[(2 * (IWIDTH + 1)) - 1:0] <= {mpy_pipe_d[IWIDTH + 0:0], {IWIDTH + 1 {1'b0}}};
				end
			longbimpy #(
				.IAW(CWIDTH + 1),
				.IBW(IWIDTH + 2)
			) mpy0(
				.i_clk(i_clk),
				.i_ce(mpy_pipe_v),
				.i_a_unsorted(mpy_cof_sum),
				.i_b_unsorted(mpy_dif_sum),
				.o_r(longmpy)
			);
			longbimpy #(
				.IAW(CWIDTH + 1),
				.IBW(IWIDTH + 2)
			) mpy1(
				.i_clk(i_clk),
				.i_ce(mpy_pipe_v),
				.i_a_unsorted({mpy_pipe_vc[CWIDTH - 1], mpy_pipe_vc}),
				.i_b_unsorted({mpy_pipe_vd[IWIDTH], mpy_pipe_vd}),
				.o_r(mpy_pipe_out)
			);
			always @(posedge i_clk)
				if ((i_ce && !MPYDELAY[0]) || (ce_phase && MPYDELAY[0]))
					rp_one <= mpy_pipe_out;
			always @(posedge i_clk)
				if ((i_ce && MPYDELAY[0]) || (ce_phase && !MPYDELAY[0]))
					rp_two <= mpy_pipe_out;
			always @(posedge i_clk)
				if (i_ce)
					rp_three <= longmpy;
			always @(posedge i_clk)
				if (i_ce) begin
					rp2_one <= rp_one;
					rp2_two <= rp_two;
					rp2_three <= rp_three;
				end
			assign p_one = rp2_one;
			assign p_two = (!MPYDELAY[0] ? rp2_two : rp_two);
			assign p_three = (MPYDELAY[0] ? rp_three : rp2_three);
			wire [(2 * ((IWIDTH + CWIDTH) + 3)) - 1:0] unused;
			assign unused = {rp2_two, rp2_three};
		end
		else if (CKPCE <= 3) begin : CKPCE_THREE
			reg [(3 * (CWIDTH + 1)) - 1:0] mpy_pipe_c;
			reg [(3 * (IWIDTH + 2)) - 1:0] mpy_pipe_d;
			wire signed [CWIDTH:0] mpy_pipe_vc;
			wire signed [IWIDTH + 1:0] mpy_pipe_vd;
			reg mpy_pipe_v;
			reg [2:0] ce_phase;
			wire signed [(CWIDTH + IWIDTH) + 2:0] mpy_pipe_out;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_one;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp_three;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_one;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_two;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp2_three;
			reg signed [((IWIDTH + 2) + (CWIDTH + 1)) - 1:0] rp3_one;
			assign mpy_pipe_vc = mpy_pipe_c[(3 * (CWIDTH + 1)) - 1:2 * (CWIDTH + 1)];
			assign mpy_pipe_vd = mpy_pipe_d[(3 * (IWIDTH + 2)) - 1:2 * (IWIDTH + 2)];
			initial ce_phase = 3'b011;
			always @(posedge i_clk)
				if (i_reset)
					ce_phase <= 3'b011;
				else if (i_ce)
					ce_phase <= 3'b000;
				else if (ce_phase != 3'b011)
					ce_phase <= ce_phase + 1'b1;
			always @(*) mpy_pipe_v = i_ce || (ce_phase < 3'b010);
			always @(posedge i_clk)
				if (ce_phase == 3'b000) begin
					mpy_pipe_c[(3 * (CWIDTH + 1)) - 1:CWIDTH + 1] <= {ir_coef_r[CWIDTH - 1], ir_coef_r, ir_coef_i[CWIDTH - 1], ir_coef_i};
					mpy_pipe_c[CWIDTH:0] <= ir_coef_i + ir_coef_r;
					mpy_pipe_d[(3 * (IWIDTH + 2)) - 1:IWIDTH + 2] <= {r_dif_r[IWIDTH], r_dif_r, r_dif_i[IWIDTH], r_dif_i};
					mpy_pipe_d[IWIDTH + 1:0] <= r_dif_r + r_dif_i;
				end
				else if (mpy_pipe_v) begin
					mpy_pipe_c[(3 * (CWIDTH + 1)) - 1:0] <= {mpy_pipe_c[(2 * (CWIDTH + 1)) - 1:0], {CWIDTH + 1 {1'b0}}};
					mpy_pipe_d[(3 * (IWIDTH + 2)) - 1:0] <= {mpy_pipe_d[(2 * (IWIDTH + 2)) - 1:0], {IWIDTH + 2 {1'b0}}};
				end
			longbimpy #(
				.IAW(CWIDTH + 1),
				.IBW(IWIDTH + 2)
			) mpy(
				.i_clk(i_clk),
				.i_ce(mpy_pipe_v),
				.i_a_unsorted(mpy_pipe_vc),
				.i_b_unsorted(mpy_pipe_vd),
				.o_r(mpy_pipe_out)
			);
			always @(posedge i_clk)
				if (MPYREMAINDER == 0) begin
					if (i_ce)
						rp_two <= mpy_pipe_out;
					else if (ce_phase == 3'b000)
						rp_three <= mpy_pipe_out;
					else if (ce_phase == 3'b001)
						rp_one <= mpy_pipe_out;
				end
				else if (MPYREMAINDER == 1) begin
					if (i_ce)
						rp_one <= mpy_pipe_out;
					else if (ce_phase == 3'b000)
						rp_two <= mpy_pipe_out;
					else if (ce_phase == 3'b001)
						rp_three <= mpy_pipe_out;
				end
				else if (i_ce)
					rp_three <= mpy_pipe_out;
				else if (ce_phase == 3'b000)
					rp_one <= mpy_pipe_out;
				else if (ce_phase == 3'b001)
					rp_two <= mpy_pipe_out;
			always @(posedge i_clk)
				if (i_ce) begin
					rp2_one <= rp_one;
					rp2_two <= rp_two;
					rp2_three <= (MPYREMAINDER == 2 ? mpy_pipe_out : rp_three);
					rp3_one <= (MPYREMAINDER == 0 ? rp2_one : rp_one);
				end
			assign p_one = rp3_one;
			assign p_two = rp2_two;
			assign p_three = rp2_three;
		end
	endgenerate
	assign fifo_r = {{2 {fifo_read[(2 * (IWIDTH + 1)) - 1]}}, fifo_read[(2 * (IWIDTH + 1)) - 1:IWIDTH + 1], {CWIDTH - 2 {1'b0}}};
	assign fifo_i = {{2 {fifo_read[IWIDTH + 0]}}, fifo_read[IWIDTH + 0:0], {CWIDTH - 2 {1'b0}}};
	assign left_sr = {{2 {fifo_r[IWIDTH + CWIDTH]}}, fifo_r};
	assign left_si = {{2 {fifo_i[IWIDTH + CWIDTH]}}, fifo_i};
	convround #(
		.IWID((CWIDTH + IWIDTH) + 3),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 4)
	) do_rnd_left_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(left_sr),
		.o_val(rnd_left_r)
	);
	convround #(
		.IWID((CWIDTH + IWIDTH) + 3),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 4)
	) do_rnd_left_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(left_si),
		.o_val(rnd_left_i)
	);
	convround #(
		.IWID((CWIDTH + IWIDTH) + 3),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 4)
	) do_rnd_right_r(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(mpy_r),
		.o_val(rnd_right_r)
	);
	convround #(
		.IWID((CWIDTH + IWIDTH) + 3),
		.OWID(OWIDTH),
		.SHIFT(SHIFT + 4)
	) do_rnd_right_i(
		.i_clk(i_clk),
		.i_ce(i_ce),
		.i_val(mpy_i),
		.o_val(rnd_right_i)
	);
	always @(posedge i_clk)
		if (i_ce) begin
			fifo_read <= fifo_left[fifo_read_addr];
			mpy_r <= p_one - p_two;
			mpy_i <= (p_three - p_one) - p_two;
		end
	initial aux_pipeline = 0;
	always @(posedge i_clk)
		if (i_reset)
			aux_pipeline <= 0;
		else if (i_ce)
			aux_pipeline <= {aux_pipeline[AUXLEN - 2:0], i_aux};
	initial o_aux = 1'b0;
	always @(posedge i_clk)
		if (i_reset)
			o_aux <= 1'b0;
		else if (i_ce)
			o_aux <= aux_pipeline[AUXLEN - 1];
	assign o_left = {rnd_left_r, rnd_left_i};
	assign o_right = {rnd_right_r, rnd_right_i};
endmodule
