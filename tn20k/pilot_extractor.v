module pilot_extractor (
	M_MAP_TICH,
	M_MAP_TQCH,
	M_MAP_TVALID,
	SAMP_ICH,
	SAMP_QCH,
	SAMP_SYNC,
	ACLK,
	ARESETN
);
	parameter DWIDTH = 13;
	output wire [DWIDTH - 1:0] M_MAP_TICH;
	output wire [DWIDTH - 1:0] M_MAP_TQCH;
	output wire M_MAP_TVALID;
	input wire [DWIDTH - 1:0] SAMP_ICH;
	input wire [DWIDTH - 1:0] SAMP_QCH;
	input wire SAMP_SYNC;
	input wire ACLK;
	input wire ARESETN;
	reg [DWIDTH - 1:0] samp_re_i;
	reg [DWIDTH - 1:0] samp_im_i;
	reg frame_valid;
	reg map_valid = 1'b0;
	always @(posedge ACLK) begin
		if (!ARESETN)
			frame_valid <= 1'b0;
		else if (SAMP_SYNC == 1'b1)
			frame_valid <= 1'b1;
		samp_re_i <= SAMP_ICH;
		samp_im_i <= SAMP_QCH;
	end
	reg [6:0] dataCount;
	always @(posedge ACLK)
		if (SAMP_SYNC)
			dataCount <= 0;
		else
			dataCount <= dataCount + 1;
	localparam NUMLGSC = 5;
	always @(posedge ACLK)
		if (frame_valid) begin
			if ((dataCount >= 4) && (dataCount < 9))
				map_valid = 1'b1;
			else if ((dataCount > 9) && (dataCount < 17))
				map_valid = 1'b1;
			else if ((dataCount > 32) && (dataCount < 39))
				map_valid = 1'b1;
			else if ((dataCount > 39) && (dataCount < 46))
				map_valid = 1'b1;
			else if ((dataCount > 46) && (dataCount < 53))
				map_valid = 1'b1;
			else if ((dataCount > 53) && (dataCount < 67))
				map_valid = 1'b1;
			else if ((dataCount > 67) && (dataCount < 73))
				map_valid = 1'b1;
			else
				map_valid = 1'b0;
		end
	localparam SF = 8;
	assign M_MAP_TVALID = map_valid;
	assign M_MAP_TICH = samp_re_i * SF;
	assign M_MAP_TQCH = samp_im_i * SF;
endmodule
