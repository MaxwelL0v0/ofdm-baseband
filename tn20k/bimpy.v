module bimpy (
	i_clk,
	i_reset,
	i_ce,
	i_a,
	i_b,
	o_r
);
	parameter BW = 18;
	localparam LUTB = 2;
	input wire i_clk;
	input wire i_reset;
	input wire i_ce;
	input wire [1:0] i_a;
	input wire [BW - 1:0] i_b;
	output reg [(BW + LUTB) - 1:0] o_r;
	wire [(BW + LUTB) - 2:0] w_r;
	wire [(BW + LUTB) - 3:1] c;
	assign w_r = {(i_a[1] ? i_b : {BW {1'b0}}), 1'b0} ^ {1'b0, (i_a[0] ? i_b : {BW {1'b0}})};
	assign c = {(i_a[1] ? i_b[BW - 2:0] : {BW - 1 {1'b0}})} & (i_a[0] ? i_b[BW - 1:1] : {BW - 1 {1'b0}});
	initial o_r = 0;
	always @(posedge i_clk)
		if (i_reset)
			o_r <= 0;
		else if (i_ce)
			o_r <= w_r + {c, 2'b00};
endmodule
