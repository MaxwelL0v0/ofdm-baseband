# Proyecto OFDM en Banda Base en Verilog

## Descripción del Proyecto
Este proyecto implementa un sistema OFDM (Orthogonal Frequency Division Multiplexing) en banda base utilizando el lenguaje de descripción de hardware (HDL) Verilog. OFDM es una técnica de modulación que divide el ancho de banda disponible en múltiples subportadoras ortogonales, lo que permite una transmisión de datos eficiente y resistente a la interferencia.

## Módulo OFDM en HDL
El módulo OFDM implementado consta de un transmisor y un receptor, cuya estructura modular interna se puede observar en el siguiente diagrama de bloques:
![Diagrama de bloques del módulo OFDM](/doc/images/sf_diagram.png)

## Estructura del Proyecto
- **`/src/`**: Contiene todos los archivos fuente Verilog del proyecto, junto con los archivos de inicialización de registros de memoria.
- **`/test_scripts/`**: Scripts para generación, testeo y verificación de módulos.
- **`/doc/`**: Documentación relacionada con el proyecto.
- **`/gowin/`**: Archivos de restauración para efectuar el proceso de síntesis, place and route y programación de la FPGA con la IDE de Gowin.
- **`/tn20k/`**: Archivos requeridos para efectuar el proceso de síntesis, place and route con yosys (en testeo).

## Requisitos de sistema para simulación del HDL
Las herramientas y el entorno necesarios para compilar y simular cada módulo disponible en el proyecto son las siguientes:
- [Icarus Verilog](https://bleyer.org/icarus/) (Compilador de Verilog HDL).
- [vvp](https://man.archlinux.org/man/vvp.1.en)  (Simulador de ejecutable generado, viene integrado en Icarus Verilog).
- [GTKWave](https://gtkwave.sourceforge.net/) (Visualizador formas de onda digitales almacenadas en archivos [*fst*](https://docs.rs/fstapi/latest/fstapi/index.html)).
- Emulador de terminal con intérprete de comandos de [*bash*](https://es.wikipedia.org/wiki/Bash).
- [Cocotb](https://www.cocotb.org/) (Librería de python opcional, utilizada para automatizar la generación de resultados).

## Requisitos de sistema para la síntesis del HDL
Para efectuar el proceso de síntesis en la placa FPGA *Tang Nano 20K* con la [IDE de Gowin](https://www.gowinsemi.com/en/support/home/), sólo debe instalar el software y restaurar los archivos del proyecto. Más abajo se entregan las instrucciones pertinentes. 

El proyecto está siendo modificado para poder efectuar la síntesis con herramientas de código abierto, para lo cual se necesita tener instalado lo siguiente:
- [sv2v](https://github.com/zachjs/sv2v) (Software para convertir algunos módulos de SystemVerilog IEEE 1800-2017 a Verilog IEEE 1364-2005).
- [Yosys](https://github.com/YosysHQ/yosys) (Sofware de síntesis del diseño en Verilog).
- [nextpnr](https://github.com/YosysHQ/nextpnr) (Sofware para el proceso de place and route).
- [openFPGALoader](https://github.com/trabucayre/openFPGALoader) (Sofware para programar la FPGA).

## Instrucciones de compilación y simulación con Icarus Verilog
Para la compilación y simulación utilizando sólo Icarus Verilog y GTKWave, debe ubicarse dentro del directorio **`/src/ofdm_module/`** y ubicar el archivo testbench escrito en verilog, nombrado `top_tb.v`. Dentro de la misma carpeta, existe un archivo llamado `simulate.sh` para facilitar la compilación y simulación del módulo bajo prueba (`top.v`):
```bash
#!/bin/bash
find ./ -name "*.vvp" | xargs rm 2>/dev/null
module_name=$(cat $1 | grep module | head -1 | awk '{print $2}')
echo  $module_name
find ./ -name "*.fst" | xargs rm 2>/dev/null

#se compila el módulo
iverilog -o $module_name.vvp -g2012 $1
#Se simula el ejecutable generado por el compilador
vvp $module_name.vvp -fst
#Se elimina el ejecutable
rm ./$module_name.vvp
#Se abre el .fst con gtkwave
gtkwave $module_name.fst > /dev/null 2>&1 & disown
```
  Debe otorgar permisos de ejecución mediante la siguiente orden:
```bash
$ chmod +x ./simulate.sh
```
Estando en el directorio, sólo debe ejecutar desde la consola:
```bash
$ ./simulate.sh top_tb.v
```

Si la simulación se ha efectuado correctamente, debería obtener el siguiente mensaje por consola:
```bash
FST info: dumpfile tb.fst opened for output.
Test complete.
top_tb.v:28: $finish called at 40500000 (1ps)
```
El archivo `tb.fst` generado se abrirá en GTKWave y podrá visualizar todas la señales del testbench junto con las correspondientes a cada submódulo. 

## Instrucciones de compilación y simulación utilizando cocotb

Los resultados presentados en el manuscrito son fácilmente replicables utilizando la librería cocotb. Para ello, sólo debe ingresar al directorio **`/test_scripts/cocotb/`** y desde consola ejecutar el comando `make`. Debe tener instalada la librería [matplotlib](https://matplotlib.org/) en su entorno virtual de python para compilar correctamente el código. Si todo se ha simulado sin errores, debería obtener los gráficos por pantalla y una copia en formato .png de cada uno almacenada en el directorio **`/test_scripts/cocotb/images/`**. 

## Instrucciones de síntesis, place and route y programación de la FPGA

De momento sólo se deja el proceso de síntesis del módulo OFDM (transmisor y receptor) con la IDE de Gowin, la rutina de bit loading se encuentra en proceso de optimización debido a la alta ocupación de recursos. 

Para efectuar los procesos de síntesis, place and route y programación de la FPGA, se sugiere proceder con la IDE de Gowin. Se ha dejado un archivo de restauración del proyecto llamado `ofdm_bb_tx_tn20k.gar` ubicado en la carpeta **`/gowin/`**. Una vez restaurado el proyecto desde la IDE, se podrá acceder a todos los archivos originados por los procesos de síntesis y place and route efectuados previamente en este mismo software. Si no quiere volver a efectuar estos procesos, dentro de la carpeta que se haya seleccionado para alojar el proyecto, debe ubicar el archivo bitstream `ao_0.fs` en **`/impl/pnr/`**, con el cuál ya podrá programar la FPGA utilizando openFPGALoader:
```bash
$ sudo openFPGALoader -b tangnano20k ao_0.fs 
```
Antes de ingresar este comando, debe asegurarse de tener correctamente conectada la placa Tang Nano 20K a un puerto USB disponible.

Los resultados podrán ser capturados con un analizador lógico conectado en los pines designados en el archivo de restricciones físicas (`tangnano20k.cst`).
