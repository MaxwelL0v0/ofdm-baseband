module frame_ctrl #(
    parameter DWIDTH = 16,
    parameter DDEPTH = 48
)(
    output reg start_bl,
    output reg start_tx,
    output reg [2:0] bitv_tdata[0:DDEPTH-1],
    input wire clk,
    input wire reset,
    input wire [DWIDTH-1:0] bit_vector[0:63],
    input wire bit_vector_tvalid,
    input wire tx_end
);
    //Register bit_vector
    integer k;
    reg [DWIDTH-1:0] bitv_i [0:DDEPTH];
    always @(posedge clk) begin
        if (bit_vector_tvalid) begin
            for (k = 5; k<10; k = k+1) bitv_i[k-5] <= bit_vector[k];
            for (k = 11; k<24; k = k+1) bitv_i[k-6] <= bit_vector[k];
            for (k = 25; k<31; k = k+1) bitv_i[k-7] <= bit_vector[k];
            for (k = 32; k<38; k = k+1) bitv_i[k-8] <= bit_vector[k];
            for (k = 39; k<52; k = k+1) bitv_i[k-9] <= bit_vector[k];
            for (k = 53; k<58; k = k+1) bitv_i[k-10] <= bit_vector[k];
        end
    end
    reg bitv_tvalid_i;
    always @(posedge clk) bitv_tvalid_i <= bit_vector_tvalid;

    //State codification
    localparam BL = 1'b0;
    localparam TX = 1'b1;
    //State registers
    reg current_state, next_state;
    //Next state logic
    always @(*)
    begin : NEXT_STATE_LOGIC
        case (current_state)
            BL : begin
                if (bitv_tvalid_i)
                    next_state = TX;
                else
                    next_state = current_state;
            end
            TX : begin
                if (tx_end)
                    next_state = BL;
                else
                    next_state = current_state;
            end
            default:
                    next_state = current_state;
         endcase
    end
    //State memory
    always @(posedge clk)
    begin : STATE_MEMORY
        if (reset)
            current_state <= BL;
        else
            current_state <= next_state;
    end
    //Counter
    reg [7:0] cnt = {8'b0};
    always @(posedge clk)
        if (next_state != current_state)
            cnt <= 0;
        else if (cnt < 15)
            cnt <= cnt + 1;
    //Output logic
    always @(*)
    begin : OUTPUT_LOGIC
        case (current_state)
            BL : begin
                if(cnt < 10) begin
                    start_bl = 1'b1;
                    start_tx = 1'b0;
                end
                else
                    start_bl = 1'b0;
            end
            TX : begin
                if (cnt < 10) begin
                    start_tx = 1'b1;
                    start_bl = 1'b0;
                end
                else
                    start_tx = 1'b0;
            end
            default: begin
                start_bl = 1'b0;
                start_tx = 1'b0;
            end
        endcase
    end
    reg [1:0] bit_vector_i [0:DDEPTH-1];
    integer j;
    always @(posedge clk)
        if (bitv_tvalid_i) begin
            for (j=0; j<DDEPTH; j = j+1)
                bitv_tdata[j] <= 3'b011; //16QAM
//            for (j=0; j<DDEPTH; j = j+1)
//                if (bitv_i[j] == 16'd2)
//                    bitv_tdata[j] <= 3'b010;
//                else if (bitv_i[j] == 16'd4)
//                    bitv_tdata[j] <= 3'b011;
//                else
//                    bitv_tdata[j] <= 3'b001;
        end
endmodule