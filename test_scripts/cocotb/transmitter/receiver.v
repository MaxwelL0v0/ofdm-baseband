module receiver #(
    parameter DWIDTH = 12
    )(
    //Master port
    output wire                 dsec_tdata,
    output wire                 dsec_tvalid,

    //Input data port
    input  wire [DWIDTH-1:0]    RX_ICH,
    input  wire [DWIDTH-1:0]    RX_QCH,
    input  wire                 RX_SYNC,
    //From frame_ctrl
    input  wire [2:0]           BITV_TDATA [0:47],
    input wire                  BITV_TVALID,
    //Global
    input wire                  ACLK1,
    input wire                  ACLK2,
    input wire                  ARESETN
);
    localparam SF_FFT = 1;
    reg i_ce;
    reg [DWIDTH-1:0] cenv_tre_i, cenv_tim_i;
    always @ (posedge ACLK2) begin
        cenv_tre_i <= SF_FFT*fftin_tich;
        cenv_tim_i <= SF_FFT*fftin_tqch;
        i_ce <= fftin_tvalid;
    end


    wire o_sync_sig;
    wire fftin_tvalid;
    wire [DWIDTH-1:0] fftin_tich;
    wire [DWIDTH-1:0] fftin_tqch;
    wire [DWIDTH-1:0] samp_tre, samp_tim;
    cp_remove CP ( .M_FFTIN_TICH(fftin_tich),
                      .M_FFTIN_TQCH(fftin_tqch),
                      .M_FFTIN_TVALID(fftin_tvalid),
                      .RX_ICH(RX_ICH),
                      .RX_QCH(RX_QCH),
                      .RX_SYNC(RX_SYNC),
                      .ACLK(ACLK2),
                      .ARESETN(ARESETN));


    fftmain FFT   ( .i_clk(ACLK2),
                    .i_reset(!ARESETN),
                    .i_ce(i_ce),
                    .i_sample({cenv_tre_i, cenv_tim_i}),
                    .o_result({samp_tre, samp_tim}),
                    .o_sync(o_sync_sig));

   wire [DWIDTH-1:0] map_tre, map_tim;
   wire map_tvalid;
   pilot_extractor U1 ( .M_MAP_TICH(map_tre),
                             .M_MAP_TQCH(map_tim),
                             .M_MAP_TVALID(map_tvalid),
                             .SAMP_ICH(samp_tre),
                             .SAMP_QCH(samp_tim),
                             .SAMP_SYNC(o_sync_sig),
                             .ACLK(ACLK2),
                             .ARESETN(ARESETN));

   const_demapper U2 (.M_SEC_TDATA(dsec_tdata),
                      .M_SEC_TVALID(dsec_tvalid),
                      .S_BITV_TDATA(BITV_TDATA),
                      .S_BITV_TVALID(BITV_TVALID),
                      .S_MAP_TICH(map_tre),
                      .S_MAP_TQCH(map_tim),
                      .S_MAP_TVALID(map_tvalid),
                      .ACLK1(ACLK1),
                      .ACLK2(ACLK2),
                      .ARESETN(ARESETN));

endmodule