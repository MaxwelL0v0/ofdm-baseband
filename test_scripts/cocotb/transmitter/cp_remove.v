module cp_remove #(
    parameter DWIDTH = 12,
    parameter DDEPTH = 80,
    parameter FFTLENGTH = 64
)(
    output reg [DWIDTH-1:0]     M_FFTIN_TICH,
    output reg [DWIDTH-1:0]     M_FFTIN_TQCH,
    output reg                  M_FFTIN_TVALID,
    //Slave port
    input wire [DWIDTH-1:0]     RX_ICH,
    input wire [DWIDTH-1:0]     RX_QCH,
    input wire                  RX_SYNC,
    //Global
    input wire                  ACLK,
    input wire                  ARESETN
);
    localparam CPLENGTH = 16;
    reg [DWIDTH-1:0] cenv_tre_i;
    reg [6:0] samp_cnt = 7'b0;
    reg [15:0] symb_cnt = 7'b0;
    //Symbol counter
    always @(posedge ACLK)
        if (RX_SYNC)
            symb_cnt <= symb_cnt + 1;
        else if (symb_cnt > 20)
            symb_cnt <= 1;
    //Samples counter
    always @(posedge ACLK)
        if (RX_SYNC)
            samp_cnt <= 0;
        else if (samp_cnt < DDEPTH-2)
            samp_cnt <= samp_cnt + 1;
        else
            samp_cnt <= 0;
    always @(posedge ACLK)
        if (samp_cnt > CPLENGTH-2 && symb_cnt != 0)
        begin
            M_FFTIN_TVALID <= 1'b1;
            M_FFTIN_TICH <= RX_ICH;
            M_FFTIN_TQCH <= RX_QCH;
        end
        else begin
            M_FFTIN_TVALID <= 1'b0;
            M_FFTIN_TICH <= 0;
            M_FFTIN_TQCH <= 0;
        end
endmodule