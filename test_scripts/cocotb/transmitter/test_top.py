# test_my_design.py (extended)

import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.clock import Clock
import matplotlib.pyplot as plt
import numpy as np  # for data manipulation
from numpy.fft import fft, ifft
   
@cocotb.test()
async def my_second_test(dut):
    """Try accessing the design."""
    cocotb.start_soon(Clock(dut.clk, 10, units="ns").start()) # Reloj de con T = 1 ns
    #await cocotb.start(generate_clock(dut))  # run the clock "in the background"
    dut.resetn.value = 0
    await Timer(5, units="ns")  # wait a bit
    dut.resetn.value = 1
    await Timer(117, units="us")  # wait a bit
    cenv_tre= []
    cenv_tim= []
    x = []
    n_symb = 26
    nsf = 80 # number of samples x field
    n_fill_points = 16
    ns_fft = 14
    sc_factor = 1
    #Obtención de muestras desde el HDL
    await RisingEdge(dut.cenv_tsync)
    for i in range (n_symb):
        for j in range(nsf+1):
            cenv_tre.append(dut.cenv_tre.value.signed_integer)
            cenv_tim.append(dut.cenv_tim.value.signed_integer)
            if (i < ns_fft and j > 16):
                for k in range(n_fill_points):
                    x.append((dut.cenv_tre.value.signed_integer) + 1j*(dut.cenv_tim.value.signed_integer))
            await RisingEdge(dut.clk2)
   
    # Generate Matplotlib plot after simulation finishes
    n = np.arange(n_symb*nsf+1)  # Convert clock cycles to ns
    #PLOT DATA 1
    fig_xn_1, axs_xn_1 = plt.subplots(2, 1)
    fig_xn_1.set_size_inches(12, 4)
    #fig_xn.suptitle('Señales en I y Q de salida del módulo OFDM')
    axs_xn_1[0].step(n[0:17], cenv_tre[0:17], color='r', label="CP")
    axs_xn_1[0].step(n[16:81], cenv_tre[16:81], color='b', label="DATA 1")
    axs_xn_1[0].set(xlabel='Tiempo discreto [n]', ylabel='$x_{re}[n]$')
    axs_xn_1[0].set_xlim([0, 80])
    axs_xn_1[0].grid()
    axs_xn_1[1].step(n[0:17], cenv_tim[0:17], color='r')
    axs_xn_1[1].step(n[16:81], cenv_tim[16:81], color='b')
    axs_xn_1[1].set(xlabel='Tiempo discreto [n]', ylabel='$x_{im}[n]$')
    axs_xn_1[1].set_xlim([0, 80])
    axs_xn_1[1].grid()
    fig_xn_1.legend(bbox_to_anchor=(0.7, 0.9, 0.2, .102), loc='lower left',
                      ncols=2, mode="expand", borderaxespad=0.2)
    fig_xn_1.savefig("images/data1.png", dpi=(250), bbox_inches='tight')
    #plt.title("dsec_tdata Signal in the clk Domain")
    fig_xn_2, axs_xn_2 = plt.subplots(2, 1)
    fig_xn_2.set_size_inches(12, 4)
    #fig_xn.suptitle('Señales en I y Q de salida del módulo OFDM')
    axs_xn_2[0].step(n[0:17], cenv_tre[80:97], color='r', label="CP")
    axs_xn_2[0].step(n[16:81], cenv_tre[96:161], color='b', label="DATA 2")
    axs_xn_2[0].set(xlabel='Tiempo discreto [n]', ylabel='$x_{re}[n]$')
    axs_xn_2[0].set_xlim([0, 80])
    axs_xn_2[0].grid()
    axs_xn_2[1].step(n[0:17], cenv_tim[80:97], color='r')
    axs_xn_2[1].step(n[16:81], cenv_tim[96:161], color='b')
    axs_xn_2[1].set(xlabel='Tiempo discreto [n]', ylabel='$x_{im}[n]$')
    axs_xn_2[1].set_xlim([0, 80])
    axs_xn_2[1].grid()
    fig_xn_2.legend(bbox_to_anchor=(0.7, 0.9, 0.2, .102), loc='lower left',
                      ncols=2, mode="expand", borderaxespad=0.2)
    fig_xn_2.savefig("images/data2.png", dpi=(250), bbox_inches='tight')
    
    B_ofdm = 20e6 # 20 MHz
    x_test = []
    for k in range(17, 81):
        x_test.append(cenv_tre[k]+1j*cenv_tim[k])

    print("Largo de la fft test:", len(x_test))
    X_test = fft(x_test)/2
    N_test = len(x_test)
    #sr_test = B_ofdm
    #T_test = N_test/sr_test
    n_test = np.arange(0, N_test)
    #freq_test = n_test/T_test
    fig_test, axs_test = plt.subplots(2, 1)
    fig_test.set_size_inches(12, 4)
    #fig_xn.suptitle('Señales en I y Q de salida del módulo OFDM')
    axs_test[0].step(n_test, np.real(X_test), color='r', label="Parte real")
    axs_test[0].set(xlabel='Tiempo discreto [n]', ylabel='$X_{re}[n]$')
    axs_test[0].set_xlim([0, N_test])
    axs_test[0].grid()
    axs_test[1].step(n_test, np.imag(X_test), color='b', label="$Parte imaginaria$")
    axs_test[1].set(xlabel='Tiempo discreto [n]', ylabel='$X_{im}[n]$')
    axs_test[1].set_xlim([0, N_test])
    axs_test[1].grid()
    fig_test.savefig("images/x_fft_test.png", dpi=(250), bbox_inches='tight')

    sr = B_ofdm * n_fill_points
    N = len(x)
    n = np.arange(0, N)

    mu, sigma = 0, 0.1 # mean and standard deviation 
    s_re_1 = np.random.normal(mu, sigma, N) * 1000
    s_im_1 =  np.random.normal(mu, sigma, N) * 1000
    
    z = np.arange(64)
    #Ruido para diagramas de constelación
    mu, sigma = 0, 0.025 # mean and standard deviation 
    s_re_2 = np.random.normal(mu, sigma, 10000) * 1000
    s_im_2 = np.random.normal(mu, sigma, 10000) * 1000
    Ich_points_16qam = []
    Qch_points_16qam = []
    Ich_points_qpsk = []
    Qch_points_qpsk = []
    X_test2 = []
    x2 = []
    n_symb_test = 26
    n_fill_points2  = 16
    for s in range(n_symb_test):
        x_test2 = []
        for j in range(17, 81):
            x_test2.append(cenv_tre[int(80*s+j)]+s_re_2[80*s+j]+1j*cenv_tim[int(80*s+j)]+s_im_2[80*s+j])
            if (s != 15):
                for n in range(n_fill_points2):
                    x2.append(cenv_tre[int(80*s+j)]+s_re_1[80*s+j+n]+1j*(cenv_tim[int(80*s+j)]+s_im_1[80*s+j+n]))
        X_test2.append(fft(x_test2)/(2000))
    k_mod_qpsk = 1/np.sqrt(2)
    k_mod_16qam = 1/np.sqrt(10)
    for i in range(n_symb_test):
        if (i != 15):
            for j in range(64):
                if (j > 4 and j != 10 and j != 24 and j != 31 and j < 36):
                    Ich_points_16qam.append(np.real(X_test2[i][j])/k_mod_16qam)
                    Qch_points_16qam.append(np.imag(X_test2[i][j])/k_mod_16qam)
                elif (j > 36 and j != 38 and j != 52 and j < 58):
                    Ich_points_qpsk.append(np.real(X_test2[i][j])/k_mod_qpsk)
                    Qch_points_qpsk.append(np.imag(X_test2[i][j])/k_mod_qpsk)
    
    t = np.arange(0, 3.2, 0.003125)
    #SEÑAL CON RUIDO AWGN
    fig_xsamp, axs_xsamp = plt.subplots(2, 1)
    fig_xsamp.set_size_inches(12, 4)
    axs_xsamp[0].plot(t, np.real(x2[:1024]), color='r', label="Parte real", linewidth=0.5)
    axs_xsamp[0].set( ylabel='$x_{re}(t)$')
    axs_xsamp[0].set_xlim([0, 3.2])
    axs_xsamp[0].grid()
    axs_xsamp[1].plot(t, np.imag(x2[:1024]), color='b', label="$Parte imaginaria$", linewidth=0.5)
    axs_xsamp[1].set(xlabel='Tiempo [$\u03bc s$]', ylabel='$x_{im}(t)$')
    axs_xsamp[1].set_xlim([0, 3.2])
    axs_xsamp[1].grid()
    fig_xsamp.savefig("images/x_sampes.png", dpi=(250), bbox_inches='tight')
    

    plt.show()
    await Timer(10000, units="ns")
    
    dut._log.info("dsec_tvalid is %s", dut.dsec_tvalid.value)
    assert dut.dsec_tdata.value == 1, "dsec_tdata is not 1!"

