`include "div.v"
`include "min.v"
module bit_load_proc #(
    parameter DWIDTH = 16,
    parameter N = 64
    )(
    input wire   clk,
    input wire   reset,
    input wire   start,
    output reg  [DWIDTH-1:0] bit_vector [0:N-1],
    output reg  bit_vector_tvalid
    );

    reg        [2*DWIDTH-1:0] En [0:N-1];
    reg        [DWIDTH/4-1:0] bn [0:N-1];
    reg signed [2*DWIDTH-1:0] Ex_bar;
    reg signed [DWIDTH-1:0] Noise_var, gap;
    integer i;
    reg signed [DWIDTH-1:0] gn_temp1 [0:N-1];
    reg [DWIDTH-1:0] dt_init [0:N-1];
    
    //INICIALIZATION
    initial begin
        //|Hn|^2
        $readmemb("sm_Hn.mem", gn_temp1);
        Ex_bar    = 16'b00000001_00000000; // 1.0
        Noise_var = 16'b00000000_00101110; // Varianza ruido = 0.179

        $readmemb("dt_init.mem", dt_init);

        for (i=0; i<=N-1; i = i+1) begin
            bn[i] = 4'd0;
            En[i] = 16'd0;
        end

    end
    wire init_done;
    reg [2:0] init_cnt;
    always @(posedge clk)
        if (reset)
            init_cnt <= 3'd0;
        else if (current_state == INIT)
            init_cnt <= init_cnt + 3'd1;
    assign init_done = (current_state == INIT && init_cnt == 2)? 1'b1 : 1'b0;
    reg start_i;
    reg reset_loop;
    reg [2:0] current_state, next_state;
    always @(posedge clk) start_i <= start;

    // STATE MACHINE FOR LEVIN CAMPELLO
    //STATE CODIFICATION
    localparam INIT  = 3'b000;
    localparam IDLE  = 3'b001;
    localparam GN_S  = 3'b010;
    localparam DT_S  = 3'b011;
    localparam ALLOC = 3'b100;
    //STATE REGISTERS
    // Next state logic
    always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          INIT   : begin
               if (init_done)
                   next_state = IDLE;
               else
                   next_state = current_state;
           end
          IDLE   : begin
               if (start_i)
                   next_state = GN_S;
               else
                   next_state = current_state;
          end
          GN_S  : begin
               if (sc_index == N)
                   next_state = DT_S;
               else
                   next_state = current_state;
          end
          DT_S  : begin
               if (sc_index == N)
                   next_state  = ALLOC;
               else
                   next_state = current_state;
          end
          ALLOC  :
               if (break_sig)
                   next_state = INIT;
               else
                   next_state = current_state;
          3'b101 : next_state = INIT;
          3'b110 : next_state = INIT;
          3'b111 : next_state = INIT;
          default:
                next_state = current_state;
        endcase
     end
    // State memory
   always @(posedge clk)
   begin : STATE_MEMORY
    if(reset) begin
           current_state <= INIT;
    end
	else begin
           current_state <= next_state;
	end
   end
    //Find gn vector serial proc
   reg [DWIDTH-1:0] gn_tmp;
   reg [DWIDTH-1:0] gn_tmp3[0:N-1];
   reg gn_s_start_div, gn_s_reset_div;
   wire gn_s_valid_div, gn_s_busy_div, gn_s_done_div, gn_s_dbz_div, gn_s_ovf_div;

    //Count data in GN_S state
    reg [12:0] dcount_gn = {13'd0};
    always @(posedge clk)
    begin : DCOUNT_GN_PROC
        if (reset)
            dcount_gn <= 0;
        else if (gn_s_done_div)
            dcount_gn <= 0;
        else if (current_state == GN_S)
            dcount_gn <= dcount_gn + 1;
        else
            dcount_gn <= 0;
    end
    //Reset div module when dcount = 0
   always @(posedge clk)
       if (dcount_gn == 0 && current_state == GN_S) begin
           gn_s_start_div <= 1;
           gn_s_reset_div <= 0;
       end
       else
           gn_s_start_div <= 0;

   reg [6:0] sc_index = {6'd0};

   div #(DWIDTH, DWIDTH/2) gn_s_div_inst (clk, gn_s_reset_div, gn_s_start_div, gn_s_busy_div, gn_s_done_div, gn_s_valid_div,
                                             gn_s_dbz_div, gn_s_ovf_div,
                                             gn_temp1[sc_index], Noise_var, gn_tmp);

    //Find dt vector serial proc
   reg [2*DWIDTH-1:0] dt_tmp;
   reg [2*DWIDTH-1:0] dt_n[0:N-1];
   reg dt_s_start_div, dt_s_reset_div;
   wire dt_s_valid_div, dt_s_busy_div, dt_s_done_div, dt_s_dbz_div, dt_s_ovf_div;

    //Count data in DT_S state
    reg [12:0] dcount_dt = {13'd0};
    always @(posedge clk)
    begin : DCOUNT_DT_PROC
        if (reset)
            dcount_dt <= 0;
        else if (dt_s_done_div)
            dcount_dt <= 0;
        else if (current_state == DT_S)
            dcount_dt <= dcount_dt + 1;
        else
            dcount_dt <= 0;
    end
    //Reset div module when dcount = 0
   always @(posedge clk)
       if (dcount_dt == 0 && current_state == DT_S) begin
           dt_s_start_div <= 1;
           dt_s_reset_div <= 0;
       end
       else
           dt_s_start_div <= 0;

   div #(2*DWIDTH, DWIDTH) dt_s_div_inst (clk, dt_s_reset_div, dt_s_start_div, dt_s_busy_div, dt_s_done_div, dt_s_valid_div,
                                             dt_s_dbz_div, dt_s_ovf_div,
                                             {8'd0, dt_init[sc_index], 8'd0}, {8'd0, gn_tmp3[sc_index], 8'd0}, dt_tmp);
   //Subcarrier counter
    wire sdf_done1, sdf_done2;
    assign sdf_done1 = gn_s_done_div;
    assign sdf_done2 = dt_s_done_div;
    always @(posedge clk)
        if (sc_index == N) begin
            sc_index <= 0;
        end
        else if (current_state == GN_S && sdf_done1)
            sc_index <= sc_index + 1;
        else if (current_state == DT_S && sdf_done2)
            sc_index <= sc_index + 1;

   always @(posedge clk)
       if (gn_s_done_div && current_state == GN_S)
           gn_tmp3[sc_index] <= gn_tmp;
       else if (dt_s_done_div && current_state == DT_S)
           if (!dt_s_dbz_div) 
               dt_n[sc_index] <= dt_tmp;
           else
               dt_n[sc_index] <= 32'd10000000;

    //Data counter for alloc state
    reg [12:0] dcount_alloc = {13'd0};
    always @(posedge clk)
    begin : DATA_COUNT_ALLOC
        if (reset)
            dcount_alloc <= 0;
        else if (reset_loop)
            dcount_alloc <= 0;
        else if (current_state == ALLOC)
            dcount_alloc <= dcount_alloc + 1;
        else
            dcount_alloc <= 0;
    end
   integer n;
   reg [2*DWIDTH-1:0] E_so_far;
   initial E_so_far = 32'd0;
   reg alloc_start_min, alloc_reset_min, alloc_valid_dt;
   initial begin
       alloc_start_min <= 0;
       alloc_reset_min <= 1;
   end
   reg [2*DWIDTH-1:0] alloc_dt [0:N-1];
   reg [2*DWIDTH-1:0] alloc_min_dt;

   localparam ADDRWN = $clog2(N);

   reg [ADDRWN-1:0] alloc_index_dt;
   min #(2*DWIDTH, N, ADDRWN) min_inst0 (alloc_min_dt, alloc_index_dt, alloc_valid_dt, alloc_dt,
                                                        clk, alloc_start_min, alloc_reset_min);
   reg [6:0] start_min_cnt;
   always @(posedge clk)
       if (current_state != ALLOC)
           start_min_cnt <= 0;
       else if (alloc_start_min)
           start_min_cnt <= start_min_cnt + 1;

   initial reset_loop = 0;
   always @(posedge clk)
   begin : ALLOC_PROC
        if (current_state == DT_S && next_state == ALLOC)
            for (int d = 0; d <= N-1; d = d+1) alloc_dt[d] <= dt_n[d];
        else if (current_state == ALLOC) begin
            if (dcount_alloc == 0) begin
               alloc_start_min <= 1;
               alloc_reset_min <= 0;
               reset_loop <= 0;
            end
            else if (dcount_alloc == 3)
                alloc_start_min <= 0;
            else if (alloc_valid_dt && (E_so_far+alloc_min_dt < Ex_N)) begin
                E_so_far <= E_so_far + alloc_min_dt;
                En[alloc_index_dt] <= En[alloc_index_dt] + alloc_min_dt;
                bn[alloc_index_dt] <= bn[alloc_index_dt] + 2;
                if (alloc_index_dt == (N-1)/2 || alloc_index_dt == N-1)
                    alloc_dt[alloc_index_dt] <= 4*alloc_dt[alloc_index_dt];
                else
                    alloc_dt[alloc_index_dt] <= 4*alloc_dt[alloc_index_dt];
                reset_loop <= 1;
            end
            else
                reset_loop <= 0;
        end
   end
   reg [2*DWIDTH-1:0] Ex_N;
   initial Ex_N = (N-1)*2*{8'd0, Ex_bar, 8'd0};
   wire break_sig;
   assign break_sig = (current_state == ALLOC && (E_so_far+alloc_min_dt) > Ex_N )? 1'b1 : 1'b0;

   always @(posedge clk) bit_vector_tvalid <= break_sig;
   always @(posedge clk) begin
       if (break_sig)
           for (int k = 0; k<N; k = k+1) bit_vector[k] <= bn[k];
   end
   //DEBUG SIGNALS
   wire [DWIDTH-1:0] bn0, bn1, bn2, bn3, bn4, bn5, bn6, bn7, bn8, bn9, bn10, bn11, bn12;
   wire [DWIDTH-1:0] bn13, bn14, bn15, bn16, bn17, bn18, bn19, bn20, bn21, bn22, bn23, bn24, bn25;
   wire [DWIDTH-1:0] bn26, bn27, bn28, bn29, bn30, bn31, bn32, bn33, bn34, bn35, bn36, bn37, bn38;
   wire [DWIDTH-1:0] bn39, bn40, bn41, bn42, bn43, bn44, bn45, bn46, bn47, bn48, bn49, bn50, bn51;
   wire [DWIDTH-1:0] bn52, bn53, bn54, bn55, bn56, bn57, bn58, bn59, bn60, bn61, bn62, bn63;
   assign bn0 = bn[0];
   assign bn1 = bn[1];
   assign bn2 = bn[2];
   assign bn3 = bn[3];
   assign bn4 = bn[4];
   assign bn5 = bn[5];
   assign bn6 = bn[6];
   assign bn7 = bn[7];
   assign bn8 = bn[8];
   assign bn9 = bn[9];
   assign bn10 = bn[10];
   assign bn11 = bn[11];
   assign bn12 = bn[12];
   assign bn13 = bn[13];
   assign bn14 = bn[14];
   assign bn15 = bn[15];
   assign bn16 = bn[16];
   assign bn17 = bn[17];
   assign bn18 = bn[18];
   assign bn19 = bn[19];
   assign bn20 = bn[20];
   assign bn21 = bn[21];
   assign bn22 = bn[22];
   assign bn23 = bn[23];
   assign bn24 = bn[24];
   assign bn25 = bn[25];
   assign bn26 = bn[26];
   assign bn27 = bn[27];
   assign bn28 = bn[28];
   assign bn29 = bn[29];
   assign bn30 = bn[30];
   assign bn31 = bn[31];
   assign bn32 = bn[32];
   assign bn33 = bn[33];
   assign bn34 = bn[34];
   assign bn35 = bn[35];
   assign bn36 = bn[36];
   assign bn37 = bn[37];
   assign bn38 = bn[38];
   assign bn39 = bn[39];
   assign bn40 = bn[40];
   assign bn41 = bn[41];
   assign bn42 = bn[42];
   assign bn43 = bn[43];
   assign bn44 = bn[44];
   assign bn45 = bn[44];
   assign bn46 = bn[46];
   assign bn47 = bn[47];
   assign bn48 = bn[48];
   assign bn49 = bn[49];
   assign bn50 = bn[50];
   assign bn51 = bn[51];
   assign bn52 = bn[52];
   assign bn53 = bn[53];
   assign bn54 = bn[54];
   assign bn55 = bn[55];
   assign bn56 = bn[56];
   assign bn57 = bn[57];
   assign bn58 = bn[58];
   assign bn59 = bn[59];
   assign bn60 = bn[60];
   assign bn61 = bn[61];
   assign bn62 = bn[62];
   assign bn63 = bn[63];

endmodule
