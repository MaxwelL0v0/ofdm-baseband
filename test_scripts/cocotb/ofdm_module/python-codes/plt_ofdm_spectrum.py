#!/usr/bin/env python3
import csv
from scipy import signal
import numpy as np
from matplotlib import pyplot as plt
#with open('/home/max/dev/hdl/ofdm-baseband2/src/modules/symbolReception/python-codes/ifft_file.data') as inf:
#    reader = csv.reader(inf, delimiter=" ")
#    first_col = list(zip(*reader))[0]
#with open('/home/max/dev/hdl/ofdm-baseband2/src/modules/symbolReception/python-codes/ifft_file.data') as inf:
#    reader = csv.reader(inf, delimiter=" ")
#    second_col = list(zip(*reader))[1]
with open('/home/max/dev/hdl/ofdm-baseband2/src/modules/symbolReception/python-codes/file.data') as inf:
    reader = csv.reader(inf, delimiter=" ")
    first_col2 = list(zip(*reader))[0]
with open('/home/max/dev/hdl/ofdm-baseband2/src/modules/symbolReception/python-codes/file.data') as inf:
    reader = csv.reader(inf, delimiter=" ")
    second_col2 = list(zip(*reader))[1]

#x_ich = np.zeros(64)
#x_qch = np.zeros(64)
xdft_ich = np.zeros(64)
xdft_qch = np.zeros(64)
for k in range(64):
    # x_ich[k] = int(first_col[k])/1000*16
    # x_qch[k] = int(second_col[k])/1000*16
    xdft_ich[k] = int(first_col2[k])/1000
    xdft_qch[k] = int(second_col2[k])/1000

fs = 200e6
N = 64
#rng = np.random.default_rng()
#noise_power = 0.0001 * fs / 2
#time = np.arange(N) / fs
#x = x_ich+1j*x_qch+rng.normal(scale=np.sqrt(noise_power), size=time.shape)
xdft = xdft_ich+1j*xdft_qch
#f, Pxx_den = signal.periodogram(x, fs)
ofdm_psd = (1/(N)) * abs(xdft)**2;

k = np.arange(64)/64
plt.plot(xdft_ich, xdft_qch, '.')
plt.xlabel('I channel')
plt.ylabel('Q channel')
plt.show()
