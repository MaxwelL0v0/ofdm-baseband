#!/bin/bash
#vcdcat ifft_signals.vcd > signals.txt
#tail -n +10 signals.txt > signals_new.txt
file="./signals_new.txt"
while IFS= read -r line
do
    #valid="$(echo "$line" | awk '{print $5}')"
    clk_state="$(echo "$line" | awk '{print $2}')"
    if [ $clk_state == "1" ] 
    then
        ICH_hex="$(echo $line | awk '{print $3}')"
        QCH_hex="$(echo $line | awk '{print $4}')"
        ICH_unsigned="$(printf "%d" 0x$ICH_hex)"
        QCH_unsigned="$(printf "%d" 0x$QCH_hex)"
        thres="32767"
        if [[ $ICH_unsigned -ge $thres ]]
        then
            ICH_signed="-$(echo "2^16-$ICH_unsigned" | bc -l)"
        else
            ICH_signed="$ICH_unsigned"
        fi
        if [[ $QCH_unsigned -ge $thres ]]
        then
            QCH_signed="-$(echo "2^16-$QCH_unsigned" | bc -l)"
        else
            QCH_signed="$QCH_unsigned"
        fi
        echo "$ICH_signed $QCH_signed"
    fi
done < "$file"
