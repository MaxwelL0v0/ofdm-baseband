`include "transmitter.v"
`include "receiver.v"
`include "clock_divider.v"
`include "frame_ctrl.v"
`include "bit_load_proc.v"
module top (
    input wire clk,
    input wire resetn
);
    wire clk2;
    wire [15:0] data_tre, data_tim;
    wire [15:0] cenv_tre, cenv_tim;
    wire        cenv_tsync, data_tsync;
    wire        dsec_tdata, dsec_tvalid;
    localparam DIV = 28'd5;
    //reg [1:0] dmap_tsel = {2'b11};
    clock_divider #(DIV) UC ( .clk_out(clk2),
                              .clk_in(clk));
    wire start_bl, start_tx, bit_vector_tvalid, tx_end;
    wire [15:0] bit_vector [0:63];
    wire [2:0]  bitv_tdata [0:47];
    frame_ctrl CTRL (start_bl, start_tx, bitv_tdata, clk, resetn, bit_vector, bit_vector_tvalid, tx_end);
    bit_load_proc LC (clk, !resetn, start_bl, bit_vector, bit_vector_tvalid); 

    transmitter U1 (  .TX_ICH(cenv_tre),
                      .TX_QCH(cenv_tim),
                      .TX_SYNC(cenv_tsync),
                      .BITV_TDATA(bitv_tdata),
                      .BITV_TVALID(start_tx),
                      .ACLK1(clk),
                      .ACLK2(clk2),
                      .ARESETN(resetn));
    
    receiver U2 (       .dsec_tdata(dsec_tdata),
                        .dsec_tvalid(dsec_tvalid),
                        .RX_ICH(cenv_tre),
                        .RX_QCH(cenv_tim),
                        .RX_SYNC(cenv_tsync),
                        .BITV_TDATA(bitv_tdata),
                        .BITV_TVALID(start_tx),
                        .ACLK1(clk),
                        .ACLK2(clk2),
                        .ARESETN(resetn));
                        
endmodule
