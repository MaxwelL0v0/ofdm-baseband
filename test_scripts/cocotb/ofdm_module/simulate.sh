#!/bin/bash
find ./ -name "*.vvp" | xargs rm 2>/dev/null
module_name=$(cat $1 | grep module | head -1 | awk '{print $2}')
echo  $module_name


#se genera un archivo vcd
find ./ -name "*.fst" | xargs rm 2>/dev/null
iverilog -o $module_name.vvp -g2005-sv $1
vvp $module_name.vvp -vcd
rm ./$module_name.vvp
gtkwave --rcvar 'fontname_signals Monospace 12' --rcvar 'fontname_waves Monospace 12' $module_name.vcd > /dev/null 2>&1 & disown
