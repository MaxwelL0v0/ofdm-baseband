module chann_interf 
    parameter DWIDTH = 16
(
    output wire [DWIDTH-1:0] ICH_OUT,
    output wire [DWIDTH-1:0] QCH_OUT,
    input wire clk,
    input wire awgn_ce,
    input wire [DWIDTH-1:0] ICH_AWNG,
    input wire [DWIDTH-1:0] QCH_AWNG,
    input wire [DWIDTH-1:0] ICH_IN,
    input wire [DWIDTH-1:0] QCH_IN
);
    reg [DWIDTH-1:0] awgn_ich = 16'd0;
    reg [DWIDTH-1:0] awgn_qch = 16'd0;
    reg [DWIDTH-1:0] input_ich = 16'd0;
    reg [DWIDTH-1:0] input_qch = 16'd0;
    reg [DWIDTH-1:0] output_ich = 16'd0;
    reg [DWIDTH-1:0] output_qch = 16'd0;
    
    //Registrar muestras de awgn
    always @(posedge clk) begin
        awgn_ich <= ICH_AWNG;
        awgn_qch <= QCH_AWNG;
        input_ich <= ICH_AWNG;
        input_qch <= QCH_AWNG;
    end

    always @(posedge clk) begin
        if (awgn_ce) begin
            output_ich <= input_ich + awgn_ich;
            output_qch <= input_qch + awgn_qch;
        end
        else begin
            output_ich <= 0;
            output_qch <= 0;
        end
    end

    assign ICH_OUT = output_ich;
    assign QCH_OUT = output_qch;
endmodule
