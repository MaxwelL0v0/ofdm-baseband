#!/usr/bin/env python3
from numpy.fft import fft
import numpy as np
scale_factor  = 10; N = 64
k = np.arange(N)
x = scale_factor*np.cos(2*np.pi*k/N)

DATASIZE = 64

def triFunc(data):
    j = 0
    while j <= DATASIZE // 4:
        data[j] = j
        j += 1
    while j <= 3 * DATASIZE // 4:
        data[j] = DATASIZE // 2 - j
        j += 1
    while j < DATASIZE:
        data[j] = -DATASIZE + j
        j += 1
# Ejemplo de uso:
data = [0]*DATASIZE  # Inicializar el arreglo con ceros
triFunc(data)
print(data)  

y = 2*fft(data)/N
print(np.round(y, 4))
