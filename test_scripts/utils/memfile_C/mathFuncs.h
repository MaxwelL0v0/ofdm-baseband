//FUNCIONES PERIODICAS
//
//Funcion triangular simetrica con respecto al eje x.
void triFunc(int* data){
	int j=0;
	while(j<=DATASIZE/4){
		data[j]=j;
		j++;
	}
	while(j<=3*DATASIZE/4){
		data[j]=DATASIZE/2-j;
		j++;
	}
	while(j<DATASIZE){
		data[j]=-DATASIZE+j;
		j++;
	}
}
//Funcion senoidal
void sinFunc(int* data, int theta){
	int k=0;
    int scale_factor = 100;
	double y;
	while (k<DATASIZE){
		y=scale_factor*sin(2*M_PI*k/DATASIZE+theta*M_PI/2);
		data[k]=round(y);
		k++;
	}
}
