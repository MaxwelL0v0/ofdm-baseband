#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define DATASIZE 64
#include "hexFuncs.h"
#include "mathFuncs.h"

void genHex(char*, char*, int);
void saveHex(char* , char* );
void triFunc(int*);
int main(int argc, char *argv[]){
	int theta = (int)*argv[2]-'0'; //
    char data_array[64];
	int data[DATASIZE]; 
    triFunc(data);
	char var_name[8];
	strcpy(var_name, "");
	int k=0;
	while(k<DATASIZE){
		genHex(data_array, var_name, data[k]);
		saveHex(data_array, argv[1]);
		strcpy(data_array, "");
		k++;
	};
	char arrayf[]="//Proceso finalizado.\n";
	saveHex(arrayf, argv[1]);
	printf("Escritura completa sin errores.\n");
	return 0;
}
