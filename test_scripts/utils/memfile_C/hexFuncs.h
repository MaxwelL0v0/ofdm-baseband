// librería de funciones utilizadas para almacenar
// los datos en hexadecimal dentro del archivo .vhd

void genHex(char* data_array, char* var_name, int int_val){
	char hex_val[4];
	strcat(data_array, var_name);
	if (int_val<0) {
		sprintf(hex_val, "%4hX", int_val);
	}
	else{ 	sprintf(hex_val, "%04X", int_val);
	}
	strcat(data_array, hex_val);
	strcat(data_array, "\n");
}
void saveHex(char* data, char* vhd_name){
         FILE * fPtr = fopen(vhd_name, "a");
         if(fPtr == NULL)
         {
                 printf("Unable to create file.\n");
                 exit(EXIT_FAILURE);
         };
         fputs(data, fPtr);
         fclose(fPtr);
}
