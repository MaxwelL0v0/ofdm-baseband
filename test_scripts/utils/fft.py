from scipy.fft import fft, ifft
import numpy as np
scale_factor  = 100; N = 64
k = np.arange(N)
x = -1*scale_factor*np.cos(2*np.pi*k/N)
y = fft(x)/32
z = ifft(y)*2

print(np.round(z, 4))
print(len(z))
