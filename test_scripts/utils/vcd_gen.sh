#!/bin/bash
find ./ -name "*.vvp" | xargs rm 2>/dev/null
module_name=$(cat $1 | grep module | head -1 | awk '{print $2}')
echo  $module_name


#se genera un archivo vcd
find ./ -name "*.vcd" | xargs rm 2>/dev/null
iverilog -o $module_name.vvp -g2012 $1 
vvp $module_name.vvp
rm ./$module_name.vvp
gtkwave $module_name.vcd > /dev/null 2>&1 & disown 
