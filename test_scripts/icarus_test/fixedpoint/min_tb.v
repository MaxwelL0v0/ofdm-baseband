`timescale 1ns/1ps
`include "min.v"
module tb ();
    parameter DWIDTH = 16;
    parameter DDEPTH = 5;
    reg clk;
    reg reset;
    reg start;
    reg [4*DWIDTH-1:0] decision_table [0:DDEPTH-1];
    initial begin
        decision_table[0] = 64'd647094545;
        decision_table[1] = 64'd1011010;
        decision_table[2] = 64'd860909;
        decision_table[3] = 64'd298122;
        decision_table[4] = 64'd571;
    end
    wire [4*DWIDTH-1:0] min_dt;
    wire [6:0] index_dt;
    reg valid_dt;
    min  #(.DWIDTH(4*DWIDTH), .DDEPTH(DDEPTH)) DUT (.*);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #0.5 clk = ~clk;
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
            reset <= 1; start <= 0;
        #5  reset <= 0; start <= 1;
        #50 $display("Test complete.");
        $finish;
    end
endmodule
