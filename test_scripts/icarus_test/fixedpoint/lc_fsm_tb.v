`timescale 1ns/1ps
`include "lc_fsm.v"
module tb ();
    parameter DWIDTH = 16;
    parameter N =5;
    reg clk;
    reg reset;
    reg start;
    wire bit_vector_tvalid;
    wire [DWIDTH-1:0] bit_vector [0:N-1];
    wire [DWIDTH-1:0] db_sig1, db_sig2, db_sig3;
    assign db_sig1 = bit_vector[1];
    assign db_sig2 = bit_vector[2];
    assign db_sig3 = bit_vector[3];

	lc_fsm #(.DWIDTH(DWIDTH), .N(N)) DUT (.*);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #0.5 clk = ~clk;
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
            reset <= 1;  
        #5  reset <= 0; start <= 1;
        #5  start <= 0; 
        #2000 $display("Test complete.");
        $finish;
    end
endmodule
