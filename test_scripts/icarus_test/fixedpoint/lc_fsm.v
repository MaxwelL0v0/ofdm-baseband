`include "sqrt.v"
`include "div.v"
`include "min.v"
module lc_fsm #(
    parameter DWIDTH = 16,
    parameter N = 5
    )(
    input wire   clk,
    input wire   reset,
    input wire   start,
    output reg  [DWIDTH-1:0] bit_vector [0:N-1],
    output reg  bit_vector_tvalid
    );

    reg signed [DWIDTH-1:0] p [0:1]; 
    reg        [4*DWIDTH-1:0] En [0:N-1]; 
    reg        [DWIDTH-1:0] bn [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_re [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_im [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_re_norm [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_im_norm [0:N-1]; 
    reg signed [DWIDTH-1:0] Ex_bar;
    reg signed [2*DWIDTH-1:0] Noise_var, gap; 
    integer i;
    reg signed [2*DWIDTH-1:0] Hn_re_temp [0:N-1];
    reg signed [2*DWIDTH-1:0] Hn_im_temp [0:N-1];
    reg signed [2*DWIDTH-1:0] gn_temp1 [0:N-1];

    //INICIALIZATION
    initial begin
        p[1]    = 16'b00000000_11100110;  // 0.8984375
        p[0]    = 16'b00000001_00000000;  // 1.0
        Ex_bar  = 16'b00000001_00000000; // 1.0
        Noise_var = 32'b0000000000000000_0010111001010110; // Varianza ruido = 0.180999755859375
        gap       = 32'b0000000000000001_0000000000000000; // gap  = 1.0 => 0 dB
        for (i=0; i<=N-1; i = i+1) begin
            bn[i] = 16'd0;
            En[i] = 64'd0;
            Hn_re[i] = 16'h0000;
            Hn_im[i] = 16'h0000;
        end
        //normalized Hn-LUT for Ntot = 8 => len(Hn) = 5
        //Hn_norm_re = [1, sqrt(2)/2, 0, -sqrt(2)/2, -1]
        Hn_re_norm[0] = 16'b00000001_00000000; // 1.0
        Hn_re_norm[1] = 16'b00000000_10110101; // 0.70703125
        Hn_re_norm[2] = 16'b00000000_00000000; // 0.0
        Hn_re_norm[3] = 16'b11111111_01001011; //-0.70703125
        Hn_re_norm[4] = 16'b11111111_00000000; //-1.0
        //Hn_norm_im = [0, sqrt(2)/2, 1, sqrt(2)/2, 0]
        Hn_im_norm[0] = 16'b00000000_00000000; // 0.0
        Hn_im_norm[1] = 16'b00000000_10110101; // 0.70703125
        Hn_im_norm[2] = 16'b00000001_00000000; // 1.0
        Hn_im_norm[3] = 16'b00000000_10110101; // 0.70703125
        Hn_im_norm[4] = 16'b00000000_00000000; // 0.0
        
        foreach (Hn_re[j]) begin
            Hn_re_temp[j] = p[1]*Hn_re_norm[j];
            Hn_im_temp[j] = p[1]*Hn_im_norm[j];
            Hn_re[j] = Hn_re_temp[j][23:8]+16'b00000001_00000000;
            Hn_im[j] = Hn_im_temp[j][23:8];
            gn_temp1[j] = Hn_re[j]*Hn_re[j]+Hn_im[j]*Hn_im[j];
        end
    end
    reg start_i;
    reg reset_loop;
    reg [1:0] current_state, next_state;
    always @(posedge clk) start_i <= start;
    //Data counter for debugging
    reg [7:0] data_count = {8'd0};
    always @(posedge clk)
    begin : DATA_COUNT_PROC
        if (reset)
            data_count <= 0;
        else if (reset_loop)
            data_count <= 228;
        else if (current_state != IDLE)
            data_count <= data_count + 1;
        else
            data_count <= 0;
    end
    
    //Contador de subportadoras
    reg [5:0] sc_index;
    reg sdf_done ={1'b0};
    always @(posedge clk)
        if (sc_index == N-1)
            sc_index <= 0;
        else if (sdf_done)
            sc_index <= sc_index + 1;

    // STATE MACHINE FOR LEVIN CAMPELLO
    //STATE CODIFICATION
    localparam IDLE  = 2'b00;
    localparam GN_S  = 2'b01;
    localparam DT_S  = 2'b10;
    localparam ALLOC = 2'b11;
    //STATE REGISTERS
    // Next state logic
    always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   : begin
               if (start_i)
                   next_state = GN_S;
               else
                   next_state = current_state;
          end
          GN_S  : begin
               if (data_count == 126)
                   next_state = DT_S;
               else
                   next_state = current_state;
          end
          DT_S  : begin
               if (data_count == 227)
                   next_state  = ALLOC;
               else
                   next_state = current_state;
          end
          ALLOC  :
               if (data_count == 250 || break_sig) 
                   next_state = IDLE;
               else
                   next_state = current_state;

          default:
                next_state = current_state;
        endcase
     end
    // State memory
   always @(posedge clk)
   begin : STATE_MEMORY
    if(reset) begin
           current_state <= IDLE;
    end
	else begin
           current_state <= next_state;
	end
   end
    // find gn vector
   localparam FBITS = 16;
   wire gn_valid_sqrt [0:N-1], gn_valid_div [0:N-1];
   reg gn_start_sqrt, gn_start_div, gn_reset_div;
   wire gn_busy_sqrt [0:N-1], gn_busy_div [0:N-1], gn_done_div [0:N-1], gn_dbz_div [0:N-1], gn_ovf_div [0:N-1];
   reg [2*DWIDTH-1:0] root_temp [0:N-1];
   reg [2*DWIDTH-1:0] rem [0:N-1];

   genvar k;
   reg [4*DWIDTH-1:0] gn_temp2 [0:N-1];
   reg [4*DWIDTH-1:0] gn_temp3 [0:N-1];
   generate for (k = 0; k < N; k = k + 1) begin
        sqrt #(2*DWIDTH, FBITS) sqrt_inst0 (clk, gn_start_sqrt, gn_busy_sqrt[k], gn_valid_sqrt[k], gn_temp1[k], root_temp[k], rem[k]);
        div #(4*DWIDTH, 2*DWIDTH) div_inst0 (clk, gn_reset_div, gn_start_div, gn_busy_div[k], gn_done_div[k], gn_valid_div[k], 
                                             gn_dbz_div[k], gn_ovf_div[k], gn_temp2[k], {16'd0, Noise_var, 16'd0}, gn_temp3[k]);
   end endgenerate

   always @(posedge clk)
   begin : GN_PROC
       if (current_state == GN_S) begin
            if (data_count == 0) begin
                gn_start_sqrt <= 1;
                gn_start_div  <= 0;
                gn_reset_div <= 1;
            end
            else if (data_count == 1)
                gn_start_sqrt <= 0;
            else if (gn_valid_sqrt[0]) begin
                gn_reset_div <= 0;
                gn_start_div <= 1;
                foreach (gn_temp2[j]) gn_temp2[j] <= root_temp[j]*root_temp[j];
            end
            else  begin
                gn_reset_div <= 1;
                gn_start_div <= 0;
            end
        end
   end
   
   reg [4*DWIDTH-1:0] E_so_far;
   wire dt_valid_div [0:N-1];
   wire dt_busy_sqrt [0:N-1], dt_busy_div [0:N-1], dt_done_div [0:N-1], dt_dbz_div [0:N-1], dt_ovf_div [0:N-1];
   reg dt_start_div, dt_reset_div;
   genvar j;
   reg [2*DWIDTH-1:0] dt_temp1 [0:N-1];
   reg [4*DWIDTH-1:0] dt_temp2 [0:N-1];
   generate for (j = 0; j < N; j = j + 1) begin
        div #(4*DWIDTH, 2*DWIDTH) div_inst1 (clk, dt_reset_div, dt_start_div, dt_busy_div[j], dt_done_div[j], dt_valid_div[j],
                                             dt_dbz_div[j], dt_ovf_div[j], {16'd0, dt_temp1[j], 16'd0}, gn_temp3[j], dt_temp2[j]);
   end endgenerate
   integer n;
   initial E_so_far = 64'd0;
   always @ (posedge clk)
   begin : DT_PROC
       if (current_state == DT_S) begin 
           if (data_count == 127) begin
                dt_reset_div <= 0;
                dt_start_div <= 1;
                dt_temp1[0] <= 3*gap; //PAM
                dt_temp1[N-1] <= 3*gap; //PAM
                for (n = 1; n < N-1; n = n + 1) dt_temp1[n] <= 2*gap; //QAM
            end
            else if (data_count > 127)
                dt_start_div <= 0;
        end
   end
   
   reg alloc_start_min, alloc_reset_min, alloc_valid_dt;
   initial begin
       alloc_start_min <= 0;
       alloc_reset_min <= 1;
   end
   reg [4*DWIDTH-1:0] alloc_dt [0:N-1];
   reg [4*DWIDTH-1:0] alloc_min_dt;
   reg [6:0] alloc_index_dt;
   min #(.DWIDTH(4*DWIDTH), .DDEPTH(N)) min_inst0 (alloc_min_dt, alloc_index_dt, alloc_valid_dt, alloc_dt, 
                                                        clk, alloc_start_min, alloc_reset_min);
   reg [6:0] start_min_cnt;
   always @(posedge clk)
       if (current_state != ALLOC)
           start_min_cnt <= 0;
       else if (alloc_start_min)
           start_min_cnt <= start_min_cnt + 1;

   initial reset_loop = 0;
   always @(posedge clk)
   begin : ALLOC_PROC
        if (current_state == ALLOC) begin
            if (data_count == 228) begin
                if (start_min_cnt < 2) begin
                    alloc_start_min <= 1;
                    alloc_reset_min <= 0;
                    foreach (alloc_dt[n]) alloc_dt[n] <= dt_temp2[n];
                end
                else  begin
                    reset_loop <= 0;
                    alloc_start_min <= 1;
                    alloc_reset_min <= 0;
                end
            end
            else if (data_count == 231)
                alloc_start_min <= 0;
            else if (alloc_valid_dt && (E_so_far+alloc_min_dt < Ex_N)) begin
                E_so_far <= E_so_far + alloc_min_dt;
                En[alloc_index_dt] <= En[alloc_index_dt] + alloc_min_dt;
                bn[alloc_index_dt] <= bn[alloc_index_dt] + 1;
                if (alloc_index_dt == 0 || alloc_index_dt == N-1)
                    alloc_dt[alloc_index_dt] <= 4*alloc_dt[alloc_index_dt];
                else
                    alloc_dt[alloc_index_dt] <= 2*alloc_dt[alloc_index_dt];
                reset_loop <= 1;
            end
        end
   end
   reg [4*DWIDTH-1:0] Ex_N;
   initial Ex_N = (N-1)*2*{24'd0, Ex_bar, 24'd0};
   wire break_sig;
   assign break_sig = (current_state == ALLOC && (E_so_far+alloc_min_dt) > Ex_N )? 1'b1 : 1'b0;
   //DEBUG SIGNALS
   wire [4*DWIDTH-1:0] alloc_dt_debug, alloc0, alloc1, alloc2, alloc3, alloc4;
   assign alloc_dt_debug = alloc_dt[alloc_index_dt];
   assign alloc0 = alloc_dt[0];
   assign alloc1 = alloc_dt[1];
   assign alloc2 = alloc_dt[2];
   assign alloc3 = alloc_dt[3];
   assign alloc4 = alloc_dt[4];
   wire [4*DWIDTH-1:0] En0, En1, En2, En3, En4;
   assign En0 = En[0];
   assign En1 = En[1];
   assign En2 = En[2];
   assign En3 = En[3];
   assign En4 = En[4];
   wire [DWIDTH-1:0] bn0, bn1, bn2, bn3, bn4;
   assign bn0 = bn[0];
   assign bn1 = bn[1];
   assign bn2 = bn[2];
   assign bn3 = bn[3];
   assign bn4 = bn[4];

   always @(posedge clk) bit_vector_tvalid <= break_sig;
   always @(posedge clk) begin
       if (break_sig)
           foreach (bn[w]) bit_vector[w] <= bn[w];
   end
     
endmodule
