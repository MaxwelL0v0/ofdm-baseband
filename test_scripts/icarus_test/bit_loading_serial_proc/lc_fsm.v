`include "div.v"
`include "min.v"
module lc_fsm #(
    parameter DWIDTH = 16,
    parameter N = 5
    )(
    input wire   clk,
    input wire   reset,
    input wire   start,
    output reg  [DWIDTH-1:0] bit_vector [0:N-1],
    output reg  bit_vector_tvalid
    );

    reg signed [DWIDTH-1:0] p [0:1]; 
    reg        [4*DWIDTH-1:0] En [0:N-1]; 
    reg        [DWIDTH-1:0] bn [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_re [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_im [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_re_norm [0:N-1]; 
    reg signed [DWIDTH-1:0] Hn_im_norm [0:N-1]; 
    reg signed [DWIDTH-1:0] Ex_bar;
    reg signed [2*DWIDTH-1:0] Noise_var, gap; 
    integer i;
    reg signed [2*DWIDTH-1:0] Hn_re_temp [0:N-1];
    reg signed [2*DWIDTH-1:0] Hn_im_temp [0:N-1];
    reg signed [2*DWIDTH-1:0] gn_temp1 [0:N-1];

    //INICIALIZATION
    initial begin
        p[1]    = 16'b00000000_11100110;  // 0.8984375
        p[0]    = 16'b00000001_00000000;  // 1.0
        Ex_bar  = 16'b00000001_00000000; // 1.0
        Noise_var = 32'b0000000000000000_0010111001010110; // Varianza ruido = 0.180999755859375
        gap       = 32'b0000000000000001_0000000000000000; // gap  = 1.0 => 0 dB
        for (i=0; i<=N-1; i = i+1) begin
            bn[i] = 16'd0;
            En[i] = 64'd0;
            Hn_re[i] = 16'h0000;
            Hn_im[i] = 16'h0000;
        end
        //normalized Hn-LUT for Ntot = 8 => len(Hn) = 5
        //Hn_norm_re = [1, sqrt(2)/2, 0, -sqrt(2)/2, -1]
        Hn_re_norm[0] = 16'b00000001_00000000; // 1.0
        Hn_re_norm[1] = 16'b00000000_10110101; // 0.70703125
        Hn_re_norm[2] = 16'b00000000_00000000; // 0.0
        Hn_re_norm[3] = 16'b11111111_01001011; //-0.70703125
        Hn_re_norm[4] = 16'b11111111_00000000; //-1.0
        //Hn_norm_im = [0, sqrt(2)/2, 1, sqrt(2)/2, 0]
        Hn_im_norm[0] = 16'b00000000_00000000; // 0.0
        Hn_im_norm[1] = 16'b00000000_10110101; // 0.70703125
        Hn_im_norm[2] = 16'b00000001_00000000; // 1.0
        Hn_im_norm[3] = 16'b00000000_10110101; // 0.70703125
        Hn_im_norm[4] = 16'b00000000_00000000; // 0.0
        
        foreach (Hn_re[j]) begin
            Hn_re_temp[j] = p[1]*Hn_re_norm[j];
            Hn_im_temp[j] = p[1]*Hn_im_norm[j];
            Hn_re[j] = Hn_re_temp[j][23:8]+16'b00000001_00000000;
            Hn_im[j] = Hn_im_temp[j][23:8];
            gn_temp1[j] = Hn_re[j]*Hn_re[j]+Hn_im[j]*Hn_im[j];
        end
    end
   wire [2*DWIDTH-1:0] gn_temp1_0, gn_temp1_1, gn_temp1_2, gn_temp1_3, gn_temp1_4;
   assign gn_temp1_0 = gn_temp1[0];
   assign gn_temp1_1 = gn_temp1[1];
   assign gn_temp1_2 = gn_temp1[2];
   assign gn_temp1_3 = gn_temp1[3];
   assign gn_temp1_4 = gn_temp1[4];

    reg start_i;
    reg reset_loop;
    reg [1:0] current_state, next_state;
    always @(posedge clk) start_i <= start;
    

    // STATE MACHINE FOR LEVIN CAMPELLO
    //STATE CODIFICATION
    localparam IDLE  = 2'b00;
    localparam GN_S  = 2'b01;
    localparam DT_S  = 2'b10;
    localparam ALLOC = 2'b11;
    //STATE REGISTERS
    // Next state logic
    always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   : begin
               if (start_i)
                   next_state = GN_S;
               else
                   next_state = current_state;
          end
          GN_S  : begin
               //if (data_count == 126+500)
               if (sc_index == N)
                   next_state = DT_S;
               else
                   next_state = current_state;
          end
          DT_S  : begin
               //if (data_count == 227+500)
               if (sc_index == N)
                   next_state  = ALLOC;
               else
                   next_state = current_state;
          end
          ALLOC  :
               if (break_sig) 
                   next_state = IDLE;
               else
                   next_state = current_state;

          default:
                next_state = current_state;
        endcase
     end
    // State memory
   always @(posedge clk)
   begin : STATE_MEMORY
    if(reset) begin
           current_state <= IDLE;
    end
	else begin
           current_state <= next_state;
	end
   end
    //Find gn vector serial proc
   reg [4*DWIDTH-1:0] gn_tmp;
   reg [4*DWIDTH-1:0] gn_tmp3[0:N-1];
   reg gn_s_start_div, gn_s_reset_div;
   wire gn_s_valid_div, gn_s_busy_div, gn_s_done_div, gn_s_dbz_div, gn_s_ovf_div;
  
    //Count data in GN_S state
    reg [12:0] dcount_gn = {13'd0};
    always @(posedge clk)
    begin : DCOUNT_GN_PROC
        if (reset)
            dcount_gn <= 0;
        else if (gn_s_done_div)
            dcount_gn <= 0;
        else if (current_state == GN_S)
            dcount_gn <= dcount_gn + 1;
        else
            dcount_gn <= 0;
    end
    //Reset div module when dcount = 0
   always @(posedge clk) 
       if (dcount_gn == 0) begin
           gn_s_start_div <= 1;
           gn_s_reset_div <= 0;
       end
       else
           gn_s_start_div <= 0;

   div #(4*DWIDTH, 2*DWIDTH) gn_s_div_inst (clk, gn_s_reset_div, gn_s_start_div, gn_s_busy_div, gn_s_done_div, gn_s_valid_div, 
                                             gn_s_dbz_div, gn_s_ovf_div, 
                                             {16'd0, gn_temp1[sc_index], 16'd0}, {16'd0, Noise_var, 16'd0}, gn_tmp);
   
    //Find dt vector serial proc
   reg [4*DWIDTH-1:0] dt_tmp;
   reg [4*DWIDTH-1:0] dt_n [0:N-1];
   reg dt_s_start_div, dt_s_reset_div;
   wire dt_s_valid_div, dt_s_busy_div, dt_s_done_div, dt_s_dbz_div, dt_s_ovf_div;
  
    //Count data in DT_S state
    reg [12:0] dcount_dt = {13'd0};
    always @(posedge clk)
    begin : DCOUNT_DT_PROC
        if (reset)
            dcount_dt <= 0;
        else if (dt_s_done_div)
            dcount_dt <= 0;
        else if (current_state == DT_S)
            dcount_dt <= dcount_dt + 1;
        else
            dcount_dt <= 0;
    end
    //Reset div module when dcount = 0
   always @(posedge clk) 
       if (dcount_dt == 0 && current_state == DT_S) begin
           dt_s_start_div <= 1;
           dt_s_reset_div <= 0;
       end
       else
           dt_s_start_div <= 0;

   integer t;
   reg [2*DWIDTH-1:0] dt_init [0:N-1];
   always @ (posedge clk)
   begin : DT_PROC
       if (current_state == DT_S) begin 
                dt_init[0] <= 3*gap; //PAM
                dt_init[N-1] <= 3*gap; //PAM
                for (t = 1; t < N-1; t = t + 1) dt_init[t] <= 2*gap; //QAM
        end
   end   
   div #(4*DWIDTH, 2*DWIDTH) dt_s_div_inst (clk, dt_s_reset_div, dt_s_start_div, dt_s_busy_div, dt_s_done_div, dt_s_valid_div, 
                                             dt_s_dbz_div, dt_s_ovf_div, 
                                             {16'd0, dt_init[sc_index], 16'd0}, gn_tmp3[sc_index], dt_tmp);
   //Subcarrier counter
    reg [5:0] sc_index = {6'd0};
    wire sdf_done1, sdf_done2;
    assign sdf_done1 = gn_s_done_div;
    assign sdf_done2 = dt_s_done_div;
    always @(posedge clk)
        if (sc_index == N)
            sc_index <= 0;
        else if (current_state == GN_S && sdf_done1)
            sc_index <= sc_index + 1;
        else if (current_state == DT_S && sdf_done2)
            sc_index <= sc_index + 1;
    
   always @(posedge clk)
       if (gn_s_done_div && current_state == GN_S) 
           gn_tmp3[sc_index] <= gn_tmp;
       else if (dt_s_done_div && current_state == DT_S) 
           dt_n[sc_index] <= dt_tmp;
        
   localparam FBITS = 16;
   
   reg [4*DWIDTH-1:0] E_so_far;
   reg [2*DWIDTH-1:0] dt_temp1 [0:N-1];
   reg [4*DWIDTH-1:0] dt_temp2 [0:N-1];
    //Data counter for alloc state
    reg [12:0] dcount_alloc = {13'd0};
    always @(posedge clk)
    begin : DATA_COUNT_PROC
        if (reset)
            dcount_alloc <= 0;
        else if (reset_loop)
            dcount_alloc <= 0;
        else if (current_state == ALLOC)
            dcount_alloc <= dcount_alloc + 1;
        else
            dcount_alloc <= 0;
    end
   integer n;
   initial E_so_far = 64'd0;
   reg alloc_start_min, alloc_reset_min, alloc_valid_dt;
   initial begin
       alloc_start_min <= 0;
       alloc_reset_min <= 1;
   end
   reg [4*DWIDTH-1:0] alloc_dt [0:N-1];
   reg [4*DWIDTH-1:0] alloc_min_dt;
   reg [6:0] alloc_index_dt;
   min #(.DWIDTH(4*DWIDTH), .DDEPTH(N)) min_inst0 (alloc_min_dt, alloc_index_dt, alloc_valid_dt, alloc_dt, 
                                                        clk, alloc_start_min, alloc_reset_min);
   reg [6:0] start_min_cnt;
   always @(posedge clk)
       if (current_state != ALLOC)
           start_min_cnt <= 0;
       else if (alloc_start_min)
           start_min_cnt <= start_min_cnt + 1;

   initial reset_loop = 0;
   always @(posedge clk)
   begin : ALLOC_PROC
        if (current_state == ALLOC) begin
            if (dcount_alloc == 0) begin
               alloc_start_min <= 1;
               alloc_reset_min <= 0;
               reset_loop <= 0;
               if (start_min_cnt == 0)
               foreach (alloc_dt[n]) alloc_dt[n] <= dt_n[n];
                end
            else if (dcount_alloc ==1)
                alloc_start_min <= 0;
            else if (alloc_valid_dt && (E_so_far+alloc_min_dt < Ex_N)) begin
                E_so_far <= E_so_far + alloc_min_dt;
                En[alloc_index_dt] <= En[alloc_index_dt] + alloc_min_dt;
                bn[alloc_index_dt] <= bn[alloc_index_dt] + 1;
                if (alloc_index_dt == 0 || alloc_index_dt == N-1)
                    alloc_dt[alloc_index_dt] <= 4*alloc_dt[alloc_index_dt];
                else
                    alloc_dt[alloc_index_dt] <= 2*alloc_dt[alloc_index_dt];
                reset_loop <= 1;
            end
            else 
                reset_loop <= 0;
        end
   end
   reg [4*DWIDTH-1:0] Ex_N;
   initial Ex_N = (N-1)*2*{24'd0, Ex_bar, 24'd0};
   wire break_sig;
   assign break_sig = (current_state == ALLOC && (E_so_far+alloc_min_dt) > Ex_N )? 1'b1 : 1'b0;
   //DEBUG SIGNALS
   wire [4*DWIDTH-1:0] dt_n0, dt_n1, dt_n2, dt_n3, dt_n4;
   assign dt_n0 = dt_n[0];
   assign dt_n1 = dt_n[1];
   assign dt_n2 = dt_n[2];
   assign dt_n3 = dt_n[3];
   assign dt_n4 = dt_n[4];
   wire [4*DWIDTH-1:0] alloc0, alloc1, alloc2, alloc3, alloc4;
   assign alloc0 = alloc_dt[0];
   assign alloc1 = alloc_dt[1];
   assign alloc2 = alloc_dt[2];
   assign alloc3 = alloc_dt[3];
   assign alloc4 = alloc_dt[4];
   wire [4*DWIDTH-1:0] En0, En1, En2, En3, En4;
   assign En0 = En[0];
   assign En1 = En[1];
   assign En2 = En[2];
   assign En3 = En[3];
   assign En4 = En[4];
   wire [DWIDTH-1:0] bn0, bn1, bn2, bn3, bn4;
   assign bn0 = bn[0];
   assign bn1 = bn[1];
   assign bn2 = bn[2];
   assign bn3 = bn[3];
   assign bn4 = bn[4];

   always @(posedge clk) bit_vector_tvalid <= break_sig;
   always @(posedge clk) begin
       if (break_sig)
           foreach (bn[w]) bit_vector[w] <= bn[w];
   end
     
endmodule
