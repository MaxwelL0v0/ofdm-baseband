`timescale 1ns/1ps
`include "div_tn20k_test.v"
module tb ();
    parameter WIDTH=8;  // width of numbers in bits (integer and fractional)
    parameter WIDTHU = WIDTH - 1;
    parameter FBITS=4;   // fractional bits within WIDTH;
    reg  clk;    // clock;
    reg  rst;    // reset;
    reg  start;  // start calculation;
    reg   [WIDTH-1:0] a;   // dividend (numerator);
    reg   [WIDTH-1:0] b;   // divisor (denominator);
    localparam ITER = WIDTHU + FBITS;  // iteration count: unsigned width + fractional bits;
    wire  busy;   // calculation in progress;
    wire  done;   // calculation is complete (high for one tick);
    wire  valid;  // result is valid;
    wire  dbz;    // divide by zero;
    wire  ovf;    // overflow;
    wire  true_result;  // result value: quotient;
	div #( WIDTH, FBITS) DUT (clk, rst, start, busy, done, valid, dbz, ovf, true_result);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #0.5 clk = ~clk;
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
            rst <= 1;
        #1  rst <= 0;
        #1  start <= 1;
        #50 $display("Test complete.");
        $finish;
    end
endmodule
