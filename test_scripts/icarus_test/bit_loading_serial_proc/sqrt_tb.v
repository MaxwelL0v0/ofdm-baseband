`timescale 1ns/1ps
`include "sqrt.v"
module tb ();

    parameter WIDTH = 16;
    parameter FBITS = 8;

    reg clk;
    reg start;             // start signal
    wire busy;              // calculation in progress
    wire valid;             // root and rem are valid
    reg [WIDTH-1:0] rad;   // radicand
    wire [WIDTH-1:0] root;  // root
    wire [WIDTH-1:0] rem;   // remainder

    sqrt #(.WIDTH(WIDTH), .FBITS(FBITS)) sqrt_inst (.*);

    //clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #0.5 clk = ~clk;
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
            rad <= 16'b00000000_10000000; //0.5
        #1  start <= 1; #10 start <= 0;
        #50 $display("Test complete.");
        $finish;
    end
endmodule
