module min #(
    parameter DWIDTH = 64,
    parameter DDEPTH = 5
    )(
    output wire [DWIDTH-1:0] min_dt,
    output reg [6:0] index_dt,
    output wire valid_dt,
    input  wire [DWIDTH-1:0] decision_table [0:DDEPTH-1],
    input clk,
    input start,
    input reset
    );
    //Data counter for debugging
    reg [6:0] cnt = {8'd0};
    always @(posedge clk)
    begin : DATA_COUNT_PROC
        if (reset)
            cnt <= 0;
        else begin
            if (current_state != IDLE) begin
                cnt <= cnt + 1;
            end
            else
                cnt <= 0;
        end
    end

    localparam IDLE  = 2'b00;
    localparam COMP  = 2'b01;
    localparam READY  = 2'b10;
    //STATE REGISTERS
    reg [1:0] current_state, next_state;
    // Next state logic
    always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   : begin
               if (start)
                   next_state = COMP;
               else
                   next_state = current_state;
          end
          COMP  : begin
               if (cnt == DDEPTH)
                   next_state = READY;
               else
                   next_state = current_state;
          end
          READY : begin
                   next_state  = IDLE;
          end
          2'b11  :
                next_state = IDLE;
          default:
                next_state = current_state;
        endcase
     end
    // State memory
    reg [DWIDTH-1:0] dt_i [0:DDEPTH-1];
    reg [DWIDTH-1:0] dt_i_resp [0:DDEPTH-1];
   always @(posedge clk)
   begin : STATE_MEMORY
    if(reset) begin
           current_state <= IDLE;
    end
	else begin
           current_state <= next_state;
	end

   end

   reg [DWIDTH-1:0] min_dt_i;
   reg [6:0] index_dt_i;
   initial min_dt_i = 64'd0;
   always @(posedge clk)
   begin : COMP_PROC
       if (cnt == 0 && start) begin
            foreach (dt_i[n]) begin
                dt_i[n] <= decision_table[n];
                dt_i_resp[n] <= decision_table[n];
            end
        end
        else if (current_state == COMP && cnt < DDEPTH) begin
               if (dt_i[cnt-1] < dt_i[cnt]) begin
                  min_dt_i <= dt_i[cnt-1];
                  dt_i[cnt] <= dt_i[cnt-1];
               end
               else begin
                  min_dt_i <= dt_i[cnt];
               end
       end
   end
   integer j;
   always @(posedge clk)
   begin
       if (cnt == DDEPTH) begin
           for (j = 0; j < DDEPTH; j = j+1) begin
               if (dt_i_resp[j] == min_dt_i)
                   index_dt <= j;
           end
       end
   end

   assign min_dt = (current_state == READY)? min_dt_i : 64'd0;
   assign valid_dt = (current_state == READY)? 1'b1 : 1'b0;
endmodule
