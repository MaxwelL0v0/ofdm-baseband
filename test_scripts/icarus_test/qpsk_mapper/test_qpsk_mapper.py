# This file is public domain, it can be freely copied without restrictions.
# SPDX-License-Identifier: CC0-1.0
# test_my_design.py (extended)
import cocotb
from cocotb.triggers import FallingEdge, Timer

async def generate_clock(dut):
    """Generate clock pulses."""

    for cycle in range(30):
        dut.clk.value = 0
        await Timer(1, units="ns")
        dut.clk.value = 1
        await Timer(1, units="ns")

@cocotb.test()
async def my_second_test(dut):
    """Try accessing the design."""
    dut.rst.value  = 1
    dut.data_valid_i.value = 1
    dut.data_ready_o.value = 1
    dut.data_in[0].value = 1
    await cocotb.start(generate_clock(dut))  # run the clock "in the background"
    await Timer(2, units="ns")
    dut.rst.value = 0
    await Timer(40, units="ns")  # wait a bit
    await FallingEdge(dut.clk)  # wait for falling edge/"negedge"

    dut._log.info("I_ch[0] is %x", dut.I_ch[0].value)
    assert dut.I_ch[0].value[0] == 0, "I_ch[0][0] is not 0!"
