module count_up #(
    parameter WIDTH = 4,
    parameter MAX = 10
)(
    output wire [WIDTH-1:0] cnt_out,
    input  wire clk, rst
);

   reg [WIDTH-1:0] CNT;
   always @(posedge clk or negedge rst)
   begin: COUNTER
       if(!rst)
           CNT <= 0;
       else if (CNT==MAX)
           CNT <= 0;
       else
           CNT <= CNT+1;
   end
   assign cnt_out = CNT;
endmodule
