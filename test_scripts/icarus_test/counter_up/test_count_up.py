# This file is public domain, it can be freely copied without restrictions.
# SPDX-License-Identifier: CC0-1.0
# test_my_design.py (extended)
import cocotb
from cocotb.triggers import FallingEdge, Timer

async def generate_clock(dut):
    """Generate clock pulses."""

    for cycle in range(30):
        dut.clk.value = 0
        await Timer(1, units="ns")
        dut.clk.value = 1
        await Timer(1, units="ns")

@cocotb.test()
async def my_second_test(dut):
    """Try accessing the design."""
    dut.rst.value  = 1
    await cocotb.start(generate_clock(dut))  # run the clock "in the background"
    await Timer(2, units="ns")
    dut.rst.value = 0
    await Timer(40, units="ns")  # wait a bit
    await FallingEdge(dut.clk)  # wait for falling edge/"negedge"

    dut._log.info("cnt_out is %s", dut.cnt_out.value)
    assert dut.cnt_out.value[0] == 0, "cnt_out[0] is not 0!"
