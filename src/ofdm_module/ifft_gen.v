`include "ifftmain.v"

module ifft_gen #(
    parameter DDEPTH = 64,
    parameter DWIDTH = 16
)(
    //outputs
    output wire [15:0] CENV_ICH,
    output wire [15:0] CENV_QCH,
    output wire        CENV_SYNC,
    //Slave Port
    input  wire [DWIDTH-1:0]    S_SAMP_TICH,
    input  wire [DWIDTH-1:0]    S_SAMP_TQCH,
    input  wire                 S_SAMP_TVALID,
    output wire                 S_SAMP_TREADY,
    input  wire                 S_SAMP_TLAST,
    //Global 
    input wire ACLK,
    input wire ARESETN
);
    localparam CPLENGTH = 16;
    //Register valid data
    reg samp_tvalid_i;
    reg [DWIDTH-1:0] ich_i, qch_i;
    initial begin
        samp_tvalid_i <= 0;
        ich_i <= 0;
        qch_i <= 0;
    end
    always @(posedge ACLK)
        if (S_SAMP_TVALID) begin
            ich_i <= S_SAMP_TICH;
            qch_i <= S_SAMP_TQCH;
            samp_tvalid_i <= 1;
        end
        else samp_tvalid_i <= 0;
        
    wire ifft_sync;
    wire [DWIDTH-1:0] ifft_re;
    wire [DWIDTH-1:0] ifft_im;
    ifftmain IFFT ( .i_clk(ACLK),
                    .i_reset(!ARESETN),
                    .i_ce(samp_tvalid_i),
                    .i_sample({ich_i, qch_i}),
                    .o_result({ifft_re, ifft_im}),
                    .o_sync(ifft_sync));
    
    reg [DWIDTH-1:0] ifft_re_i;
    reg [DWIDTH-1:0] ifft_im_i;
    reg ifft_sync_i;
    always @(posedge ACLK) begin
        ifft_re_i <= ifft_re;
        ifft_im_i <= ifft_im;
        ifft_sync_i <= ifft_sync;
    end
    
    reg [6:0] samples_cnt = {7'd0};
    always @(posedge ACLK)
    begin : SAMPLES_COUNTER1
        if (ifft_sync)
            samples_cnt <= 0;
        else if(samples_cnt == DDEPTH+CPLENGTH-1)
            samples_cnt <= 0;
        else if(symb_cnt != 0)
            samples_cnt <= samples_cnt + 1;
    end
    
    reg [15:0] symb_cnt = {16'd0};
    always @(posedge ACLK)
        if (ifft_sync)
            if(symb_cnt == 16)
                symb_cnt <= 1;
            else
                symb_cnt <= symb_cnt + 1;
    
    reg dg_sig;    
    reg [DWIDTH-1:0] ICH_MEM1 [0:DDEPTH-1]; 
    reg [DWIDTH-1:0] QCH_MEM1 [0:DDEPTH-1];
    reg [DWIDTH-1:0] ICH_MEM2 [0:DDEPTH-1]; 
    reg [DWIDTH-1:0] QCH_MEM2 [0:DDEPTH-1];
    always @(posedge ACLK)
    begin : FRAME_REG_PROC
        if (symb_cnt != 0) 
            if (samples_cnt <= 53) begin
                dg_sig <= 1;
                ICH_MEM1[samples_cnt] <= ifft_re_i;
                QCH_MEM1[samples_cnt] <= ifft_im_i;
            end
            else if (samples_cnt >= 70) begin
                dg_sig <= 1;
                ICH_MEM1[samples_cnt-CPLENGTH] <= ifft_re_i;
                QCH_MEM1[samples_cnt-CPLENGTH] <= ifft_im_i;
            end
            else
                dg_sig <= 0;
        else
            dg_sig <= 0;
    end
    integer i, j;
    always @(posedge ACLK)
      if (samples_cnt == 0)
        for (i = 0; i < DDEPTH; i = i+1) begin
            for (j = 0; j < DWIDTH; j = j+1) begin
                ICH_MEM2[i][j] = ICH_MEM1[i][j];
                QCH_MEM2[i][j] = QCH_MEM1[i][j];
            end
        end
    reg [DWIDTH-1:0] ich_o; 
    reg [DWIDTH-1:0] qch_o; 
    always @(posedge ACLK)
    begin : CYCLIC_PREFIX_PROC
        if (symb_cnt > 1) begin
            if(samples_cnt < CPLENGTH) begin
                ich_o <= ICH_MEM2[DDEPTH-CPLENGTH+samples_cnt];
                qch_o <= QCH_MEM2[DDEPTH-CPLENGTH+samples_cnt];
            end
            else begin
                ich_o <= ICH_MEM2[samples_cnt-CPLENGTH];
                qch_o <= QCH_MEM2[samples_cnt-CPLENGTH];
            end
        end
        else begin
            ich_o <= 0;
            qch_o <= 0;
        end
    end
    reg ifft_cp;
    always @(posedge ACLK) ifft_cp <= ifft_sync_i;
    assign CENV_SYNC = (symb_cnt > 1)? ifft_cp : 1'b0;
    assign CENV_ICH  = ich_o;
    assign CENV_QCH  = qch_o;
        
    assign S_SAMP_TREADY = 1'b1;
    endmodule 
