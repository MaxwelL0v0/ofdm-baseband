`include "counter_7b.v"
module symbolFormation #(
    parameter DWIDTH = 16
)(
    //Master port
    output wire [DWIDTH-1:0]    M_SAMP_TICH,
    output wire [DWIDTH-1:0]    M_SAMP_TQCH,
    output wire                 M_SAMP_TVALID,
    output wire                 M_SAMP_TLAST,
    input  wire                 M_SAMP_TREADY,

    //Slave port 
    input  wire [DWIDTH-1:0]    S_MAP_TICH,
    input  wire [DWIDTH-1:0]    S_MAP_TQCH,
    input  wire                 S_MAP_TVALID,
    input  wire                 S_MAP_TLAST,
    output wire                 S_MAP_TREADY,
    //Global
    input wire                  ACLK1,
    input wire                  ACLK2,
    input wire                  ARESETN
);
    localparam   DLENGTH = 48; 
    localparam   FFTLENGTH = 64;
    localparam   NUMLGSC = 5;
    localparam   NUMRGSC = 6;
    localparam   CPLENGTH = 16;
    //// store tx_data_i when valid for use later
    reg [DWIDTH-1:0] MEM_RE_1[0:DLENGTH-1];
    reg [DWIDTH-1:0] MEM_IM_1[0:DLENGTH-1];
    reg [DWIDTH-1:0] MEM_RE_2[0:DLENGTH-1];
    reg [DWIDTH-1:0] MEM_IM_2[0:DLENGTH-1];
    
    //internal signals
    wire readytoForm;
    reg [15:0] tx_re_i, tx_im_i; 
    
    //Count data in
    reg [5:0] countData = {6'd0};
    reg [5:0] countData_delay = {6'd0};
    always @(posedge ACLK1) countData_delay <= countData;
    
    always @(posedge ACLK1) begin
        if (S_MAP_TVALID)
            if (countData == 47)
               countData <= 0;
            else 
                countData <= countData + 1;
    end
    always @(posedge S_MAP_TVALID) begin
            MEM_RE_1 [countData] <= S_MAP_TICH;
            MEM_IM_1 [countData] <= S_MAP_TQCH;
    end
    //Store data frame k for processing and  store data frame k+1
    wire store_ofdm_symb;
    integer i, j;
    assign store_ofdm_symb = (next_state == LGSC && countData == 0)? 1'b1: 1'b0;
    always @(posedge store_ofdm_symb) begin
      for (i = 0; i < DLENGTH; i = i+1) begin
          for (j = 0; j < DWIDTH; j = j+1) begin
              MEM_RE_2[i][j] = MEM_RE_1[i][j];
              MEM_IM_2[i][j] = MEM_IM_1[i][j];
          end
      end
    end
    //Symbol counter
    reg [5:0] symb_cnt = {6'd0};
    always @(negedge S_MAP_TLAST)
            symb_cnt <= symb_cnt + 1;
    // Define our states
    reg [3:0] current_state, next_state;
    //States codification
    localparam IDLE        =   4'b0000;
    localparam LGSC        =   4'b0001;
    localparam D1          =   4'b0010;
    localparam P1          =   4'b0011;
    localparam D2          =   4'b0100;
    localparam P2          =   4'b0101;
    localparam D3          =   4'b0110;
    localparam DC          =   4'b0111;
    localparam D4          =   4'b1000;
    localparam P3          =   4'b1001;
    localparam D5          =   4'b1010;
    localparam P4          =   4'b1011;
    localparam D6          =   4'b1100;
    localparam HGSC        =   4'b1101;
    
    //S_CONST_TREADY logic (id_symb max = 7'd15 -> 16QAM)
    assign S_MAP_TREADY = (((current_state == IDLE) || id_symb == 7'd10) && M_SAMP_TREADY)? 1'b1 : 1'b0;

    // Define tx signal constants
    localparam TX_IDLE   = 16'h0000;
    localparam TX_GSC    = 16'd0000;
    localparam TX_DC     = 16'h0000;
    wire LG_done, D1_done, P1_done, D2_done, P2_done;
    wire D3_done, DC_done, D4_done, P3_done;
    wire D5_done, P4_done, D6_done, HG_done;

//Data counting
    assign readytoForm = (current_state != IDLE)? 1'b1 : 1'b0;
    wire  [6:0] id_symb;
    counter_7b_up SYM_COUNTER (
                                .clk(ACLK2),    // input wire CLK
                                .rst(readytoForm),  // input wire SCLR
                                .Q(id_symb)        // output wire [6 : 0] Q
                                );

    assign LG_done = (id_symb == NUMLGSC-1) ? 1'b1 : 1'b0;
    assign D1_done = (id_symb == NUMLGSC+4) ? 1'b1 : 1'b0;
    assign P1_done = (id_symb == 10) ? 1'b1 : 1'b0;
    assign D2_done = (id_symb == NUMLGSC+(17+1)) ? 1'b1 : 1'b0;
    assign P2_done = (id_symb == 24) ? 1'b1 : 1'b0;
    assign D3_done = (id_symb == NUMLGSC+(23+2)) ? 1'b1 : 1'b0;
    assign DC_done = (id_symb == 31) ? 1'b1 : 1'b0;
    assign D4_done = (id_symb == NUMLGSC+(29+3)) ? 1'b1 : 1'b0;
    assign P3_done = (id_symb == 38) ? 1'b1 : 1'b0;
    assign D5_done = (id_symb == NUMLGSC+(42+4)) ? 1'b1 : 1'b0;
    assign P4_done = (id_symb == 52) ? 1'b1 : 1'b0;
    assign D6_done = (id_symb == NUMLGSC+(47+5)) ? 1'b1 : 1'b0;
    assign HG_done = (id_symb == FFTLENGTH+CPLENGTH-1) ? 1'b1 : 1'b0;
// State Machine
// Next state logic
   always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   :
            begin
               if (countData == DLENGTH-1) begin
                  next_state = LGSC;

               end
               else begin
                  next_state = current_state;

               end
            end
          LGSC  :
            begin
               if (LG_done) begin
                  next_state = D1;
               end
               else begin
                  next_state = current_state;

               end
            end
          D1   :
            begin
               if (D1_done) begin
                  next_state = P1;

               end
               else begin
                  next_state = current_state;

               end
            end
          P1 :
            begin
               if (P1_done) begin
                  next_state = D2;
               end
               else begin
                  next_state = current_state;

               end
            end
          D2   :
            begin
               if (D2_done) begin
                  next_state = P2;
               end
               else begin
                  next_state = current_state;

               end
            end
           P2   :
             begin
                if (P2_done) begin
                   next_state = D3;
                end
                else begin
                   next_state = current_state;

                end
              end
            D3   :
              begin
                if (D3_done) begin
                   next_state = DC;
                end
                else begin
                   next_state = current_state;

                end
              end
            DC   :
              begin
                if (DC_done) begin
                   next_state = D4;
                end
                else begin
                   next_state = current_state;

                end
              end
            D4   :
              begin
                if (D4_done) begin
                   next_state = P3;
                end
                else begin
                   next_state = current_state;

                end
              end
            P3   :
              begin
                if (P3_done) begin
                   next_state = D5;
                end
                else begin
                   next_state = current_state;

                end
              end
            D5   :
              begin
                if (D5_done) begin
                   next_state = P4;
                end
                else begin
                   next_state = current_state;

                end
              end
            P4   :
              begin
                if (P4_done) begin
                   next_state = D6;
                end
                else begin
                   next_state = current_state;

                end
              end
            D6   :
              begin
                if (D6_done) begin
                   next_state = HGSC;
                end
                else begin
                   next_state = current_state;

                end
              end
          HGSC   :
            begin
               if (HG_done) begin
                  next_state = LGSC;
               end
               else begin
                  next_state = current_state;

               end
            end
          default:
            next_state = current_state;
        endcase
     end

// State memory
   always @(posedge ACLK2)
     begin : STATE_MEMORY
	if(!ARESETN) begin
           current_state <= IDLE;
	end
	else begin
           current_state <= next_state;
	end

     end
//Output logic
   always @(*)
     begin : OUTPUT_LOGIC
        case (current_state)
          IDLE   :
            begin
               tx_re_i = TX_IDLE;
               tx_im_i = TX_IDLE;
               
            end
          LGSC  :
            begin
               tx_re_i = TX_GSC;
               tx_im_i = TX_GSC;
            end
          D1   :
            begin
               tx_re_i = MEM_RE_2[id_symb-NUMLGSC];
               tx_im_i = MEM_IM_2[id_symb-NUMLGSC];
            end
          P1   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;
            end
          D2   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+1)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+1)];
            end
          P2   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;

            end
          D3   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+2)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+2)];

            end
          DC   :
            begin
               tx_re_i = 0;
               tx_im_i = 0;

            end
          D4   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+3)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+3)];

            end
          P3   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;

            end
          D5   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+4)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+4)];

            end
          P4   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;

            end
          D6   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+5)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+5)];

            end
          HGSC   :
            begin
               tx_re_i = TX_GSC;
               tx_im_i = TX_GSC;

            end
        endcase
     end
   
   assign M_SAMP_TLAST = (current_state == HGSC && id_symb == FFTLENGTH-1)? 1'b1 : 1'b0;
   localparam SF = 1;
   assign M_SAMP_TICH = SF*tx_re_i;
   assign M_SAMP_TQCH = SF*tx_im_i;
   assign M_SAMP_TVALID = (current_state != IDLE && M_SAMP_TREADY && id_symb < FFTLENGTH)? 1'b1 : 1'b0;
endmodule
