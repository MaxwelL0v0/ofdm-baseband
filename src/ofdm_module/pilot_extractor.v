module pilot_extractor #(
    parameter DWIDTH = 16
)(
    //Master port
    output wire [DWIDTH-1:0]    M_MAP_TICH, 
    output wire [DWIDTH-1:0]    M_MAP_TQCH, 
    output wire                 M_MAP_TVALID, 
    //Slave port
    input wire [DWIDTH-1:0]     SAMP_ICH,
    input wire [DWIDTH-1:0]     SAMP_QCH,
    input wire                  SAMP_SYNC,
    input wire                  ACLK,
    input wire                  ARESETN
);

    reg [15:0] samp_re_i, samp_im_i;
    reg frame_valid;
    reg map_valid;

    always @(posedge ACLK, negedge ARESETN) begin
       if (!ARESETN) begin
           frame_valid <= 1'b0;
           map_valid <= 1'b0;
       end
       else if (SAMP_SYNC == 1'b1) begin
            frame_valid <= 1'b1;
        end
       samp_re_i <= SAMP_ICH;
       samp_im_i <= SAMP_QCH;
    end

    reg [6:0] dataCount;
    always @(posedge ACLK) begin
        if (SAMP_SYNC)
            dataCount <= 0;
        else
            dataCount <= dataCount + 1;
    end
   
    localparam NUMLGSC = 5;
    always @(posedge ACLK) begin
       if (frame_valid) begin
            if (dataCount >= NUMLGSC-1 && dataCount < 9)
                map_valid = 1'b1;
            else if (dataCount > 9 && dataCount < 23) 
                map_valid = 1'b1;
            else if (dataCount > 23 && dataCount < 30) 
                map_valid = 1'b1;
            else if (dataCount > 30 && dataCount < 37) 
                map_valid = 1'b1;
            else if (dataCount > 37 && dataCount < 50) 
                map_valid = 1'b1;
            else if (dataCount == 66)
                map_valid = 1'b1;
            else if (dataCount > 67 && dataCount < 73) 
                map_valid = 1'b1;
            else
                map_valid = 1'b0;
       end
    end
    assign M_MAP_TVALID = map_valid;
    assign M_MAP_TICH = samp_re_i;
    assign M_MAP_TQCH = samp_im_i;

endmodule
