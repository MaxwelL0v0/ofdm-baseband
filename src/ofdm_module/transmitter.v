`include "const_mapper.v"
`include "sec_gen.v"
`include "symbol_formation.v"
`include "ifft_gen.v"

module transmitter (
    output wire [15:0] TX_ICH,
    output wire [15:0] TX_QCH,
    output wire        TX_SYNC,
    input wire [2:0] BITV_TDATA [0:47],
    input wire BITV_TVALID, 
    input wire ACLK1, 
    input wire ACLK2, 
    input wire ARESETN
);
//local parameter
localparam DDEPTH = 48;

//output symbol formation signals
wire samp_tvalid, samp_tlast, samp_tready;
wire [15:0] samp_i_ch, samp_q_ch;
//internal signals
wire sec_tdata, sec_tvalid, sec_tready;
wire [15:0] map_q_ch, map_i_ch;
wire map_tvalid, map_tready, map_tlast;
wire [15:0] tx_ich_i, tx_qch_i;
wire tx_sync_i;
sec_gen U1 (.M_SEC_TDATA(sec_tdata),
            .M_SEC_TVALID(sec_tvalid),
            .M_SEC_TREADY(sec_tready),
            .ACLK(ACLK1), 
            .ARESETN(ARESETN));

wire bitv_ready;
//Constellation modulator
const_mapper U2(.M_MAP_TICH(map_i_ch),
                .M_MAP_TQCH(map_q_ch),
                .M_MAP_TVALID(map_tvalid),
                .M_MAP_TREADY(map_tready),
                .M_MAP_TLAST(map_tlast),
                //Slave port 1
                .S_BITV_TDATA(BITV_TDATA),
                .S_BITV_TVALID(BITV_TVALID),
                .S_BITV_TREADY(bitv_ready),
                //Slave port 2
                .S_SEC_TDATA(sec_tdata),
                .S_SEC_TVALID(sec_tvalid),
                .S_SEC_TREADY(sec_tready),
                .ACLK(ACLK1),
                .ARESETN(ARESETN));

symbolFormation U3 (.S_MAP_TICH(map_i_ch),
                    .S_MAP_TQCH(map_q_ch),
                    .S_MAP_TVALID(map_tvalid),
                    .S_MAP_TLAST(map_tlast),
                    .S_MAP_TREADY(map_tready),
                    .M_SAMP_TICH(samp_i_ch),
                    .M_SAMP_TQCH(samp_q_ch),
                    .M_SAMP_TVALID(samp_tvalid),
                    .M_SAMP_TLAST(samp_tlast),
                    .M_SAMP_TREADY(samp_tready), 
                    .ACLK1(ACLK1),
                    .ACLK2(ACLK2),
                    .ARESETN(ARESETN));

ifft_gen U4 (.CENV_ICH(tx_ich_i),
             .CENV_QCH(tx_qch_i),
             .CENV_SYNC(tx_sync_i),
             .S_SAMP_TICH(samp_i_ch),
             .S_SAMP_TQCH(samp_q_ch),
             .S_SAMP_TVALID(samp_tvalid),
             .S_SAMP_TREADY(samp_tready),
             .S_SAMP_TLAST(samp_tlast),
             .ACLK(ACLK2),
             .ARESETN(ARESETN));

assign TX_ICH = tx_ich_i;
assign TX_QCH = tx_qch_i;
assign TX_SYNC = tx_sync_i;

endmodule
