`include "data_gen.v" 
`include "fftmain.v" 
module top #(
    parameter DATA_DEPTH = 80,
    parameter DATA_WIDTH = 16
)(
    //outputs
    output wire [15:0] dataOut_re,
    output wire [15:0] dataOut_im,
    output wire data_valid_o,
    //inputs 
    input wire clk,
    input wire rst
);

    localparam ADDRW = 10;
    localparam CNT_WIDTH=ADDRW;
    localparam CNT_MAX = DATA_DEPTH-1;
    
    wire [DATA_WIDTH-1:0] data;
	data_gen #(.DATA_DEPTH(DATA_DEPTH)) DATA_U ( data, clk, !rst);
    reg [15:0] data_i;
    always @(posedge clk) data_i <= data; 
    reg [15:0] cnt = {16'd0};
    always @(posedge clk)
    begin
        cnt <= cnt + 1;
    end
    reg en_sig = {1'b0};
    fftmain FFT_V2 (clk, rst, en_sig,
                    {data_i, data_i},
                    {dataOut_re, dataOut_im},
                    data_valid_o);

    reg [15:0] frame_cnt = {16'b0};
    always @(posedge data_valid_o) frame_cnt <= frame_cnt + 1;
    
    reg [6:0] cnt2 = {7'd0};
    wire resetn;
    assign resetn = !rst;
    always @(posedge clk or negedge resetn)
    begin: COUNTER2
        if (!resetn)
            cnt2 <= 0;
        else if (cnt2 == 79)
            cnt2 <= 0;
        else
            cnt2 <= cnt2 + 1;
    end
    always @(posedge clk or negedge resetn)
        if (!resetn)
            en_sig <= 0;
        else 
            if (cnt2 >= 64 && cnt2 < 80)
                en_sig <= 1'b0;
            else
                en_sig <= 1'b1;
            
endmodule
