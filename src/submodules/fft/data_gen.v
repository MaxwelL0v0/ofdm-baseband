`include "rom_sync.v"
`include "count_up.v"
module data_gen #(
    parameter DATA_DEPTH = 80,
    parameter DATA_WIDTH = 16,
    parameter INIT_F = "sinLUT.mem",
    parameter ADDRW = 10,
    parameter CNT_WIDTH=ADDRW,
    parameter CNT_MAX = DATA_DEPTH-1,
    parameter INIT_CNT = 0
)(
    output wire [DATA_WIDTH-1:0] data,
    input  wire clk, reset
);
    wire [ADDRW-1:0] addr;

    rom_sync #(DATA_WIDTH, DATA_DEPTH, INIT_F, ADDRW)  DATA_GEN (data, clk, addr);
    count_up #(CNT_WIDTH, CNT_MAX, INIT_CNT) ADDR_GEN (addr, clk, reset);
endmodule
