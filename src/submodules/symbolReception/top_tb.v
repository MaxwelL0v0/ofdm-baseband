`timescale 1ns/1ps
`include "top.v"
module tb ();
    reg clk;
    reg resetn;
    wire [15:0] dataOut_re;
    wire [15:0] dataOut_im;
	top DUT (clk, resetn);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #5 clk = ~clk; //T = 10 ns -> f_clk = 100 M Hz
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
        resetn <= 1'b0;
        #20 resetn <= 1'b1;
        #60000 $display("Test complete.");
        $finish;
    end
endmodule
