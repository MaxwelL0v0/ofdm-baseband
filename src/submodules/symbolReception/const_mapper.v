module const_mapper #(
    parameter DWIDTH = 16,
    parameter DDEPTH = 48,
   // parameter DDEPTH = 64,
    parameter WORD_LENGTH = 2
)(
    //Master port
    output  wire [DWIDTH-1:0] M_MAP_TICH,
    output  wire [DWIDTH-1:0] M_MAP_TQCH,
    output  wire              M_MAP_TVALID,
    output  wire              M_MAP_TLAST,
    input   wire              M_MAP_TREADY,
    //Slave port 1
    input   wire [1:0]        S_BITV_TDATA [0:DDEPTH-1],
    input   wire              S_BITV_TVALID,
    output  wire              S_BITV_TREADY,
    //Salve port 2
    input   wire              S_SEC_TDATA,
    input   wire              S_SEC_TVALID,
    output  reg               S_SEC_TREADY,
    //Global
    input wire                ACLK,
    input wire                ARESETN
);
    //States codification
    localparam INIT        =   2'b00;
    localparam IDLE        =   2'b01;
    localparam SHIFTING    =   2'b10;
    localparam DONE        =   2'b11;
    
    localparam CPLEN = 16;
    // Define tx signal constants
    localparam TX_IDLE = 16'h0000;
    localparam TX_SHIFTING = 16'h0000;

    //Internal registers
    reg                 data_in;
    reg [1:0]           sel;
    reg [2:0]           data_count;
    reg [1:0]           shift_buffer1;
    reg [3:0]           shift_buffer2;
    reg [DWIDTH-1:0]    I_ch;
    reg [DWIDTH-1:0]    Q_ch;
    reg [1:0]  map_mode;
    always @(posedge ACLK) 
        if (sampCount < 48)
            map_mode <= bitv_tdata_i[sampCount];
        else
            map_mode <= 2'b00;
    //Control signals
    wire  bpsk_sel, qpsk_sel, qam16_sel;
    assign bpsk_sel     = (map_mode == 2'b01)? 1'b1 : 1'b0;
    assign qpsk_sel     = (map_mode == 2'b10)? 1'b1 : 1'b0;
    assign qam16_sel    = (map_mode == 2'b11)? 1'b1 : 1'b0;
    // Define our states
    reg [1:0] current_state, next_state;
    //Vector bit register
    reg balloc_done = {1'b0};
    reg [1:0] bitv_tdata_i [0:DDEPTH-1];
    assign S_BITV_TREADY = (current_state == INIT)? 1'b1 : 1'b0;
    integer k;
    always @(ACLK)
        if (S_BITV_TVALID && S_BITV_TREADY)
            for (k=0; k<DDEPTH; k = k +1) bitv_tdata_i[k] <= S_BITV_TDATA[k];
        else
            balloc_done <= 0;
    //Logic to determine S_SEC_TREADY
    always @(posedge ACLK)
        if (!ARESETN) begin
            S_SEC_TREADY <= 1'b0;
        end
        else if (next_state != DONE && sampCount < DDEPTH && current_state != INIT)
                    if (map_mode == 2'b01 && pos_cnt > 1)
                        S_SEC_TREADY <= 1'b0;
                    else if (map_mode == 2'b10 && pos_cnt > 2)
                        S_SEC_TREADY <= 1'b0;
                    //else if (map_mode == 2'b11 && pos_cnt > 3)
                   //     S_SEC_TREADY <= 1'b0;
                    else
                        S_SEC_TREADY <= 1'b1;
        else
                S_SEC_TREADY <= 1'b0;
    //Store data in
    always @(posedge ACLK)
        if (S_SEC_TVALID && S_SEC_TREADY) begin
            data_in <= S_SEC_TDATA;
            shift_buffer1[0] <= S_SEC_TDATA;
            shift_buffer2[0] <= S_SEC_TDATA;
        end
    integer l;
    always @(posedge ACLK) begin
        if (!ARESETN) 
            data_count <= 0;
        else begin
            if (S_SEC_TVALID && S_SEC_TREADY) begin
                data_count <= data_count + 1;
                shift_buffer1[1]  = shift_buffer1[0];
                for (l = 3; l > 0; l = l-1) begin
                      shift_buffer2[l]  = shift_buffer2[l-1];
                end
            end
            else if (current_state != next_state)
                data_count <= 0;
        end
    end

    // STATE MACHINE
    wire shift_done, bpsk_sig, qpsk_sig, qam16_sig;
    assign bpsk_sig = (bpsk_sel && S_SEC_TVALID && S_SEC_TREADY)? 1'b1 : 1'b0;
    assign qpsk_sig = (qpsk_sel && S_SEC_TVALID && S_SEC_TREADY)? 1'b1 : 1'b0;
    assign qam16_sig = (qam16_sel && S_SEC_TVALID && S_SEC_TREADY)? 1'b1 : 1'b0;
    assign shift_done = ((qpsk_sel && data_count == 1) || (qam16_sel && data_count == 3))? 1'b1: 1'b0;
    // Next state logic
    always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          INIT   : begin
               if (S_BITV_TVALID)
                   next_state = IDLE;
               else
                   next_state = current_state;
          end
          IDLE   : begin
               if (bpsk_sig) 
                   next_state = DONE;
               else if (qpsk_sig && pos_cnt < 3) 
                   next_state = SHIFTING;
               else if (qam16_sig) 
                   next_state = SHIFTING;
               else
                  next_state = current_state;
          end
          SHIFTING  : begin
               if (shift_done)
                       next_state = DONE;
               else 
                  next_state = current_state;
          end
          DONE  :  
                next_state = IDLE;
          default:
                next_state = current_state;
        endcase
     end
   reg [2:0] pos_cnt = {3'b000};
   always @(posedge ACLK)
       if (current_state == INIT && next_state == IDLE)
           pos_cnt <= 0;
       else if (pos_cnt == 4)
           pos_cnt <= 0;
       else
           pos_cnt <= pos_cnt + 1;

// State memory
   always @(posedge ACLK)
   begin : STATE_MEMORY
    if(!ARESETN) begin
           current_state <= INIT;
    end
	else begin
           current_state <= next_state;
	end

   end
//Output logic
   always @(*)
     begin : OUTPUT_LOGIC
        case (current_state)
          INIT   :
          begin
            I_ch = TX_IDLE;
            Q_ch = TX_IDLE;
          end
          IDLE   :
          begin
            I_ch = TX_IDLE;
            Q_ch = TX_IDLE;
          end
          SHIFTING   :
          begin
            I_ch = TX_SHIFTING;
            Q_ch = TX_SHIFTING;
          end
          DONE:
          begin
            case (map_mode)
                2'b01: begin
                    case (data_in)
                        1'b0: begin
                            I_ch = -1;
                            Q_ch =  0;
                        end
                        1'b1: begin
                            I_ch =  1;
                            Q_ch =  0;
                        end
                    endcase
                end
                2'b10: begin
                    case (shift_buffer1)
                        2'b00: begin
                            I_ch = -1;
                            Q_ch = -1;
                        end
                        2'b01: begin
                            I_ch = -1;
                            Q_ch =  1;
                        end
                        2'b10: begin
                            I_ch =  1;
                            Q_ch = -1;
                        end
                        2'b11: begin
                            I_ch =  1;
                            Q_ch =  1;
                        end
                        default: begin
                            I_ch =  4;
                            Q_ch =  4;
                        end
                    endcase
                end
                2'b11: begin
                    case (shift_buffer2)
                          4'b0000: begin
                            I_ch = -3;
                            Q_ch = -3;
                          end
                          4'b0001: begin
                            I_ch = -3;
                            Q_ch = -1;
                          end
                          4'b0010: begin
                            I_ch = -3;
                            Q_ch =  3;
                          end
                          4'b0011: begin
                            I_ch = -3;
                            Q_ch =  1;
                          end
                          4'b0100: begin
                            I_ch = -1;
                            Q_ch = -3;
                          end
                          4'b0101: begin
                            I_ch = -1;
                            Q_ch = -1;
                          end
                          4'b0110: begin
                            I_ch = -1;
                            Q_ch =  3;
                          end
                          4'b0111: begin
                            I_ch = -1;
                            Q_ch =  1;
                          end
                          4'b1000: begin
                            I_ch =  3;
                            Q_ch = -3;
                          end
                          4'b1001: begin
                            I_ch =  3;
                            Q_ch = -1;
                          end
                          4'b1010: begin
                            I_ch =  3;
                            Q_ch =  3;
                          end
                          4'b1011: begin
                            I_ch =  3;
                            Q_ch =  1;
                          end
                          4'b1100: begin
                            I_ch =  1;
                            Q_ch = -3;
                          end
                          4'b1101: begin
                            I_ch =  1;
                            Q_ch = -1;
                          end
                          4'b1110: begin
                            I_ch =  1;
                            Q_ch =  3;
                          end
                          4'b1111: begin
                            I_ch =  1;
                            Q_ch =  1;
                          end
                          default: begin
                            I_ch =  4;
                            Q_ch =  4;
                          end
                    endcase
                end
                2'b00: begin
                    I_ch = TX_IDLE;
                    Q_ch = TX_IDLE;
                end
            endcase 
          end
        endcase
   end
   //Master port logic
   wire done_sig;
   reg map_tlast;
   assign done_sig = (current_state == DONE)? 1'b1 : 1'b0;
   reg [6:0] sampCount = 6'd0;
   always @(posedge ACLK)
    begin
        if (!ARESETN)
            sampCount <= 0;
        else if (current_state != INIT && pos_cnt == 4)
            sampCount <= sampCount + 1;
        //else if (M_MAP_TREADY)
        //    sampCount <= 0;
        else if (sampCount > DDEPTH-1 && M_MAP_TREADY)
            sampCount <= 0;
    end
   assign M_MAP_TLAST  = (done_sig && sampCount == DDEPTH)? 1'b1 : 1'b0;
   assign M_MAP_TVALID = (done_sig && sampCount <= DDEPTH)? 1'b1 : 1'b0;
   assign M_MAP_TICH = I_ch;
   assign M_MAP_TQCH = Q_ch;

endmodule
