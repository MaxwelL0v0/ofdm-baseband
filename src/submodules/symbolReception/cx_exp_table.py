#!/usr/bin/env python
import numpy as np

Ntot = 8
Hn = np.zeros(int(Ntot/2+1));
p = [1, 0.9]
f = [0, 0.125, 0.25, 0.375, 0.5]

Hn2 = np.exp(1j*2*np.pi*f[4]*(2-1));
Hn3 = p[1]*Hn2
print(Hn2)
print(Hn3)
