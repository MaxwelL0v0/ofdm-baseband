module frame_ctrl #(
    parameter DWIDTH = 16,
    parameter DDEPTH = 48
)(
    output reg start_bl,
    output reg start_tx,
    output reg [1:0] bitv_tdata[0:DDEPTH-1],
    input wire clk,
    input wire resetn,
    input wire [DWIDTH-1:0] bit_vector[0:4],
    input wire bit_vector_tvalid,
    input wire tx_end
);
    //State codification
    localparam BL = 1'b0;
    localparam TX = 1'b1;
    //State registers
    reg current_state, next_state;
    //Next state logic
    always @(*)
    begin : NEXT_STATE_LOGIC
        case (current_state)
            BL : begin
                if (bit_vector_tvalid)
                    next_state = TX;
                else
                    next_state = current_state;
            end
            TX : begin
                if (tx_end)
                    next_state = BL;
                else
                    next_state = current_state;
            end
            default:
                    next_state = current_state;
         endcase
    end
    //State memory
    always @(posedge clk)
    begin : STATE_MEMORY
        if (!resetn)
            current_state <= BL;
        else
            current_state <= next_state;
    end
    //Counter
    reg [7:0] cnt = {8'b0};
    always @(posedge clk)
        if (next_state != current_state)
            cnt <= 0;
        else if (cnt < 15)
            cnt <= cnt + 1;
    //Output logic
    always @(*)
    begin : OUTPUT_LOGIC
        case (current_state)
            BL : begin
                if(cnt < 10) begin
                    start_bl = 1'b1;
                    start_tx = 1'b0;
                end
                else
                    start_bl = 1'b0;
            end
            TX : begin
                if (cnt < 10) begin
                    start_tx = 1'b1;
                    start_bl = 1'b0;
                end
                else
                    start_tx = 1'b0;
            end
            default: begin
                start_bl = 1'b0; 
                start_tx = 1'b0;
            end
        endcase
    end
    reg [1:0] bit_vector_i [0:DDEPTH-1];
    integer j;
    initial begin
        bit_vector_i[0] <= 2'b01;
        bit_vector_i[1] <= 2'b10;
        bit_vector_i[2] <= 2'b11;
        bit_vector_i[3] <= 2'b01;
        for (j=4; j<DDEPTH; j = j+1) bit_vector_i[j] = 2'b11;
    end
    always @(posedge clk)
       foreach (bitv_tdata[n])  bitv_tdata[n] <= bit_vector_i[n];
endmodule
