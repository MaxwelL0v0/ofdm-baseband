module sec_gen(
    //Master port
    output reg          M_SEC_TDATA,
    output reg [1:0]    M_SEC_TSEL,
    output reg          M_SEC_TVALID,
    input wire          M_SEC_TREADY,
    //Global
    input wire          ACLK,
    input wire          ARESETN
);
    localparam WIDTH = 8;
    localparam MAX = 192;

    reg [WIDTH-1:0]     CNT;
    reg [MAX-1:0]       data_sec = 192'b1101_0010_1011_1110_0001_1001_1010_0110_0101_0101_1001_1101_1001_1100_0111_0101_0110_0101_1011_0010_1001_1110_0010_1110_1101_1100_1001_0001_0011_1110_0101_1001_1000_1011_1010_0111_0000_1010_0100_1100_0111_1000_1101_1110_0110_1010_0010_0101;
    reg next_valid, next_data;
    reg [1:0] next_sel;
    always @(posedge ACLK or negedge ARESETN)
    begin
        if (!ARESETN) begin
            CNT <= 0;
            next_valid <= 0;
            next_sel <= 2'b01;
        end
        else if (M_SEC_TREADY) begin
                next_valid = 1;
                next_data  = data_sec[CNT];
            if(CNT==MAX-1) begin
                CNT <= 0;
            end
            else begin 
                CNT = CNT+1;
            end
        end
        else 
            next_valid = 0;
    end

  //  always @(ACLK)
  //      if (CNT == 0) next_sel <= 2'b00;
  //      else if (CNT == MAX/4) next_sel <= 2'b01;
  //      else if (CNT == (MAX/2)) next_sel <= 2'b10;

    localparam [0:0] OPT_LOWPOWER = 1'b0;

	always @(posedge ACLK)
	if (!ARESETN)
		M_SEC_TVALID <= 0;
	else if (!M_SEC_TVALID || M_SEC_TREADY)
		M_SEC_TVALID <= next_valid;

	always @(posedge ACLK)
	if (OPT_LOWPOWER && !ARESETN)
		M_SEC_TDATA <= 0;
	else if (!M_SEC_TVALID || M_SEC_TREADY)
	begin
		M_SEC_TDATA <= next_data;
        M_SEC_TSEL  <= next_sel;

		if (OPT_LOWPOWER && !next_valid)
			M_SEC_TDATA <= 0;
	end

endmodule

