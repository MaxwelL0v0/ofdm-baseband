`include "ifftmain.v"

module ifft_gen #(
    parameter DDEPTH = 64,
    parameter DWIDTH = 16
)(
    //outputs
    output wire [15:0] dataOut_re,
    output wire [15:0] dataOut_im,
    //Slave Port
    input  wire [DWIDTH-1:0]    S_SAMP_TRE,
    input  wire [DWIDTH-1:0]    S_SAMP_TIM,
    input  wire                 S_SAMP_TVALID,
    output wire                 S_SAMP_TREADY,
    input  wire                 S_SAMP_TLAST,
    //Global 
    input wire ACLK,
    input wire ARESETN
);
    wire o_sync_sig;
    ifftmain IFFT ( .i_clk(ACLK),
                    .i_reset(!ARESETN),
                    .i_ce(S_SAMP_TVALID),
                    .i_sample({S_SAMP_TRE, S_SAMP_TIM}),
                    .o_result({dataOut_re, dataOut_im}),
                    .o_sync(o_sync_sig)); 
    //reg [DWIDTH-1:0] MEM_RE[0:DDEPTH-1];
    //reg [DWIDTH-1:0] MEM_IM[0:DDEPTH-1];
    assign S_SAMP_TREADY = 1'b1;
        
    endmodule 
