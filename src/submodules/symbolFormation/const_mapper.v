/* verilator lint_off WIDTHEXPAND */
module const_mapper #(
    parameter DWIDTH = 16,
    parameter DDEPTH = 48,
    parameter WORD_LENGTH = 2
)(
    //Master port
    output  wire [DWIDTH-1:0] M_MAP_TICH,
    output  wire [DWIDTH-1:0] M_MAP_TQCH,
    output  wire              M_MAP_TVALID,
    output  wire              M_MAP_TLAST,
    input   wire              M_MAP_TREADY,
    //Slave port
    input   wire [1:0]        S_SEC_TSEL,
    input   wire              S_SEC_TDATA,
    input   wire              S_SEC_TVALID,
    output  reg               S_SEC_TREADY,
    //Global
    input wire                ACLK,
    input wire                ARESETN
);
  
    //States codification
    localparam IDLE        =   2'b00;
    localparam SHIFTING    =   2'b01;
    localparam DONE        =   2'b10;

    // Define tx signal constants
    localparam TX_IDLE = 16'h0000;
    localparam TX_SHIFTING = 16'hFF00;

    //Internal registers
    reg                 data_in;
    reg [1:0]           sel;
    reg [2:0]           data_count;
    reg [1:0]           shift_buffer1;
    reg [3:0]           shift_buffer2;
    reg [DWIDTH-1:0]    I_ch;
    reg [DWIDTH-1:0]    Q_ch;

    //Control signals
    wire  bpsk_sec, qpsk_sec, qam16_sig;
    assign bpsk_sig     = (S_SEC_TSEL == 2'b00)? 1'b1 : 1'b0;
    assign qpsk_sig     = (S_SEC_TSEL == 2'b01)? 1'b1 : 1'b0;
    assign qam16_sig    = (S_SEC_TSEL == 2'b10)? 1'b1 : 1'b0;
    // Define our states
    reg [1:0] current_state, next_state;
    
    //Logic to determine S_AXIS_TREADY
    always @(posedge ACLK)
        if (!ARESETN) begin
            S_SEC_TREADY <= 1'b0;
        end
        else if (next_state != DONE && sampCount < DDEPTH)
                    S_SEC_TREADY <= 1'b1;
        else
                S_SEC_TREADY <= 1'b0;
    //Store data in
    always @(posedge ACLK)
        if (S_SEC_TVALID && S_SEC_TREADY) begin
            data_in <= S_SEC_TDATA;
            sel     <= S_SEC_TSEL;
            shift_buffer1[0] <= S_SEC_TDATA;
            shift_buffer2[0] <= S_SEC_TDATA;
        end

    always @(posedge ACLK) begin
        if (!ARESETN) 
            data_count <= 0;
        else begin
            if (S_SEC_TVALID && S_SEC_TREADY) begin
                data_count <= data_count + 1;
                shift_buffer1  = shift_buffer1 << 1;
                shift_buffer2  = shift_buffer2 << 1;
            end
            else if (current_state != next_state)
                data_count <= 0;
        end
    end

    // STATE MACHINE
    // Next state logic
    always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   : begin
               if (bpsk_sig && S_SEC_TVALID && S_SEC_TREADY) 
                   next_state = DONE;
               else if (!bpsk_sig && S_SEC_TVALID && S_SEC_TREADY) 
                   next_state = SHIFTING;
               else
                  next_state = current_state;
          end
          SHIFTING  : begin
               if ((qpsk_sig && data_count == 1) || (qam16_sig && data_count == 3))
                       next_state = DONE;
               else 
                  next_state = current_state;
          end
          DONE  :  
                next_state = IDLE;
          2'b11  : 
                next_state = IDLE;
          default:
                next_state = current_state;
        endcase
     end

// State memory
   always @(posedge ACLK)
   begin : STATE_MEMORY
    if(!ARESETN) begin
           current_state <= IDLE;
    end
	else begin
           current_state <= next_state;
	end

   end
//Output logic
   always @(current_state)
     begin : OUTPUT_LOGIC
        case (current_state)
          IDLE   :
          begin
            I_ch = TX_IDLE;
            Q_ch = TX_IDLE;
          end
          SHIFTING   :
          begin
            I_ch = TX_SHIFTING;
            Q_ch = TX_SHIFTING;
          end
          DONE:
          begin
            case (sel)
                2'b00: begin
                    case (data_in)
                        1'b0: begin
                            I_ch = -1;
                            Q_ch =  0;
                        end
                        1'b1: begin
                            I_ch =  1;
                            Q_ch =  0;
                        end
                    endcase
                end
                2'b01: begin
                    case (shift_buffer1)
                        2'b00: begin
                            I_ch = -1;
                            Q_ch = -1;
                        end
                        2'b01: begin
                            I_ch = -1;
                            Q_ch =  1;
                        end
                        2'b10: begin
                            I_ch =  1;
                            Q_ch = -1;
                        end
                        2'b11: begin
                            I_ch =  1;
                            Q_ch =  1;
                        end
                        default: begin
                            I_ch =  4;
                            Q_ch =  4;
                        end
                    endcase
                end
                2'b10: begin
                    case (shift_buffer2)
                          4'b0000: begin
                            I_ch = -3;
                            Q_ch = -3;
                          end
                          4'b0001: begin
                            I_ch = -3;
                            Q_ch = -1;
                          end
                          4'b0010: begin
                            I_ch = -3;
                            Q_ch =  3;
                          end
                          4'b0011: begin
                            I_ch = -3;
                            Q_ch =  1;
                          end
                          4'b0100: begin
                            I_ch = -1;
                            Q_ch = -3;
                          end
                          4'b0101: begin
                            I_ch = -1;
                            Q_ch = -1;
                          end
                          4'b0110: begin
                            I_ch = -1;
                            Q_ch =  3;
                          end
                          4'b0111: begin
                            I_ch = -1;
                            Q_ch =  1;
                          end
                          4'b1000: begin
                            I_ch =  3;
                            Q_ch = -3;
                          end
                          4'b1001: begin
                            I_ch =  3;
                            Q_ch = -1;
                          end
                          4'b1010: begin
                            I_ch =  3;
                            Q_ch =  3;
                          end
                          4'b1011: begin
                            I_ch =  3;
                            Q_ch =  1;
                          end
                          4'b1100: begin
                            I_ch =  1;
                            Q_ch = -3;
                          end
                          4'b1101: begin
                            I_ch =  1;
                            Q_ch = -1;
                          end
                          4'b1110: begin
                            I_ch =  1;
                            Q_ch =  3;
                          end
                          4'b1111: begin
                            I_ch =  1;
                            Q_ch =  1;
                          end
                          default: begin
                            I_ch =  4;
                            Q_ch =  4;
                          end
                    endcase
                end
                2'b11: begin
                    I_ch = TX_IDLE;
                    Q_ch = TX_IDLE;
                end
            endcase 
          end
          2'b11:
          begin
            I_ch = TX_IDLE;  
            Q_ch = TX_IDLE;  
          end
        endcase
   end
   //Master port logic
   wire done_sig;
   reg map_tlast;
   assign done_sig = (current_state == DONE)? 1'b1 : 1'b0;
   reg [5:0] sampCount = 6'd0;
   always @(posedge done_sig)
   begin
       if (sampCount <= DDEPTH) 
        sampCount <= sampCount + 1;
   end
   always @(posedge M_MAP_TREADY) 
       if (sampCount == DDEPTH) sampCount <= 0;
   
   assign M_MAP_TLAST  = (done_sig && sampCount == DDEPTH)? 1'b1 : 1'b0;
   assign M_MAP_TVALID = (done_sig && sampCount <= DDEPTH)? 1'b1 : 1'b0;
   assign M_MAP_TICH = I_ch;
   assign M_MAP_TQCH = Q_ch;

endmodule
