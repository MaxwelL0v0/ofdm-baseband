`include "const_mapper.v"
`include "sec_gen.v"
`include "symbolFormation.v"
`include "ifft_gen.v"
`include "clock_divider.v"

module top (
    output wire [15:0] dataOut_re,
    output wire [15:0] dataOut_im,
    input wire clk, 
    input wire resetn
);
//output symbol formation signals
wire samp_tvalid, samp_tlast, samp_tready;
wire [15:0] samp_i_ch, samp_q_ch;
wire clk2;
//internal signals
wire sec_tdata, sec_tvalid, sec_tready;
wire [1:0] sec_tsel;
wire [15:0] map_q_ch, map_i_ch;
wire map_tvalid, map_tready, map_tlast;
sec_gen U1 (.M_SEC_TDATA(sec_tdata),
            .M_SEC_TSEL(sec_tsel),
            .M_SEC_TVALID(sec_tvalid),
            .M_SEC_TREADY(sec_tready),
            .ACLK(clk), 
            .ARESETN(resetn));

const_mapper U2(.M_MAP_TICH(map_i_ch),
                .M_MAP_TQCH(map_q_ch),
                .M_MAP_TVALID(map_tvalid),
                .M_MAP_TREADY(map_tready),
                .M_MAP_TLAST(map_tlast),
                .S_SEC_TSEL(sec_tsel),
                .S_SEC_TDATA(sec_tdata),
                .S_SEC_TVALID(sec_tvalid),
                .S_SEC_TREADY(sec_tready),
                .ACLK(clk),
                .ARESETN(resetn));

parameter DIVISOR = 28'd5;
clock_divider #(DIVISOR) UCD (clk2, clk);

symbolFormation U3 (.S_MAP_TRE(map_i_ch),
                    .S_MAP_TIM(map_q_ch),
                    .S_MAP_TVALID(map_tvalid),
                    .S_MAP_TLAST(map_tlast),
                    .S_MAP_TREADY(map_tready),
                    .M_SAMP_TRE(samp_i_ch),
                    .M_SAMP_TIM(samp_q_ch),
                    .M_SAMP_TVALID(samp_tvalid),
                    .M_SAMP_TLAST(samp_tlast),
                    .M_SAMP_TREADY(samp_tready), 
                    .ACLK(clk),
                    .ACLK2(clk2),
                    .ARESETN(resetn));

ifft_gen U4 (.dataOut_re(dataOut_re),
             .dataOut_im(dataOut_im),
             .S_SAMP_TRE(samp_i_ch),
             .S_SAMP_TIM(samp_q_ch),
             .S_SAMP_TVALID(samp_tvalid),
             .S_SAMP_TREADY(samp_tready),
             .S_SAMP_TLAST(samp_tlast),
             .ACLK(clk2),
             .ARESETN(resetn));

endmodule
