#Test para qpsk_mapper.v con cocotb
import random
import cocotb
from cocotb.triggers import RisingEdge, FallingEdge, Timer

# Rutina para reset
async def rst_dut(rst, duration_ns):
    rst.value = 1
    await Timer(duration_ns, units="ns")
    rst.value = 0
    rst._log.debug("Reset complete")

#Rutina para clock ->  f= 500 MHz ( periodo T = 2ns)
async def generate_clock(dut):
    """Generate clock pulses."""
    global clk
    clk = dut.clk
    for cycle in range(100):
        clk.value = 0
        await Timer(1, units="ns")
        clk.value = 1
        await Timer(1, units="ns")

async def generate_input(dut):
    """Generate input bits."""
    data_ready_i = dut.data_ready_i
    clk = dut.clk
    await FallingEdge(dut.rst)
    dut.data_ready_o = 1
    bits_sec = [0, 0, 0, 1, 1, 0, 1, 1]
    k = 0
    while (k < len(bits_sec)):
        await RisingEdge(clk)
        if (data_ready_i.value.binstr == "1"):
            dut.data_in[0].value = bits_sec[k]
            dut.data_valid_i.value = 1
            print(bits_sec[k])
            k = k+1
            await FallingEdge(clk)
            dut.data_valid_i.value = 0
        else:
            await RisingEdge(data_ready_i)


#Rutina de verificación inicial
async def valid_output_assert(dut):
    """Rutina de verificación de salida"""
    data_valid_o = dut.data_valid_o
    for cycle in range(30):
        await RisingEdge(data_valid_o)
        await Timer(1, units="ps")
        print("data_shift_buffer[0] = ", dut.data_shift_buffer[0].value.binstr,  
              " -> I_ch[0] = ", dut.I_ch[0].value.integer,  
              "  Q_ch[0] = ", dut.Q_ch[0].value.integer)
        #if (dut.data_shift_buffer[0].value.binstr == "11"): 
        #     assert dut.I_ch[0].value.integer == 1, "I_ch[0] is not 1!"
        #     assert dut.Q_ch[0].value.integer == 1, "Q_ch[0] is not 1!"

@cocotb.test()
async def module_test(dut):
    """Try accessing the design."""
   # print(dir(dut))
    rst = dut.rst
    # Run reset_dut concurrently
    rst_thread = cocotb.start_soon(rst_dut(rst, duration_ns=5))
    # Print output cannels when valid
    await  cocotb.start(valid_output_assert(dut)) 
    await cocotb.start(generate_clock(dut))  # run the clock "in the background"
    await cocotb.start(generate_input(dut))
    await Timer(2, units="ns")
    await Timer(90, units="ns")  # wait a bit
    await FallingEdge(clk)  # wait for falling edge/"negedge"

