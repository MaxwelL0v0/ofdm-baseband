/* verilator lint_off WIDTHEXPAND */
module qpsk_mapper #(
    parameter DWIDTH = 16,
    parameter DDEPTH = 47,
    parameter WORD_LENGTH = 2
)(
    output wire [DWIDTH-1:0] I_ch[0:DDEPTH-1],
    output wire [DWIDTH-1:0] Q_ch[0:DDEPTH-1],
    output wire data_valid_o,
    output wire data_ready_i,
    input wire data_in[0:DDEPTH-1],
    input wire data_valid_i,
    input wire data_ready_o,
    input wire clk,
    input rst
);
reg [DWIDTH-1:0] I_ch_i[0:DDEPTH-1];
reg [DWIDTH-1:0] Q_ch_i[0:DDEPTH-1];

wire idle_sig;

// count the data
   localparam DATA_COUNTER_MAX = WORD_LENGTH;
   localparam DATA_COUNTER_SIZE = $clog2(DATA_COUNTER_MAX);

   reg [DATA_COUNTER_SIZE-1:0] data_counter;
   wire                        data_done;
   
   // buffer to store tx data while shifting
   reg [WORD_LENGTH-1:0] 	 data_shift_buffer [0:DDEPTH-1];

//Data counting and shifting
    always @(posedge clk)
    begin
	    if(rst)
        begin
            data_counter <= 0;
            foreach (data_shift_buffer[i]) 
                data_shift_buffer[i] <= 2'b00;
        end
        else if (data_valid_i && data_ready_i) 
        begin
            data_counter <= data_counter + 1;
            foreach (data_shift_buffer[i]) begin
                data_shift_buffer[i] <= {data_shift_buffer[i][0],data_in[i]};
                end
        end
    end
   assign data_done = (data_counter == WORD_LENGTH-1) ? 1'b1 : 1'b0;
// Define our states
    reg [1:0] current_state, next_state;
//States codification
    localparam IDLE        =   2'b00;
    localparam SHIFTING    =   2'b01;
    localparam COMPARE     =   2'b10;

// Define tx signal constants
   localparam TX_IDLE = 16'h0000;
   localparam TX_SHIFTING = 16'hFF00;

// State Machine
// Next state logic
   always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   :
            begin
               if (data_valid_i && data_ready_i) begin
                  next_state = SHIFTING;

               end
               else begin
                  next_state = current_state;

               end
            end
          SHIFTING   :
            begin
               if (data_done) begin
                  next_state = COMPARE;
               end
               else begin
                  next_state = current_state;

               end
            end
          COMPARE   :
            begin
                  next_state = IDLE;
            end
          2'b11    :
                  next_state = IDLE;
          default:
            next_state = current_state;
        endcase
     end

// State memory
   always @(posedge clk)
     begin : STATE_MEMORY
	if(rst) begin
           current_state <= IDLE;
	end
	else begin
           current_state <= next_state;
	end

     end
//Output logic
   always @(*)
     begin : OUTPUT_LOGIC
        case (current_state)
          IDLE   :
            begin
               foreach (I_ch_i[i]) begin
                    I_ch_i[i] =  TX_IDLE;
                    Q_ch_i[i] =  TX_IDLE;
               end
            end
          SHIFTING :
            begin
               foreach (I_ch_i[i]) begin
                    I_ch_i[i] =  TX_SHIFTING;
                    Q_ch_i[i] =  TX_SHIFTING;
               end
            end
          COMPARE   :
            begin
                foreach (I_ch_i[i]) begin
                    case (data_shift_buffer[i][1]) 
                        1'b0 :  I_ch_i[i] = -1;
                        1'b1 :  I_ch_i[i] = 1;
                        default: I_ch_i[i] = 4;
                    endcase
                end
                foreach (Q_ch_i[i]) begin
                    case (data_shift_buffer[i][0]) 
                        1'b0 :  Q_ch_i[i] = -1;
                        1'b1 :  Q_ch_i[i] = 1;
                        default: Q_ch_i[i] = 4;
                    endcase
                end
            end
            2'b11    :
               begin
                   foreach (I_ch_i[i]) begin
                        I_ch_i[i] =  TX_IDLE;
                        Q_ch_i[i] = TX_IDLE;
                   end
               end
        endcase
     end

   assign idle_sig = (current_state == IDLE) ? 1'b1 : 1'b0;
   assign data_ready_i = idle_sig && data_ready_o;
   assign data_valid_o = (current_state == COMPARE) ? 1'b1 : 1'b0;
   assign I_ch = I_ch_i;
   assign Q_ch = Q_ch_i;
   initial
    begin
        $dumpfile("dump.vcd");
        //input signals
        $dumpvars(0, clk, rst, data_in[0], data_valid_i, data_ready_o);
        //output signals
        $dumpvars(0, I_ch[0], Q_ch[0], data_valid_o, data_ready_i);
        //internal signals
        $dumpvars(0, data_counter, idle_sig, data_done, data_shift_buffer[0], current_state, next_state);
    end
endmodule
