`timescale 1ns/1ps
`include "top.v"
module tb ();
    reg map_tready;
    reg clk;
    reg resetn;
    wire [15:0] map_i_ch;
    wire [15:0] map_q_ch;
    wire map_tvalid, map_tlast;
	top DUT (map_i_ch, map_q_ch, map_tvalid, map_tlast, map_tready, clk, resetn);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #0.5 clk = ~clk;
    end

    always @(posedge map_tlast) begin
            map_tready <= 1'b0;
            #5  map_tready <= 1'b1; end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
        resetn <= 1'b0;
        #5 resetn <= 1'b1;
        map_tready <= 1'b1;
        #400 $display("Test complete.");
        $finish;
    end
endmodule
