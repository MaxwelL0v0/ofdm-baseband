`include "const_mapper.v"
`include "sec_gen.v"

module top (
    output wire [15:0] map_i_ch,
    output wire [15:0] map_q_ch,
    output wire map_tvalid,
    output wire map_tlast,
    input wire map_tready,
    input wire clk, 
    input wire resetn
);

//internal signals
wire sec_tdata, sec_tvalid, sec_tready;
wire [1:0] sec_tsel;

sec_gen U1 (.M_SEC_TDATA(sec_tdata),
            .M_SEC_TSEL(sec_tsel),
            .M_SEC_TVALID(sec_tvalid),
            .M_SEC_TREADY(sec_tready),
            .ACLK(clk), 
            .ARESETN(resetn));

const_mapper U2( .M_MAP_TICH(map_i_ch),
                        .M_MAP_TQCH(map_q_ch),
                        .M_MAP_TVALID(map_tvalid),
                        .M_MAP_TREADY(map_tready),
                        .M_MAP_TLAST(map_tlast),
                        .S_SEC_TSEL(sec_tsel),
                        .S_SEC_TDATA(sec_tdata),
                        .S_SEC_TVALID(sec_tvalid),
                        .S_SEC_TREADY(sec_tready),
                        .ACLK(clk),
                        .ARESETN(resetn));
endmodule
