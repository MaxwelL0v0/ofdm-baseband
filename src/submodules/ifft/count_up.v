module count_up #(
    parameter WIDTH = 4,
    parameter MAX = 10,
    parameter INIT_CNT = 0
)(
    output reg [WIDTH-1:0] CNT,
    input  wire clk, reset
);
   initial 
       CNT <= INIT_CNT;
   always @(posedge clk or negedge reset)
   begin: COUNTER
       if(!reset)
           CNT <= INIT_CNT;
       else if (CNT==MAX)
           CNT <= 0;
       else
           CNT <= CNT+1;
   end
endmodule
