`include "data_gen.v" 
`include "fftmain.v"
`include "ifftmain.v"

module top #(
    parameter DATA_DEPTH = 64,
    parameter DATA_WIDTH = 16
)(
    //outputs
    output wire [15:0] dataOut_re,
    output wire [15:0] dataOut_im,
    output wire data_valid_o_fft, data_valid_o_ifft,
    //inputs 
    input wire clk,
    input wire rst
);

    localparam ADDRW = 10;
    localparam CNT_WIDTH=ADDRW;
    localparam CNT_MAX = DATA_DEPTH-1;
    
    wire [DATA_WIDTH-1:0] data;
	data_gen #(.DATA_DEPTH(DATA_DEPTH)) DATA_U ( data, clk, !rst);
    
    wire [DATA_WIDTH-1:0] dataS1_re, dataS1_im;
    fftmain FFT_U (clk, rst, 1'b1,
                    {data, 16'd0},
                    {dataS1_re, dataS1_im},
                    data_valid_o_fft);
    
    reg start_ifft_sig;
    always @(posedge data_valid_o_fft)
    begin : START_IFFT_PROC
        assign start_ifft_sig  = 1'b1;
    end
    ifftmain IFFT_U (clk, rst, start_ifft_sig,
                    {dataS1_re, dataS1_im},
                    {dataOut_re, dataOut_im},
                    data_valid_o_ifft);
endmodule
