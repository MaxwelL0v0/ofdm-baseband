module rom_sync #(
    parameter WIDTH=8,
    parameter DEPTH=4,
    parameter INIT_F="",
    parameter ADDRW=2
    ) (
    output reg  [WIDTH-1:0] data,
    input wire              clk,
    input wire  [ADDRW-1:0] addr
    );
    reg [WIDTH-1:0] memory [0:DEPTH-1];
    initial begin
        if (INIT_F != 0) begin

            $display("Creando rom_async de archivo init '%s'.", INIT_F);

            $readmemh(INIT_F, memory);
        end
    end
    always @ (posedge clk) data = memory[addr];
endmodule

