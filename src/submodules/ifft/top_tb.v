`timescale 1ns/1ps
`include "top.v"
module tb ();
    parameter DATA_DEPTH = 64;
    parameter DATA_WIDTH = 16;
    //inputs ;
    reg clk;
    reg rst;
    //outputs;
    wire [15:0] dataOut_re;
    wire [15:0] dataOut_im;
    wire data_valid_o_fft, data_valid_o_ifft;
	top #(DATA_DEPTH, DATA_WIDTH) DUT (dataOut_re, dataOut_im, data_valid_o_fft, data_valid_o_ifft, clk, rst);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #0.5 clk = ~clk;
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
        rst = 1;
//signals
        #10 rst = 0;
        #500 $display("Test complete.");
        $finish;
    end
endmodule
