`timescale 1ns/1ps
`include "top.v"
module tb ();
    reg clk;
    reg resetn;
    wire [15:0] dataOut_re;
    wire [15:0] dataOut_im;
	top DUT (dataOut_re, dataOut_im, clk, resetn);

//clock
    initial
    begin
        clk = 1'b0;
    end
    always
    begin
        #10 clk = ~clk; //T = 20 ns -> f_clk = 50 MHz
    end

    initial
    begin
        $dumpfile("tb.fst");
        $dumpvars(0, tb);
//signals
        resetn <= 1'b0;
        #500 resetn <= 1'b1;
        #40000 $display("Test complete.");
        $finish;
    end
endmodule
