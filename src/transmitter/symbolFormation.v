`include "counter_7b.v"
module symbolFormation (
    //Master port
    output wire [DWIDTH-1:0]    M_SAMP_TRE,
    output wire [DWIDTH-1:0]    M_SAMP_TIM,
    output wire                 M_SAMP_TVALID,
    output wire                 M_SAMP_TLAST,
    input  wire                 M_SAMP_TREADY,

    //Slave port 
    input  wire [DWIDTH-1:0]    S_MAP_TRE,
    input  wire [DWIDTH-1:0]    S_MAP_TIM,
    input  wire                 S_MAP_TVALID,
    input  wire                 S_MAP_TLAST,
    output wire                 S_MAP_TREADY,
    //Global
    input wire                  ACLK,
    input wire                  ACLK2,
    input wire                  ARESETN
);
    localparam   DWIDTH = 16;
    localparam   DLENGTH = 48; 
    localparam   FFTLENGTH = 64;
    localparam   NUMLGSC = 5;
    localparam   NUMRGSC = 6;
    //// store tx_data_i when valid for use later
    reg [DWIDTH-1:0] MEM_RE_1[0:DLENGTH-1];
    reg [DWIDTH-1:0] MEM_IM_1[0:DLENGTH-1];
    reg [DWIDTH-1:0] MEM_RE_2[0:DLENGTH-1];
    reg [DWIDTH-1:0] MEM_IM_2[0:DLENGTH-1];
    
    //internal signals
    wire readytoForm;
    reg [15:0] tx_re_i, tx_im_i; 
    
    //Count data in
    reg [5:0] countData = {6'd0};
    always @(posedge S_MAP_TVALID) begin
    //    if (countData == DLENGTH)
    //        countData <= 0;
    //    else
            countData <= countData + 1;
    end
    always @(posedge ACLK2)
        //if (current_state != IDLE) countData <= 0;
        if (current_state == LGSC) countData <= 0;

    always @(posedge S_MAP_TVALID) begin
            MEM_RE_1 [countData] <= S_MAP_TRE;
            MEM_IM_1 [countData] <= S_MAP_TIM;
    end
    //Store data frame k for processing and  store data frame k+1
    wire store_ofdm_symb;
    assign store_ofdm_symb = (next_state == LGSC)? 1'b1: 1'b0;
    always @(posedge store_ofdm_symb) begin
        foreach (MEM_RE_2[j]) begin
            MEM_RE_2[j] = MEM_RE_1[j];
            MEM_IM_2[j] = MEM_IM_1[j];
        end
    end
    // Define our states
    reg [3:0] current_state, next_state;
    //States codification
    localparam IDLE             =   4'b0000;
    localparam LGSC             =   4'b0001;
    localparam DATA_S1          =   4'b0010;
    localparam DATA_P1          =   4'b0011;
    localparam DATA_S2          =   4'b0100;
    localparam DATA_P2          =   4'b0101;
    localparam DATA_S3          =   4'b0110;
    localparam DATA_DC          =   4'b0111;
    localparam DATA_S4          =   4'b1000;
    localparam DATA_P3          =   4'b1001;
    localparam DATA_S5          =   4'b1010;
    localparam DATA_P4          =   4'b1011;
    localparam DATA_S6          =   4'b1100;
    localparam HGSC             =   4'b1101;
    
    //S_CONST_TREADY logic (id_symb max = 7'd15 -> 16QAM)
    assign S_MAP_TREADY = (((current_state == IDLE) || id_symb == 7'd10) && M_SAMP_TREADY)? 1'b1 : 1'b0;

    // Define tx signal constants
    localparam TX_IDLE   = 16'h0000;
    localparam TX_GSC    = 16'd0000;
    localparam TX_DC     = 16'h0000;
    wire LGSC_done, DATA_S1_done, DATA_P1_done, DATA_S2_done, DATA_P2_done;
    wire DATA_S3_done, DATA_DC_done, DATA_S4_done, DATA_P3_done;
    wire DATA_S5_done, DATA_P4_done, DATA_S6_done, HGSC_done;

//Data counting
    assign readytoForm = (current_state != IDLE)? 1'b1 : 1'b0;
    wire  [6:0] id_symb;
    counter_7b_up SYM_COUNTER (
                                .clk(ACLK2),    // input wire CLK
                                .rst(readytoForm),  // input wire SCLR
                                .Q(id_symb)        // output wire [6 : 0] Q
                                );

    assign LGSC_done = (id_symb == NUMLGSC-1) ? 1'b1 : 1'b0;
    assign DATA_S1_done = (id_symb == NUMLGSC+4) ? 1'b1 : 1'b0;
    assign DATA_P1_done = (id_symb == 10) ? 1'b1 : 1'b0;
    assign DATA_S2_done = (id_symb == NUMLGSC+(17+1)) ? 1'b1 : 1'b0;
    assign DATA_P2_done = (id_symb == 24) ? 1'b1 : 1'b0;
    assign DATA_S3_done = (id_symb == NUMLGSC+(23+2)) ? 1'b1 : 1'b0;
    assign DATA_DC_done = (id_symb == 31) ? 1'b1 : 1'b0;
    assign DATA_S4_done = (id_symb == NUMLGSC+(29+3)) ? 1'b1 : 1'b0;
    assign DATA_P3_done = (id_symb == 38) ? 1'b1 : 1'b0;
    assign DATA_S5_done = (id_symb == NUMLGSC+(42+4)) ? 1'b1 : 1'b0;
    assign DATA_P4_done = (id_symb == 52) ? 1'b1 : 1'b0;
    assign DATA_S6_done = (id_symb == NUMLGSC+(47+5)) ? 1'b1 : 1'b0;
    assign HGSC_done = (id_symb == FFTLENGTH-1) ? 1'b1 : 1'b0;
// State Machine
// Next state logic
   always @(*)
     begin : NEXT_STATE_LOGIC
        case (current_state)
          IDLE   :
            begin
               if (countData == DLENGTH) begin
                  next_state = LGSC;

               end
               else begin
                  next_state = current_state;

               end
            end
          LGSC  :
            begin
               if (LGSC_done) begin
                  next_state = DATA_S1;
               end
               else begin
                  next_state = current_state;

               end
            end
          DATA_S1   :
            begin
               if (DATA_S1_done) begin
                  next_state = DATA_P1;

               end
               else begin
                  next_state = current_state;

               end
            end
          DATA_P1 :
            begin
               if (DATA_P1_done) begin
                  next_state = DATA_S2;
               end
               else begin
                  next_state = current_state;

               end
            end
          DATA_S2   :
            begin
               if (DATA_S2_done) begin
                  next_state = DATA_P2;
               end
               else begin
                  next_state = current_state;

               end
            end
           DATA_P2   :
             begin
                if (DATA_P2_done) begin
                   next_state = DATA_S3;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_S3   :
              begin
                if (DATA_S3_done) begin
                   next_state = DATA_DC;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_DC   :
              begin
                if (DATA_DC_done) begin
                   next_state = DATA_S4;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_S4   :
              begin
                if (DATA_S4_done) begin
                   next_state = DATA_P3;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_P3   :
              begin
                if (DATA_P3_done) begin
                   next_state = DATA_S5;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_S5   :
              begin
                if (DATA_S5_done) begin
                   next_state = DATA_P4;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_P4   :
              begin
                if (DATA_P4_done) begin
                   next_state = DATA_S6;
                end
                else begin
                   next_state = current_state;

                end
              end
            DATA_S6   :
              begin
                if (DATA_S6_done) begin
                   next_state = HGSC;
                end
                else begin
                   next_state = current_state;

                end
              end
          HGSC   :
            begin
               if (HGSC_done) begin
                  next_state = LGSC;
               end
               else begin
                  next_state = current_state;

               end
            end
          default:
            next_state = current_state;
        endcase
     end

// State memory
   always @(posedge ACLK2)
     begin : STATE_MEMORY
	if(!ARESETN) begin
           current_state <= IDLE;
	end
	else begin
           current_state <= next_state;
	end

     end
//Output logic
   always @(current_state or id_symb)
     begin : OUTPUT_LOGIC
        case (current_state)
          IDLE   :
            begin
               tx_re_i = TX_IDLE;
               tx_im_i = TX_IDLE;
               
            end
          LGSC  :
            begin
               tx_re_i = TX_GSC;
               tx_im_i = TX_GSC;
            end
          DATA_S1   :
            begin
               tx_re_i = MEM_RE_2[id_symb-NUMLGSC];
               tx_im_i = MEM_IM_2[id_symb-NUMLGSC];
            end
          DATA_P1   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;
            end
          DATA_S2   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+1)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+1)];
            end
          DATA_P2   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;

            end
          DATA_S3   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+3)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+3)];

            end
          DATA_DC   :
            begin
               tx_re_i = 0;
               tx_im_i = 0;

            end
          DATA_S4   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+3)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+3)];

            end
          DATA_P3   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;

            end
          DATA_S5   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+4)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+4)];

            end
          DATA_P4   :
            begin
               tx_re_i = 1;
               tx_im_i = 1;

            end
          DATA_S6   :
            begin
               tx_re_i = MEM_RE_2[id_symb-(NUMLGSC+5)];
               tx_im_i = MEM_IM_2[id_symb-(NUMLGSC+5)];

            end
          HGSC   :
            begin
               tx_re_i = TX_GSC;
               tx_im_i = TX_GSC;

            end
        endcase
     end
   
   assign M_SAMP_TLAST = (current_state == HGSC && id_symb == FFTLENGTH-1)? 1'b1 : 1'b0;
   assign M_SAMP_TRE = tx_re_i*10000;
   assign M_SAMP_TIM = tx_im_i*10000;
   assign M_SAMP_TVALID = (current_state != IDLE && M_SAMP_TREADY)? 1'b1 : 1'b0;
endmodule
